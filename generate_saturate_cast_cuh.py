import os.path as pt
import itertools as it

import numpy as np


ROOT = pt.abspath(pt.dirname(__file__))
C_PATH = pt.join(ROOT, 'augpy', 'saturate_cast.cuh')
PY_PATH = pt.join(ROOT, 'augpy', 'numeric_limits.py')
INT_BITS = [8, 16, 32, 64]
FLOAT_BITS = [32, 64]


C_HEADER = """// AUTOGENERATED FILE
// DO NOT EDIT
// CHANGES WILL BE OVERWRITTEN
// CHANGE generate_saturate_cast_cuh.py INSTEAD
#ifndef AUGPY_SATURATE_CAST_CUH
#define AUGPY_SATURATE_CAST_CUH


#include <algorithm>
#include <limits>
#include <cuda.h>
#include <math.h>


namespace augpy {


typedef float float32_t;
typedef double float64_t;


template<typename input_t, typename output_t>
__device__ __host__ __forceinline__ output_t saturate_cast(const input_t &vin);

"""


C_FOOTER = """// namespace augpy
}


// AUGPY_SATURATE_CAST_CUH
#endif
"""


PY_HEADER = """import numpy as __np


""" + '\n'.join(f'from . import int{bits}' for bits in INT_BITS) + """
""" + '\n'.join(f'from . import uint{bits}' for bits in INT_BITS) + """
""" + '\n'.join(f'from . import float{bits}' for bits in FLOAT_BITS) + """


CAST_LIMITS = {"""


PY_FOOTER = "}\n"


__FLOAT_MAPPINGS = (np.float64, 'double'), (np.float32, 'float')
__INT_MAPPINGS = (np.int32, 'int'), (np.uint32, 'uint'), (np.int64, 'll'), (np.uint64, 'ull')
INTRINSICS = {
    (np.dtype(t1), np.dtype(t2)): f'__{alias1}2{alias2}_rn'
    for (t1, alias1), (t2, alias2) in it.chain(
        (__FLOAT_MAPPINGS,),
        it.product(__FLOAT_MAPPINGS, __INT_MAPPINGS),
        it.product(__INT_MAPPINGS, __FLOAT_MAPPINGS),
    )
}


def tostring(v):
    """
    Represent number v as a decimal string without contraction.
    """
    if v is None:
        return None
    return f'{v:F}' if v % 1 else str(int(v))


def todtype(dtype):
    """
    Return name of C++ type for given dtype as string.
    """
    return str(dtype) + '_t'


def tonptype(dtype):
    """
    Return dtype as numpy attribute string.
    """
    return '__np.' + str(dtype)


def toaptype(dtype):
    """
    Return dtype as augpy attribute string.
    """
    return str(dtype)


def is_int_type(info):
    """
    Return True if info is for integer type.
    """
    return isinstance(info, np.iinfo)


def is_float_type(info):
    """
    Return True if info is for floating point type.
    """
    return isinstance(info, np.finfo)


def representable(v, t_from, t_to, direction):
    """
    Returns true if both v and v+direction are np.isclose when casting
    from t_from to t_to.

    :param v: value to check
    :param t_from: input dtype
    :param t_to: output dtype
    :param direction: search direction, -1 for upper bound, 1 for lower bound
    :return: True if casting is valid
    """
    if v is None:
        return True
    r1 = np.array(v, dtype=t_from).astype(t_to)
    r2 = np.array(v+direction, dtype=t_from).astype(t_to)
    return np.isclose(r1, v) and np.isclose(r2, v+direction)


def __find(v, t_from, t_to, direction):
    """
    Search the limit of representational capability when casting from
    t_from to t_to, starting from value v searching in the given direction.
    Returned value is representable in both t_from and t_to.

    :param v: value to start searching from
    :param t_from: input dtype
    :param t_to: output dtype
    :param direction: search direction, -1 for upper bound, 1 for lower bound
    :return: the found limit value
    """
    if v is None:
        return None
    # first check if value is already representable
    # this avoids infinite loops caused by floating point errors
    if representable(v, t_from.dtype, t_to.dtype, direction):
        return v
    # use resolution of input type to reduce number of required steps
    step_size = np.float128(v) * t_from.resolution if is_float_type(t_from) else 1
    if not is_float_type(t_to):
        step_size = int(step_size)
    # step in the given direction until value is representable
    # then reduce step size and repeat
    while step_size > 1:
        while not representable(v, t_from.dtype, t_to.dtype, direction):
            v += direction * step_size
        v -= direction * step_size
        step_size //= 2
    # make sure value is indeed representable
    while not representable(v, t_from.dtype, t_to.dtype, direction):
        v += direction
    return v


def find_lower(t_from, t_to):
    """
    Find the lower limit when casting from t_from to t_to

    :param t_from: numpy dtype info
    :param t_to:  numpy dtype info
    :return: lower limit as number
    """
    if t_from.min >= t_to.min:
        return None
    return __find(t_to.min, t_from, t_to, 1)


def find_upper(t_from, t_to):
    """
    Find the upper limit when casting from t_from to t_to

    :param t_from: numpy dtype info
    :param t_to:  numpy dtype info
    :return: upper limit as number
    """
    if t_from.max <= t_to.max:
        return None
    return __find(t_to.max, t_from, t_to, -1)


def literal(value, dtype):
    """
    Return C++ literal of value with given numpy dtype as string.
    """
    if value is None:
        return None
    s = tostring(value)
    if is_float_type(dtype):
        if dtype.bits == 32:
            s += '.0F'
        elif dtype.bits == 64:
            s += '.0'
        else:
            raise RuntimeError("only 32 and 64 bit floats supported")
        return s
    elif is_int_type(dtype):
        return f'{"U" if dtype.kind.lower() == "u" else ""}INT{dtype.bits}_C({s})'
    else:
        raise ValueError('unknown dtype ' + repr(dtype))


def apply_rounding(t_from, t_to, op):
    """
    Apply rounding to a given operation string if required.

    :param t_from: numpy dtype info
    :param t_to: numpy dtype info
    :param op: inner operation
    :return: operation string with rounding if required
    """
    if is_float_type(t_from) and is_int_type(t_to):
        if t_from.bits == 32:
            return f'rintf({op})'
        elif t_from.bits == 64:
            return f'rint({op})'
        else:
            raise RuntimeError("only 32 and 64 bit floats supported")
    return op


def generate_code(t_from, t_to):
    """
    Generate code and find lower and upper limits when casting
    from t_from to t_to.

    :param t_from: numpy dtype info
    :param t_to:  numpy dtype info
    :return: code as string, lower limit as number, upper limit as number
    """
    lower = find_lower(t_from, t_to)
    upper = find_upper(t_from, t_to)
    input_t = todtype(t_from.dtype)
    output_t = todtype(t_to.dtype)

    # generate CPU code
    cpu_lines = []
    lower_safe_limit = f'({input_t}){literal(lower, t_from)}'
    lower_limit = f'std::numeric_limits<{output_t}>::lowest()'
    upper_safe_limit = f'({input_t}){literal(upper, t_from)}'
    upper_limit = f'std::numeric_limits<{output_t}>::max()'
    if is_float_type(t_from):
        cpu_lines.append(f'{input_t} vtemp = {apply_rounding(t_from, t_to, "vin")};')
        if lower is not None:
            cpu_lines.append(f'vtemp = vtemp < {lower_safe_limit} ? {lower_limit} : vtemp;')
        if upper is not None:
            cpu_lines.append(f'vtemp = vtemp > {upper_safe_limit} ? {upper_limit} : vtemp;')
        cpu_lines.append('return vtemp;')
        cpu_code = '\n        '.join(cpu_lines)
    else:
        op = apply_rounding(t_from, t_to, 'vin')
        if lower is not None:
            op = f'std::max({op}, ({input_t})std::numeric_limits<{output_t}>::lowest())'
        if upper is not None:
            op = f'std::min({op}, ({input_t})std::numeric_limits<{output_t}>::max())'
        cpu_code = f'return {op};'

    # generate CUDA code
    op = 'vin'
    t_temp = t_to
    intrinsic = INTRINSICS.get((t_from.dtype, t_to.dtype))
    if is_int_type(t_to) and intrinsic is None:
        if t_to.kind == 'i':
            t_temp = np.iinfo(np.int32)
        elif t_to.kind == 'u':
            t_temp = np.iinfo(np.uint32)
        else:
            raise RuntimeError(f'unexpected combination {input_t} -> {output_t}')
    intrinsic = INTRINSICS.get((t_from.dtype, t_temp.dtype))
    if intrinsic is None:
        t_temp = t_to
    if t_temp != t_to:
        lower = find_lower(t_temp, t_to)
        upper = find_upper(t_temp, t_to)
    temp_t = todtype(t_temp.dtype)
    if intrinsic is not None:
        op = f'{intrinsic}({op})'
    if intrinsic is None:
        op = apply_rounding(t_from, t_to, op)
        if lower is not None:
            op = f'max({op}, ({input_t}){literal(lower, t_temp)})'
        if upper is not None:
            op = f'min({op}, ({input_t}){literal(upper, t_temp)})'
    elif temp_t != output_t:
        if lower is not None:
            op = f'max({op}, ({temp_t}){literal(lower, t_temp)})'
        if upper is not None:
            op = f'min({op}, ({temp_t}){literal(upper, t_temp)})'
    cuda_code = f'return {op};'

    # combine CUDA and CPU code
    code = f'''template<>
__device__ __host__ __forceinline__ {output_t} saturate_cast(const {input_t} &vin) {{
    #ifdef __CUDA_ARCH__
        {cuda_code}
    #else
        {cpu_code}
    #endif
}}'''
    return (
        code,
        t_to.min if t_to.min > t_from.min else None,
        t_to.max if t_to.max < t_from.max else None,
    )


def update_if_different(path, new_content):
    """
    Given a file path and new content as string,
    write content to file if new content differs from current content,
    or file does not exist.

    :param path: file path to update if necessary
    :param new_content: new content to written if necessary
    :return: True if file was updated
    """
    if pt.exists(path):
        with open(path, 'r') as f:
            old_content = f.read()
    else:
        old_content = None
    if new_content != old_content:
        with open(path, 'w') as f:
            f.write(new_content)
        return True
    return False


def generate_limits(t1, t2, lower, upper):
    """
    Generate dictionary entries for casting t1 to t2 as strings,
    given lower and upper limits.
    Keys will be all possible pairs of numpy and augpy types.

    :param t1: input type
    :param t2: target type
    :param lower: lower limit
    :param upper: upper limit
    :return: list of dict entries as string
    """
    return [f'    ({f1(t1.dtype)}, {f2(t2.dtype)}): ({tostring(lower)}, {tostring(upper)}),'
            for f1, f2 in it.product((toaptype, tonptype), (toaptype, tonptype))]


def generate_saturate_cast_cuh():
    """
    Generate the saturate_cast.cuh and  numeric_limits.py
    files for this system.
    """
    types = [np.iinfo('uint%d' % b) for b in INT_BITS] \
          + [np.iinfo('int%d' % b) for b in INT_BITS] \
          + [np.finfo('float%d' % b) for b in FLOAT_BITS]
    lines = [C_HEADER]
    numeric_limits = [PY_HEADER]
    for t1, t2 in it.combinations_with_replacement(types, 2):
        code, lower, upper = generate_code(t1, t2)
        numeric_limits.extend(generate_limits(t1, t2, lower, upper))
        lines.extend([code, '\n'])
        if t1 != t2:
            code, lower, upper = generate_code(t2, t1)
            numeric_limits.extend(generate_limits(t2, t1, lower, upper))
            lines.extend([code, '\n'])
    lines.append(C_FOOTER)
    numeric_limits.append(PY_FOOTER)
    update_if_different(C_PATH, '\n'.join(lines))
    update_if_different(PY_PATH, '\n'.join(numeric_limits))


if __name__ == '__main__':
    generate_saturate_cast_cuh()
