# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import re
import sys
from pathlib import Path
import ast
from inspect import Signature
import subprocess

from sphinx.util.typing import stringify
import astor


ROOT = Path(__file__).parent.absolute()
AUGPY_DIR = ROOT / '..' / '..' / 'augpy'
sys.path.insert(0, str((ROOT / '..' / '..').absolute()))

# Use mocked up module on readthedocs
#if os.environ.get('READTHEDOCS', None) != 'True':
#    (AUGPY_DIR / '_augpy.py.mock').rename(AUGPY_DIR / '_augpy.py')


# -- Run Doxygen -------------------------------------------------------------

print('Running Doxygen...')
doxygen_output = subprocess.check_output(
    f'cd {str(ROOT.parent)}; doxygen',
    shell=True,
).decode('utf-8')
print(doxygen_output)


# -- Modifications to sphinx behavior ----------------------------------------


# Include __init__ and _call__ in doc
def include_magic_funcs(app, what, name, obj, would_skip, options):
    if name in ('__call__',):
        return False
    return would_skip


def _cleanup_type_hints(signature):
    return stringify(signature).replace('augpy._augpy.', '').replace('augpy::', '')


TYPE_HINTS = {}


def ast2src(a):
    return astor.to_source(a).strip()


def parse_signature(signature, __dtype_pattern=re.compile(r'<.*?>')):
    type_hints = {}
    signature = __dtype_pattern.sub('0', signature)
    if signature.startswith('('):
        signature = 'f' + signature
    t = ast.parse('def ' + signature + ':\n    pass').body[0]
    if t.returns:
        type_hints['return'] = ast2src(t.returns)
    for arg in t.args.args:
        if arg.annotation:
            type_hints[arg.arg] = ast2src(arg.annotation)
    return type_hints


# For classes with missing signature, add
def add_class_signature(
        app, what, name, obj, options, signature, return_annotation,
        __signature_pattern=re.compile(r'(\(.*\))'),
        __self_pattern=re.compile(r'(self:\s*[^,]+,\s+)')
):
    if signature is None and isinstance(obj, type):
        signature, _, _ = obj.__init__.__doc__.partition('\n')
        signature = __signature_pattern.search(signature).groups(0)[0]
        signature = __self_pattern.sub('', signature)
        signature = _cleanup_type_hints(signature)
    return signature, return_annotation


# Cleanup function signatures and record type hints
def cleanup_signature(
        app, what, name, obj, options, signature, return_annotation
):
    TYPE_HINTS[name] = {}
    if signature is not None:
        signature = _cleanup_type_hints(signature)
        TYPE_HINTS[name] = parse_signature(signature)
    if return_annotation is not None:
        return_annotation = return_annotation.replace('augpy._augpy.', '')
        TYPE_HINTS[name]['return'] = return_annotation
    return signature, return_annotation


def find_indent(
        lines,
        __directive_pattern=re.compile(r'^(\s*):'),
        __whitespace_pattern=re.compile(r'^\s*'),
):
    # find lines that contain directives like :param: and :returns:
    # then count the number of whitespace characters at the beginning
    indents = [__directive_pattern.match(l) for l in lines]
    indents = [len(m.groups(0)[0]) for m in indents if m]
    if indents:
        # find common denominator for indentation length
        return min(indents)

    # if not directive lines are found,
    # search for any whitespace characters instead
    indents = [__whitespace_pattern.match(l).end() for l in lines]
    # ignore lines with no indentation
    indents = [i for i in indents if i > 0]
    if indents:
        # find common denominator for indentation length
        return min(indents)

    # everything failed, so there is no indent
    return 0


# Remove excess indentation
def fix_indentation(app, what, name, obj, options, lines):
    indent = find_indent(lines)
    if indent:
        # only remove indentation at beginning of string,
        # not other whitespace sequences of the same length
        whitespace_pattern = re.compile(fr'^\s{{{indent}}}')
        for i in range(len(lines)):
            lines[i] = whitespace_pattern.sub('    ', lines[i], count=1)


# Rewrite overloaded function docstring
def fix_overloaded(
        app, what, name, obj, options, lines,
        __signature_pattern=re.compile(r'^[1-9]+\. ([a-zA-Z_][a-zA-Z0-9_]*\(.*\)\s+->\s+.+)')
):
    indent = find_indent(lines)
    # scan for overloaded functions
    function_lines = []
    if lines and lines[0].startswith('Overloaded function'):
        lines.pop(0)  # remove "Overloaded function"
        lines.pop(0)  # remove generic signature
        for i, line in enumerate(lines):
            m = __signature_pattern.search(line)
            # add a function signature for each overloaded variant
            if m is not None:
                signature = _cleanup_type_hints(m.groups(0)[0])
                lines[i] = '.. py:function:: ' + signature
                function_lines.append(i)
        # add noindex option to each variant
        # iterate from back so indices are still valid
        for i in function_lines[::-1]:
            lines.insert(i+1, ' '*indent + ':noindex:')


# Attempt to extract type hints from obj
# On failure, attempt to load from recorded type hints
def find_type_hints(obj, name):
    # for pure Python functions, extract annotations from object
    try:
        sig = Signature(obj)
        type_hints = {k: _cleanup_type_hints(v.annotation)
                      for k, v in sig.parameters.items()
                      if v.annotation != Signature.empty}
        if sig.return_annotation != Signature.empty:
            type_hints['return'] = _cleanup_type_hints(sig.return_annotation)
    # for native functions, use type hints recorded from signatures
    except (ValueError, TypeError):
        type_hints = TYPE_HINTS[name]
    return type_hints


# Add type hints for parameters and rtype
def add_type_hints(
    app, what, name, obj, options, lines,
    __signature_pattern=re.compile(r'^\.\. py:function:: ([a-zA-Z_][a-zA-Z0-9_]*\(.*\)\s+->\s+.+)'),
    __param_pattern=re.compile(r'^\s*:param ([^:]+?):'),
    __returns_pattern=re.compile(r'^\s*:return[s]*?:]'),
    __rtype_pattern=re.compile(r'^\s*:rtype:')
):
    type_hints = find_type_hints(obj, name)

    # for every line, check whether it's a parameter definition
    # and add a :type: directive if a type hint is available
    # also add :rtype: if none defined yet and available
    rtype_found = False
    indent = find_indent(lines)
    offset = 0
    for i, line in enumerate(list(lines)):
        # check whether line contains a function signature
        # parse signature to extract type hints
        # add :rtype: if necessary and reset rtype_found flag
        m = __signature_pattern.search(line)
        if m is not None:
            # add rtype if not found and new signature
            hint = type_hints.get('return')
            if not rtype_found and hint:
                lines.insert(i+offset, f'{" "*indent}:rtype: {hint}')
                offset += 1
            type_hints = parse_signature(m.groups(0)[0])
            rtype_found = False
        # check whether line contains a parameter definition
        # add :type: directive for parameter if available
        m = __param_pattern.search(line)
        if m is not None:
            arg = m.groups(0)[0]
            hint = type_hints.get(arg)
            if hint:
                indent = find_indent([line])
                lines.insert(i+offset, f'{" "*indent}:type {arg}: {hint}')
                offset += 1
        m = __rtype_pattern.search(line)
        if m is not None:
            rtype_found = True

    # add rtype if not found and end of docstring
    if not rtype_found and type_hints.get('return'):
        lines.append(f'{" "*indent}:rtype: {type_hints.get("return")}')


def setup(app):
    app.connect('autodoc-skip-member', include_magic_funcs)
    app.connect('autodoc-process-signature', add_class_signature)
    app.connect('autodoc-process-signature', cleanup_signature)
    app.connect('autodoc-process-docstring', fix_indentation)
    app.connect('autodoc-process-docstring', fix_overloaded)
    app.connect('autodoc-process-docstring', add_type_hints)


# -- Project information -----------------------------------------------------

project = 'augpy'
copyright = '2020, Joachim Folz'
author = 'Joachim Folz'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx.ext.napoleon',
    'sphinx.ext.todo',
    'sphinx.ext.viewcode',
    'sphinx.ext.autodoc',
    'sphinx.ext.autosectionlabel',
    'sphinx.ext.intersphinx',
    # 'sphinx_autodoc_typehints',
    'sphinxcontrib.katex',
    'breathe',
]

# location of index.rst
master_doc = 'index'

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_rtd_theme'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']


html_logo = '_static/never.png'


# Config for sphinx-rtd-theme
html_theme_options = {
    'display_version': True,
    'prev_next_buttons_location': 'bottom',
    'style_external_links': True,
    'collapse_navigation': True,
    'sticky_navigation': True,
    'navigation_depth': 4,
    'includehidden': False,
    'titles_only': False
}

html_css_files = [
    'custom.css',
]


# Cuda defines
cpp_id_attributes = [
    '__device__',
    '__host__',
    '__global__',
    '__forceinline__',
    'CNMEM_API',
]


# -- Other -------------------------------------------------------------------

# Add Python doc to intersphinx
intersphinx_mapping = {'python': ('https://docs.python.org/3', None)}


# autodoc Configuration
autoclass_content = 'class'
autodoc_docstring_signature = True
autodoc_typehints = 'signature'
#autodoc_mock_imports = ["augpy._augpy"]

# avoid clashing labels between C++ and Python
autosectionlabel_prefix_document = True

# enable napoleon types for undocumented params
always_document_param_types = True

# napoleon config
napoleon_numpy_docstring = False
napoleon_use_param = True
napoleon_use_keyword = True
napoleon_include_init_with_doc = True

# Breathe Configuration

# default project to build doc for
breathe_default_project = 'augpy'

# where Doxygen XML files are located relative to this file
breathe_projects = {'augpy': ROOT / '..' / 'xml'}

# list of source files in the augpy dir
# this enables the autodoxygenfile to read docstrings from files
breathe_projects_source = {
    "augpy": (AUGPY_DIR, os.listdir(AUGPY_DIR)),
}

# tell breathe which programming language of file types
breathe_domain_by_extension = {
    'h': 'cpp',
    'cuh': 'cpp',
    'cpp': 'cpp',
    'cu': 'cpp',
    'py': 'python',
}

# tell breathe which members to show
breathe_default_members = ('members', 'undoc-members')
