Reductions
----------

Reduction operations like sums on tensors.


.. doxygenfunction:: augpy::sum
.. doxygenfunction:: augpy::sum_axis
.. doxygenfunction:: augpy::all
