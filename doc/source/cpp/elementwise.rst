Elementwise Iteration
---------------------

.. doxygenfunction:: elementwise_contiguous_kernel
.. doxygenfunction:: elementwise_kernel
.. doxygenfunction:: elementwise_function
