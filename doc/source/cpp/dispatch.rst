Dispatching
-----------

Convenience macros to dispath functions based
on tensor data type.

.. doxygendefine:: AUGPY_DISPATCH
.. doxygendefine:: AUGPY_DISPATCH_NOEXC
.. doxygendefine:: AUGPY_DISPATCH_F
.. doxygendefine:: AUGPY_DISPATCH_F_NOEXC
.. doxygendefine:: AUGPY_DISPATCH_D
.. doxygendefine:: AUGPY_DISPATCH_D_NOEXC
