Tensors
-------

augpy's :cpp:struct:`CudaTensor <augpy::CudaTensor>` class is a backward
compatible extension to the :ref:`cpp/tensor:DLPack` specification.
This allows trivial conversion to and from DLPack tensors
and thus exchange of tensors between frameworks.

Currently, only GPU tensors are supported.


Data types
^^^^^^^^^^

:cpp:struct:`CudaTensors <augpy::CudaTensor>` can have the following
data types, defined as :cpp:struct:`DLDataType <DLDataType>`.

.. note::
    Only scalar data types are supported, so lanes is always 1.

.. doxygenvariable:: augpy::dldtype_int8
.. doxygenvariable:: augpy::dldtype_uint8
.. doxygenvariable:: augpy::dldtype_int16
.. doxygenvariable:: augpy::dldtype_uint16
.. doxygenvariable:: augpy::dldtype_int32
.. doxygenvariable:: augpy::dldtype_uint32
.. doxygenvariable:: augpy::dldtype_int64
.. doxygenvariable:: augpy::dldtype_uint64
.. doxygenvariable:: augpy::dldtype_float16
.. doxygenvariable:: augpy::dldtype_float32
.. doxygenvariable:: augpy::dldtype_float64

.. doxygenfunction:: get_dldatatype
.. doxygenfunction:: dldatatype_equals


CudaTensor
^^^^^^^^^^

.. doxygendefine:: DLTENSOR_MAX_NDIM
.. doxygenstruct:: augpy::CudaTensor
.. doxygenfunction:: augpy::copy
.. doxygenfunction:: augpy::fill
.. doxygenfunction:: augpy::cast_tensor
.. doxygenfunction:: augpy::cast_type
.. doxygenfunction:: augpy::empty_like
.. doxygentypedef:: augpy::ndim_array


Tensor Math
^^^^^^^^^^^

For these functions, the ``result`` parameter is optional.
If ``result`` is ``NULL`` a new tensor of appropriate size
is created and returned.
If ``result`` is not ``NULL``, use the given tensor as output
and return ``NULL``.

For basic math functions all inputs and the ``result``
tensor must have the same data type.

For comparison functions ``uint8`` is used as result.
A value of ``1`` means the condition is fulfilled,
otherwise it is ``0``.

Unless otherwise stated, all functions support all
data type, broadcasting, and work with strided tensors.

The ``blocks_per_sm`` and ``num_threads`` control the
kernel launch parameters. The defaults are probably fine,
but they can be used to get some more speed if you
optimize for specific hardware.

.. doxygenfunction:: augpy::add_scalar
.. doxygenfunction:: augpy::add_tensor
.. doxygenfunction:: augpy::sub_scalar
.. doxygenfunction:: augpy::rsub_scalar
.. doxygenfunction:: augpy::sub_tensor
.. doxygenfunction:: augpy::mul_scalar
.. doxygenfunction:: augpy::mul_tensor
.. doxygenfunction:: augpy::div_scalar
.. doxygenfunction:: augpy::rdiv_scalar
.. doxygenfunction:: augpy::div_tensor
.. doxygenfunction:: augpy::fma
.. doxygenfunction:: augpy::gemm
.. doxygenfunction:: augpy::lt_scalar
.. doxygenfunction:: augpy::lt_tensor
.. doxygenfunction:: augpy::le_scalar
.. doxygenfunction:: augpy::le_tensor
.. doxygenfunction:: augpy::gt_scalar
.. doxygenfunction:: augpy::ge_scalar
.. doxygenfunction:: augpy::eq_scalar
.. doxygenfunction:: augpy::eq_tensor


Tensor Management
^^^^^^^^^^^^^^^^^

Converting from and to arrays or tensors,
exporting augpy's
:cpp:struct:`CudaTensors <augpy::CudaTensor>`
to other frameworks, and importing existing
tensors from other frameworks without copying.

.. doxygenfunction:: augpy::tensor_to_array1
.. doxygenfunction:: augpy::tensor_to_array2
.. doxygenfunction:: augpy::array_to_tensor1
.. doxygenfunction:: augpy::array_to_tensor2
.. doxygenfunction:: augpy::import_dltensor
.. doxygenfunction:: augpy::export_dltensor


Utility Functions
^^^^^^^^^^^^^^^^^

Functions that help writing functions that operate on tensors.

.. doxygenfunction:: augpy::array_equals
.. doxygenfunction:: augpy::assert_contiguous
.. doxygenfunction:: augpy::numel(CudaTensor *tensor)
.. doxygenfunction:: augpy::numel(DLTensor *tensor)
.. doxygenfunction:: augpy::numel(DLTensor &tensor)
.. doxygenfunction:: augpy::numel(py::buffer_info &array)
.. doxygenfunction:: augpy::numel(scalar_t *shape, size_t ndim)
.. doxygenfunction:: augpy::numel(std::vector<scalar_t> &shape)
.. doxygenfunction:: augpy::numbytes(CudaTensor *tensor)
.. doxygenfunction:: augpy::numbytes(DLTensor *tensor)
.. doxygenfunction:: augpy::numbytes(py::buffer_info &array)
.. doxygenfunction:: augpy::check_contiguous(CudaTensor *tensor)
.. doxygenfunction:: augpy::check_contiguous(DLTensor *tensor)
.. doxygenfunction:: augpy::check_contiguous(py::buffer_info &array)
.. doxygenfunction:: augpy::check_tensor
.. doxygenfunction:: augpy::check_same_device
.. doxygenfunction:: augpy::check_same_dtype_device
.. doxygenfunction:: augpy::check_same_dtype_device_shape
.. doxygenfunction:: augpy::calc_threads
.. doxygenfunction:: augpy::calc_blocks_values_1d
.. doxygenfunction:: augpy::calc_blocks_values_nd
.. doxygenfunction:: augpy::calculate_contiguous_strides
.. doxygenfunction:: augpy::calculate_broadcast_strides
.. doxygenfunction:: augpy::calculate_broadcast_output_shape(DLTensor t1, DLTensor t2, int &ndim, int64_t *shape)
.. doxygenfunction:: augpy::calculate_broadcast_output_shape(DLTensor t1, DLTensor t2, int &ndim, ndim_array &shape)
.. doxygenfunction:: augpy::create_output_tensor
.. doxygenfunction:: augpy::coalesce_dimensions


DLPack
^^^^^^

This is the documentation of the common
`DLPack <https://github.com/dmlc/dlpack>`_
header.
To quote the readme:

    DLPack is an open in-memory tensor structure to for sharing tensor among frameworks. DLPack enables

      - Easier sharing of operators between deep learning frameworks.
      - Easier wrapping of vendor level operator implementations, allowing collaboration when introducing new devices/ops.
      - Quick swapping of backend implementations, like different version of BLAS
      - For final users, this could bring more operators, and possibility of mixing usage between frameworks.

Please refer to their Github for more details.

.. doxygenfile:: dlpack.h
