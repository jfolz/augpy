Image Functions
---------------


.. toctree::

    jpeg.rst
    blur.rst
    lighting.rst
    warp.rst


