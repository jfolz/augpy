JPEG Decoding
-------------

Hybrid JPEG decoding on CPU and GPU using Nvjpeg.

.. doxygenclass:: augpy::Decoder
