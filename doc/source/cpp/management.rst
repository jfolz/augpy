Device & Memory
---------------

Classes and functions to manage GPU devices and memory.


Device Management
^^^^^^^^^^^^^^^^^

augpy gives you fine control over which Cuda
device is used and which Cuda stream kernels
are run on.
All functions are asynchronous by design, so
events and streams can be used to synchronize
host code.

There are two thread-local global variables
that control which device and stream are
currently active:

  - :any:`current_device <augpy::current_device>`
  - :any:`current_stream <augpy::current_stream>`

.. doxygenclass:: augpy::CudaEvent
.. doxygenclass:: augpy::CudaStream
.. doxygenvariable:: augpy::current_device
.. doxygenvariable:: augpy::current_stream
.. doxygenvariable:: augpy::default_stream


Device Information
^^^^^^^^^^^^^^^^^^

.. doxygenstruct:: augpy::cudaDevicePropEx
.. doxygenfunction:: augpy::get_device_properties
.. doxygenfunction:: augpy::get_num_cuda_cores
.. doxygenfunction:: augpy::cores_per_sm(int device_id)
.. doxygenfunction:: augpy::cores_per_sm(int major, int minor)


Memory Management
^^^^^^^^^^^^^^^^^

.. doxygenfunction:: augpy::meminfo(int device_id)
.. doxygenstruct:: augpy::managed_allocation
.. doxygenfunction:: augpy::managed_cudamalloc
.. doxygenfunction:: augpy::managed_cudafree
.. doxygenfunction:: augpy::managed_eventalloc
.. doxygenfunction:: augpy::managed_eventfree
.. doxygenfunction:: augpy::init_device
.. doxygenfunction:: augpy::release


cnmem
^^^^^

augpy uses `cnmem <https://github.com/NVIDIA/cnmem>`_
to manage GPU device memory.

.. doxygenfile:: cnmem.h
