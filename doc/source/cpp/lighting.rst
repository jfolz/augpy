Lighting
--------

The following functions change the lighting of 2D images.
Both input and output must be contiguous and in channel-first format
:math:`(C,H,W)` (channel, height, width).
All dtypes and an arbitrary number of channels is supported.

Output tensor ``out`` may be ``NULL``, in which case a new tensor
of the same shape and dtype as the input is returned.
Output tensor must be same shape and dtype as
the input.
If output is given ``NULL`` is returned.

.. doxygenfunction:: augpy::lighting
