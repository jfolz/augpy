Exceptions
----------

.. doxygendefine:: CUDA
.. doxygenclass:: augpy::cuda_error
.. doxygendefine:: CNMEM
.. doxygenclass:: augpy::cnmem_error
.. doxygendefine:: NVJPEG
.. doxygenclass:: augpy::nvjpeg_error
.. doxygendefine:: CURAND
.. doxygenclass:: augpy::curand_error
.. doxygendefine:: CUBLAS
.. doxygenclass:: augpy::cublas_error
