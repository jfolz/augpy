Affine Warp
-----------

Functions to apply affine transformations on 2D images.


.. doxygenenum:: augpy::WarpScaleMode
.. doxygenfunction:: augpy::make_affine_matrix
.. doxygenfunction:: augpy::warp_affine
