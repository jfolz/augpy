Blur
----

The following functions apply different types of blur on 2D images.
Both input and output must be contiguous and in channel-first format
(channel, height, width).
All dtypes are supported.
Edge values are repeated for locations that fall outside
the input image.

Output tensor ``out`` may be ``NULL``, in which case a new tensor
of the same shape and dtype as the input is returned.
Output tensor must be same shape and dtype as
the input.
If output is given ``NULL`` is returned.

.. doxygenfunction:: box_blur_single
.. doxygenfunction:: gaussian_blur_single
.. doxygenfunction:: gaussian_blur
