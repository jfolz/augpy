Info & Profiling Functions
--------------------------

.. doxygenfunction:: augpy::meminfo
.. doxygenfunction:: augpy::enable_profiler
.. doxygenfunction:: augpy::disable_profiler
.. doxygenfunction:: augpy::nvtx_range_start
.. doxygenfunction:: augpy::nvtx_range_end
