Core Functionality
------------------

Functionality to catch exceptions, dispatch kernels for
different dtypes, and writing kernels.


.. toctree::

    exception.rst
    dispatch.rst
    saturate_cast.rst
    elementwise.rst
    translate_idx.rst
    function.rst


.. doxygenclass:: augpy::array
.. doxygenfunction:: augpy::make_array
.. doxygendefine:: BLOCKS_PER_SM
