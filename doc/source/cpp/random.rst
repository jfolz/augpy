Random Number Generation
------------------------

Classes for random number generation.
augpy's speciality is that all dtypes are supported for
all distributions.
For example, it is possible to fill an integer tensor
with approximately Gaussian distributed numbers.

.. doxygentypedef:: augpy::rng_t
.. doxygenclass:: augpy::RandomNumberGenerator
