C++ Reference
=============


.. toctree::

    core.rst
    management.rst
    tensor.rst
    reduce.rst
    random.rst
    image.rst
