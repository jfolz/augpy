Index Translation
-----------------

.. doxygenfunction:: translate_idx_strided
.. doxygenfunction:: translate_idx_contiguous_strided
.. doxygenfunction:: translate_idx_strided_strided
.. doxygenfunction:: translate_idx_strided_strided_strided
.. doxygendefine:: THREAD_LOOP_1
.. doxygendefine:: THREAD_LOOP_2
.. doxygendefine:: THREAD_LOOP_3
