Welcome to augpy
================

augpy is a lightweight library with minimal dependencies that
provides a comprehensive tensor implementation for Cuda-enabled
GPUs, with most of the functionality you are used to from numpy
and Pytorch with a similar syntax.

What sets augpy apart is its focus on saturating math
(no under or overflows possible), as well as comprehensive
support for all data types in all functions.
For example, augpy allows you to fill a ``uint8`` tensor with
Gaussian distributed random numbers.

augpy's tensors are based on the :ref:`cpp/tensor:DLPack` specification
and can be exchanged copy-free with other frameworks such as
Jax or Pytorch.

While augpy's support for tensors is quite generic, it also
includes additional functionality to work on 2D images, such
as high quality affine warps with supersampling and Gaussian
blurs.


.. contents:: :local:


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`



Examples
================

.. toctree::
    :maxdepth: 4

    examples.rst


Python Reference
================

.. toctree::
    :maxdepth: 4

    py/index.rst


C++ Reference
=============

.. toctree::
    :maxdepth: 4

    cpp/index.rst
