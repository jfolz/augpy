Image Functions
---------------


JPEG Decoding
^^^^^^^^^^^^^

Hybrid JPEG decoding on CPU and GPU using Nvjpeg.

.. autoclass:: augpy.Decoder
   :members:
   :undoc-members:
   :show-inheritance:


Affine Warp
^^^^^^^^^^^

Functions to apply affine transformations on 2D images.

.. autofunction:: augpy.make_transform

.. autofunction:: augpy.make_affine_matrix

.. autofunction:: augpy.warp_affine

.. autoclass:: augpy.WarpScaleMode
   :members:
   :undoc-members:
   :show-inheritance:


Lighting
^^^^^^^^

The following functions change the lighting of 2D images.
Both input and output must be contiguous and in channel-first format
:math:`(C,H,W)` (channel, height, width).
All dtypes and an arbitrary number of channels is supported.

Output tensor ``out`` may be ``None``, in which case a new tensor
of the same shape and dtype as the input is returned.
Output tensor must be same shape and dtype as
the input.
If output is given ``None`` is returned.

.. autofunction:: augpy.lighting


Blur
^^^^

The following functions apply different types of blur on 2D images.
Both input and output must be contiguous and in channel-first format
(channel, height, width).
All dtypes are supported.
Edge values are repeated for locations that fall outside
the input image.

Output tensor ``out`` may be ``None``, in which case a new tensor
of the same shape and dtype as the input is returned.
Output tensor must be same shape and dtype as
the input.
If output is given ``None`` is returned.

.. autofunction:: augpy.box_blur_single

.. autofunction:: augpy.gaussian_blur_single

.. autofunction:: augpy.gaussian_blur


augpy.image
-----------

.. automodule:: augpy.image
   :members:
   :undoc-members:
   :show-inheritance:
