Tensors
-------


.. contents:: :local:


.. note::
    All operations in augpy are asynchronous with respect to the
    CPU, i.e., function call initiate work on the GPU and return
    immediately.
    For example :py:meth:`CudaTensor.numpy` will initiate copying
    data from the device to the host memory and return the array
    immediately, even though data has not yet been fully copied
    over.

    Use :py:meth:`CudaStream.synchronize`, or
    :py:meth:`CudaEvent.record` and :py:meth:`CudaEvent.synchronize`
    to synchronize CPU code with the respective stream or event
    on the GPU.

    However, all work done on the GPU is sequential within a
    :py:class:`CudaStream`.
    You can use augpy functions to "queue up" operations on tensors,
    so synchronization is only required when using interacting
    with the CPU or another GPU framework.


Data Types
^^^^^^^^^^

.. autoclass:: augpy.DLDataType
    :members:
    :undoc-members:
    :show-inheritance:

.. autoclass:: augpy.DLDataTypeCode
    :members:
    :undoc-members:
    :show-inheritance:

.. autofunction:: augpy.to_augpy_dtype

.. autofunction:: augpy.to_numpy_dtype

.. autofunction:: augpy.swap_dtype

.. autofunction:: augpy.to_temp_dtype

.. py:attribute:: augpy.int8

    <DLDataType int8>

.. py:attribute:: augpy.int16

    <DLDataType int16>

.. py:attribute:: augpy.int32

    <DLDataType int32>

.. py:attribute:: augpy.int64

    <DLDataType int64>

.. py:attribute:: augpy.uint8

    <DLDataType uint8>

.. py:attribute:: augpy.uint16

    <DLDataType uint16>

.. py:attribute:: augpy.uint32

    <DLDataType uint32>

.. py:attribute:: augpy.uint64

    <DLDataType uint64>

.. py:attribute:: augpy.float16

    <DLDataType float16>

    .. warning::

        Not yet supported.

.. py:attribute:: augpy.float32

    <DLDataType float32>

.. py:attribute:: augpy.float64

    <DLDataType float64>


CudaTensor
^^^^^^^^^^

augpy's tensor class.
It is a backwards compatible extension to the :ref:`cpp/tensor:dlpack`
specification.

It supports all the usual operations you would expect from a
full-featured tensor class, like complex indexing and slicing::

    >>> t = CudaTensor((2, 2, 4), uint8)
    >>> t
    <CudaTensor shape=(2, 2, 4), device=0, dtype=uint8>
    >>> t[1, 1, 3]
    <CudaTensor shape=(), device=0, dtype=uint8>
    >>> t[-1]
    <CudaTensor shape=(2, 4), device=0, dtype=uint8>
    >>> t[:,0]
    <CudaTensor shape=(2, 4), device=0, dtype=uint8>
    >>> t[:, 1:, 1:-1]
    <CudaTensor shape=(2, 1, 2), device=0, dtype=uint8>
    >>> t[:, :, ::2]
    <CudaTensor shape=(2, 2, 2), device=0, dtype=uint8>

Math and comparison operations you are used to from
numpy or Pytorch also work just fine::

    >>> t.numpy()
    array([[[0, 0, 0, 0],
            [0, 0, 0, 0]],

           [[0, 0, 0, 0],
            [0, 0, 0, 0]]], dtype=uint8)
    >>> t += 3
    >>> t.numpy()
    array([[[3, 3, 3, 3],
            [3, 3, 3, 3]],

           [[3, 3, 3, 3],
            [3, 3, 3, 3]]], dtype=uint8)
    >>> (5 - t).numpy()
    array([[[2, 2, 2, 2],
            [2, 2, 2, 2]],

           [[2, 2, 2, 2],
            [2, 2, 2, 2]]], dtype=uint8)
    >>> (t > (t - 1)).numpy()
    array([[[1, 1, 1, 1],
            [1, 1, 1, 1]],

           [[1, 1, 1, 1],
            [1, 1, 1, 1]]], dtype=uint8)

.. note::
    All operations in augpy are asynchronous, so calling
    :py:meth:`CudaTensor.numpy` will initiate copying data
    from the device to the host memory.
    You need to use :py:meth:`CudaStream.synchronize`, or
    :py:meth:`CudaEvent.record` and :py:meth:`CudaEvent.synchronize`
    to ensure that data is fully copied before the array
    is accessed.

Math is saturating in augpy. Integer tensors will never
over or underflow::

    >>> t[:] = 40
    >>> t.numpy()
    array([[[40, 40, 40, 40],
            [40, 40, 40, 40]],

           [[40, 40, 40, 40],
            [40, 40, 40, 40]]], dtype=uint8)
    >>> (0 - t).numpy()
    array([[[0, 0, 0, 0],
            [0, 0, 0, 0]],

           [[0, 0, 0, 0],
            [0, 0, 0, 0]]], dtype=uint8)

Broadcasting is also supported::

    >>> t1 = CudaTensor((3, 1), uint8)
    >>> t2 = CudaTensor((1, 3), uint8)
    >>> ((t1 + 3) * (t2 + 4)).numpy()
    array([[12, 12, 12],
           [12, 12, 12],
           [12, 12, 12]], dtype=uint8)

.. note::
    Tensors may appear to be initialized with zeros.
    They may, however, reuse memory from previously deleted
    tensors, so they should be treated as uninitialized and
    need to be zeroed or otherwise initialized.


.. autoclass:: augpy.CudaTensor
    :members:
    :undoc-members:
    :show-inheritance:


Creation & Conversion
^^^^^^^^^^^^^^^^^^^^^

.. autofunction:: augpy.cast

.. autofunction:: augpy.copy

.. autofunction:: augpy.empty_like

.. autofunction:: augpy.array_to_tensor

.. autofunction:: augpy.tensor_to_array

.. autofunction:: augpy.import_dltensor

.. autofunction:: augpy.export_dltensor


Functions
^^^^^^^^^

.. autofunction:: augpy.add

.. autofunction:: augpy.sub

.. autofunction:: augpy.rsub

.. autofunction:: augpy.mul

.. autofunction:: augpy.div

.. autofunction:: augpy.rdiv

.. autofunction:: augpy.lt

.. autofunction:: augpy.le

.. autofunction:: augpy.gt

.. autofunction:: augpy.ge

.. autofunction:: augpy.eq

.. autofunction:: augpy.fma

.. autofunction:: augpy.gemm

.. autofunction:: augpy.fill

.. autofunction:: augpy.sum
