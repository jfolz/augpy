Core Functionality
------------------

Exceptions raised by augpy, managing devices and computation streams,
and controlling how functions are run on GPUs.


.. contents:: :local:


Device Management
^^^^^^^^^^^^^^^^^

augpy gives you fine control over which Cuda
device is used and which Cuda stream kernels
are run on.
All functions are asynchronous by design, so
events and streams can be used to synchronize
host code.

There are two thread-local global variables
that control which device and stream are
currently active.


current_device
""""""""""""""

Each thread tracks its currently used Cuda device in the
:any:`current_device <augpy::current_device>` variable.
Use :py:meth:`CudaDevice.activate() <augpy.CudaDevice.activate>`
to make a stream the current stream and
:py:meth:`CudaDevice.deactivate() <augpy.CudaDevice.deactivate>`.
to restore the previous state.
Use :py:func:`get_current_device() <augpy.get_current_device>`
to get the currently active device.


current_stream
""""""""""""""

Each thread tracks its currently used Cuda stream in the
:any:`current_stream <augpy::current_stream>` variable.
Use :py:meth:`CudaStream.activate() <augpy.CudaStream.activate>`
to make a stream the current stream and
:py:meth:`CudaStream.deactivate() <augpy.CudaStream.deactivate>`.
to restore the previous state.
Use :py:func:`get_current_stream() <augpy.get_current_stream>`
to get the currently active stream.


default_stream
""""""""""""""

You can use the :py:attr:`default_stream <augpy.default_stream>`
to synchronize CPU and GPU execution without explicitly creating
and activating a different stream.


.. note::
    All operations in augpy are asynchronous with respect to the
    CPU, so calling :py:meth:`CudaTensor.numpy` will initiate
    copying data from the device to the host memory and return
    immediately.
    You need to use :py:meth:`CudaStream.synchronize`, or
    :py:meth:`CudaEvent.record` and :py:meth:`CudaEvent.synchronize`
    to ensure that data is fully copied before the array
    is accessed.


.. autoclass:: augpy.CudaDevice
    :members:

.. autoclass:: augpy.CudaEvent
    :members:

.. autoclass:: augpy.CudaStream
    :members:

.. autofunction:: augpy.get_current_device

.. autofunction:: augpy.get_current_stream

.. py:attribute:: augpy.default_stream

    The default :py:class:`CudaStream`.
    Implicitly available on all Cuda devices.

.. autofunction:: augpy.release


Device Information
^^^^^^^^^^^^^^^^^^

.. autofunction:: augpy.get_device_properties

.. autoclass:: augpy.CudaDeviceProp
    :members:

.. autofunction:: augpy.meminfo


Exceptions
^^^^^^^^^^

.. py:exception:: augpy.CudaError

    Raised when a problem with a GPU occurs,
    e.g., device is unavailable or invalid kernel configuration.

.. py:exception:: augpy.CuRandError

    Raised when a problem with CuRand occurs,
    e.g., no memory left for random state.

.. py:exception:: augpy.CuBlasError

    Raised when a problem with CuBlas occurs,
    e.g., no memory left for handle.

.. py:exception:: augpy.MemoryError

    Raised when a problem with GPU memory occurs,
    e.g., no memory left for tensor.

.. py:exception:: augpy.NvJpegError

    Raised when a problem with JPEG decoding occurs,
    e.g., corrupt or unsupported image.


Blocks and threads
^^^^^^^^^^^^^^^^^^

Cuda code executes in blocks of threads, each of which calculates
one or more values in a tensor.
The number of threads in a block greatly influences the performance
of kernels, as they will share resources likes caches, but can
also collaborate in calculations.

A Cuda-enabled GPU is organized in SMs with a number of Cuda cores each.
Each SM can work on a block independently.
Thus, it is important that a task is divided into at least as many
blocks as there are SMs.
You can use
:py:func:`get_device_properties() <augpy.get_device_properties>`
to find out, among other useful information, how many SMs there
are and the number of Cuda cores per SM.

augpy functions often allow you to control how they are executed on
the GPU:

#. Set the ``blocks_per_sm`` parameter to control how many blocks
   the work is divided into. The total number of blocks will be
   ``blocks_per_sm`` times number of SMs on the GPU.
#. Set the ``threads`` parameter to control how many threads
   there are in each block. If ``threads`` is zero, the number of
   cores per SM is used.

Together these parameters define the total number of threads that
will be started for the kernel.
The calculations for each element of the tensor are distributed
evenly among these threads, i.e., each thread may calculate more
than one value.
More values per thread is often more efficient, however it must
be balanced against the number of blocks to keep all SMs busy.

The defaults for ``blocks_per_sm`` and ``threads`` parameters
are usually quite sensible, but depending on your GPU architecture
other combinations may provide better performance.