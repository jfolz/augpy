augpy.numeric\_limits
---------------------

.. py:attribute:: augpy.numeric_limits.CAST_LIMITS

    Dictionary of numeric limits when casting between data types
    in augpy.

    Keys are tuples :math:`(d_{from}, d_{to})` for casting from
    type :math:`d_{from}` to type :math:`d_{to}`.
    Types may be any combination of augpy and numpy types.

    Values are tuples :math:`(v_{min}, v_{max})`, which define
    the lowest and highest possible value after casting.
    ``None`` for either means that all negative or positive
    :math:`d_{from}` values are representable by :math:`d_{to}`.

    For casting floating point types to integers these limits
    may be different from the lowest and highest values of
    :math:`d_{to}` respectively.
    augpy guarantees that, for negative values, the cast value
    is never lower and for positive values is never higher than
    the original value.
    Some values near the end of the value range would otherwise
    produce inconsistent results.

    Example::

        CAST_LIMITS[numpy.uint16, numpy.uint32] = (None, None)
        CAST_LIMITS[augpy.uint64, augpy.int32] = (None, 2147483647)
        CAST_LIMITS[numpy.float64, augpy.int16] = (-32768, 32767)
        CAST_LIMITS[augpy.float32, numpy.uint64] = (0, 18446744073709551615)

