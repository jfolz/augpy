Examples
========

Here are some simple examples of what you can do with augpy tensors.


Create a new tensor and do basic math on it::

    >>> import augpy
    >>> t = augpy.CudaTensor((3, 3)).fill(7)
    >>> print(t.numpy())
    [[7 7 7]
     [7 7 7]
     [7 7 7]]
    >>> t += 11
    >>> print(t.numpy())
    [[18 18 18]
     [18 18 18]
     [18 18 18]]


Decode a JPEG image on the GPU::

    >>> with open('cat.jpg', 'rb') as f:
    ...     data = f.read()
    ...
    >>> decoder = augpy.Decoder()
    >>> img = decoder.decode(data)
    >>> print(img)
    <CudaTensor shape=(480, 640, 3), device=0, dtype=uint8>


Convert a numpy array to augpy tensor::

    >>> import numpy as np
    >>> array = np.array([124, 116, 104], dtype=np.uint8)
    >>> background = augpy.array_to_tensor(array)
    >>> print(background)
    <CudaTensor shape=(3), device=0, dtype=uint8>


Apply affine warp to an image::

    >>> warped = augpy.CudaTensor((3, 224, 224))
    >>> m, s = augpy.make_transform(img.shape[:2], warped.shape[1:], angle=10)
    >>> print(m)
    [[  2.1103022   -0.37210324 125.3217    ]
     [  0.37210324   2.1103022  -38.029423  ]]
    >>> augpy.warp_affine(img, warped, m, background, s)
    >>> print(warped.numpy())
    [[[124 124 124 ... 138 138 138]
      [124 124 124 ... 138 138 138]
      [124 124 124 ... 138 138 137]
      ...
      [110 106 102 ... 124 124 124]
      [101  95  90 ... 124 124 124]
      [ 86  82  77 ... 124 124 124]]

     [[116 116 116 ... 101 101 101]
      [116 116 116 ... 101 101 101]
      [116 116 116 ... 100 101 100]
      ...
      [ 94  91  90 ... 116 116 116]
      [ 88  86  83 ... 116 116 116]
      [ 82  78  75 ... 116 116 116]]

     [[104 104 104 ...  68  68  68]
      [104 104 104 ...  68  68  68]
      [104 104 104 ...  67  68  67]
      ...
      [ 85  84  83 ... 104 104 104]
      [ 83  81  80 ... 104 104 104]
      [ 81  80  77 ... 104 104 104]]]


Add noise to red channel::

    >>> gen = augpy.RandomNumberGenerator()
    >>> noise = augpy.CudaTensor((224, 224), dtype=augpy.int8)
    >>> gen.gaussian(noise, 5, 80)
    >>> print(noise.numpy())
    [[ -15   20   -3 ...    4   40   22]
     [  50   73  -30 ...   -8   45   58]
     [ -65  -32  -32 ...  127   35  -76]
     ...
     [  79  -23   94 ...   43  -17  127]
     [   0  -24   -4 ... -128   27   25]
     [  -1  -21  127 ...   52 -128  -34]]
    >>> augpy.fma(0.5, noise, warped[0], warped[0])
    >>> print(warped.numpy())
    [[[116 134 122 ... 140 158 149]
      [149 160 109 ... 134 160 167]
      [ 92 108 108 ... 202 156  99]
      ...
      [150  94 149 ... 146 116 188]
      [101  83  88 ...  60 138 136]
      [ 86  72 140 ... 150  60 107]]

     [[116 116 116 ... 101 101 101]
      [116 116 116 ... 101 101 101]
      [116 116 116 ... 100 101 100]
      ...
      [ 94  91  90 ... 116 116 116]
      [ 88  86  83 ... 116 116 116]
      [ 82  78  75 ... 116 116 116]]

     [[104 104 104 ...  68  68  68]
      [104 104 104 ...  68  68  68]
      [104 104 104 ...  67  68  67]
      ...
      [ 85  84  83 ... 104 104 104]
      [ 83  81  80 ... 104 104 104]
      [ 81  80  77 ... 104 104 104]]]


.. note::
    No clipping is required, since integer math is saturating.
    Over or underflows cannot occur.


Finally, export tensors to other frameworks like Pytorch::

    >>> from torch.utils.dlpack import from_dlpack
    >>> capsule = augpy.export_dltensor(warped)
    >>> torch_tensor = from_dlpack(capsule)
    >>> print(torch_tensor)
    tensor([[[116, 134, 122,  ..., 140, 158, 149],
             [149, 160, 109,  ..., 134, 160, 167],
             [ 92, 108, 108,  ..., 202, 156,  99],
             ...,
             [150,  94, 149,  ..., 146, 116, 188],
             [101,  83,  88,  ...,  60, 138, 136],
             [ 86,  72, 140,  ..., 150,  60, 107]],

            [[116, 116, 116,  ..., 101, 101, 101],
             [116, 116, 116,  ..., 101, 101, 101],
             [116, 116, 116,  ..., 100, 101, 100],
             ...,
             [ 94,  91,  90,  ..., 116, 116, 116],
             [ 88,  86,  83,  ..., 116, 116, 116],
             [ 82,  78,  75,  ..., 116, 116, 116]],

            [[104, 104, 104,  ...,  68,  68,  68],
             [104, 104, 104,  ...,  68,  68,  68],
             [104, 104, 104,  ...,  67,  68,  67],
             ...,
             [ 85,  84,  83,  ..., 104, 104, 104],
             [ 83,  81,  80,  ..., 104, 104, 104],
             [ 81,  80,  77,  ..., 104, 104, 104]]], device='cuda:0',
           dtype=torch.uint8)


