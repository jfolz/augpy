#!/bin/bash
set -e -x

# check nvidia driver and cuda status
nvidia-smi

# install cmake3 and create a symlink to cmake
yum install -y cmake3
ln -s /usr/bin/cmake3 /usr/bin/cmake

# Python 3.5 is not supported
rm -r /opt/python/cp35*

# Compile wheels
for PYBIN in /opt/python/*/bin; do
    OLDPIP=$("${PYBIN}/pip" freeze --all | grep '^pip==' | tr -d '\n')
    OLDWHEEL=$("${PYBIN}/pip" freeze --all | grep '^wheel==' | tr -d '\n')
    "${PYBIN}/pip" install -U pip wheel --no-warn-script-location
    "${PYBIN}/pip" install -r build-requirements.txt
    "${PYBIN}/pip" install pybind11-stubgen
    "${PYBIN}/python" setup.py build_ext --inplace
    "${PYBIN}/python" -m pybind11_stubgen -o . --no-setup-py augpy
    mv augpy-stubs/_augpy/__init__.pyi augpy/_augpy.pyi
    rm -r augpy-stubs build
    "${PYBIN}/pip" wheel . -v -w wheelhouse/ --no-deps
    "${PYBIN}/pip" install --force "$OLDPIP" "$OLDWHEEL"
done

# Bundle external shared libraries into the wheels
for whl in wheelhouse/*.whl; do
    auditwheel repair "$whl" -w dist
done

# Install and test
cd test
for PYBIN in /opt/python/*/bin/; do
    "${PYBIN}/pip" install -r ../test-requirements.txt
    "${PYBIN}/pip" install augpy --no-index -f ../dist
    "${PYBIN}/python" -m pytest -vv
done
cd ..
