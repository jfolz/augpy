augpy
=====

augpy is a lightweight library with minimal dependencies that
provides a comprehensive tensor implementation for Cuda-enabled
GPUs, with most of the functionality you are used to from numpy
and Pytorch with a similar syntax.

What sets augpy apart is its focus on saturating math
(no under or overflows possible), as well as comprehensive
support for all data types in all functions.
For example, augpy allows you to fill a ``uint8`` tensor with
Gaussian distributed random numbers.

augpy's tensors are based on the
`DLPack <https://github.com/dmlc/dlpack>`_ specification
and can be exchanged copy-free with other frameworks such as
Jax or Pytorch.

While augpy's support for tensors is quite generic, it also
includes additional functionality to work on 2D images, such
as high quality affine warps with supersampling and Gaussian
blurs.


Building
--------

Make sure you have the following installed.

- Compiler with C++14 support (e.g. GCC 5)
- Cuda 10 or higher
- Cmake 3.13 or higher
- setuptools>=44.0.0
- wheel>=0.34.0
- pybind11>=2.4.3
- numpy>=1.15.0

Now simply run ``setup.py`` as normal to build the wheel and install with pip.

::

    python setup.py bdist_wheel



Usage
-----

TODO



Changelog
---------

1.0.0a1
~~~~~~~

- WIP
