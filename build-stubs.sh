#!/bin/bash
set -e -x

python3.6 -m pybind11_stubgen -o . --no-setup-py augpy
mv augpy-stubs/_augpy/__init__.pyi augpy/_augpy.pyi
