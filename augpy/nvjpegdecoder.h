#ifndef AUGPY_NVJPEGDECODER_H
#define AUGPY_NVJPEGDECODER_H


#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#include "nvjpeg.h"
#include "tensor.h"


namespace py = pybind11;


namespace augpy {


/**
Wrapper for Nvjpeg-based JPEG decoding.
*/
class Decoder {
public:
    /**
    Create a new decoder on the \link current_device \endlink.

    @param device_padding see <a href=
    "https://docs.nvidia.com/cuda/nvjpeg/index.html#nvjpeg-set-device-mem-padding">Nvjpeg docs</a>
    @param host_padding see <a href=
    "https://docs.nvidia.com/cuda/nvjpeg/index.html#nvjpeg-set-pinned-mem-padding">Nvjpeg docs</a>
    @param gpu_huffman enable Huffman decoding on the GPU;
                       not recommended unless you really need
                       to offload from CPU
    */
    Decoder(
        size_t device_padding,
        size_t host_padding,
        bool gpu_huffman
    );

    ~Decoder();

    /**
    Decode a JPEG image using Nvjpeg.
    Output is in \f$ (H,W,C) \f$ format and resides on the GPU device.


    @param data compressed JPEG image as a JFIF string, i.e.,
                the full file contents
    @param buffer optional buffer to use; may be `NULL`;
                  if not `NULL` must be big enough to contain
                  the decoded image
    @return decoded image on GPU in \f$ (H,W,C) \f$ format
    */
    CudaTensor* decode(std::string data, CudaTensor* buffer);

private:
    int device_id;
    nvjpegHandle_t handle;
    nvjpegJpegState_t state;
    nvjpegJpegState_t state_batched;
    nvjpegDevAllocator_t device_allocator;
};


// namespace augpy
}


// AUGPY_NVJPEGDECODER_H
#endif
