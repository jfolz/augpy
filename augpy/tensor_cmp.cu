#include <cuda.h>
#include <cuda_runtime.h>
#include "tensor.h"
#include "dispatch.h"
#include "saturate_cast.cuh"
#include "elementwise.cuh"
#include <tuple>
#include <algorithm>


namespace augpy {


CudaTensor* make_result_tensor(CudaTensor* tensor, CudaTensor* out) {
    if (!tensor) {
        throw std::invalid_argument("argument 1 may not be None");
    }
    DLTensor t = tensor->dl_tensor;
    if (!out) {
        out = new CudaTensor(t.shape, t.ndim, dldtype_uint8, t.ctx.device_id);
    }
    return out;
}


CudaTensor* make_result_tensor(CudaTensor* tensor1, CudaTensor* tensor2, CudaTensor* out) {
    if (!tensor1 || !tensor2) {
        throw std::invalid_argument("argument 1 and 2 may not be None");
    }
    DLTensor t1 = tensor1->dl_tensor;
    DLTensor t2 = tensor2->dl_tensor;
    check_same_dtype_device(t1, t2);
    if (!out) {
        int ndim;
        ndim_array shape;
        calculate_broadcast_output_shape(t1, t2, ndim, shape);
        out = new CudaTensor(shape.ptr(), ndim, dldtype_uint8, t1.ctx.device_id);
    }
    return out;
}


template <typename scalar_t>
__device__ __forceinline__ void __lt(
        const array<tensor_param, 2> &tensors,
        const scalar_t &scalar
){
    tensors[0].store<uint8_t>(tensors[1].load<scalar_t>() < scalar);
}


template <typename scalar_t>
__device__ __forceinline__ void __le(
        const array<tensor_param, 2> &tensors,
        const scalar_t &scalar
){
    tensors[0].store<uint8_t>(tensors[1].load<scalar_t>() <= scalar);
}


template <typename scalar_t>
__device__ __forceinline__ void __gt(
        const array<tensor_param, 2> &tensors,
        const scalar_t &scalar
){
    tensors[0].store<uint8_t>(tensors[1].load<scalar_t>() > scalar);
}


template <typename scalar_t>
__device__ __forceinline__ void __ge(
        const array<tensor_param, 2> &tensors,
        const scalar_t &scalar
){
    tensors[0].store<uint8_t>(tensors[1].load<scalar_t>() >= scalar);
}


template <typename scalar_t>
__device__ __forceinline__ void __eq(
        const array<tensor_param, 2> &tensors,
        const scalar_t &scalar
){
    tensors[0].store<uint8_t>(tensors[1].load<scalar_t>() == scalar);
}


template<typename scalar_t>
__device__ __forceinline__ void __lt_tensor(
        const array<tensor_param, 3> &tensors,
        const uint8_t &nothing
){
    tensors[0].store<uint8_t>(
        tensors[1].load<scalar_t>() < tensors[2].load<scalar_t>()
    );
}


template<typename scalar_t>
__device__ __forceinline__ void __le_tensor(
        const array<tensor_param, 3> &tensors,
        const uint8_t &nothing
){
    tensors[0].store<uint8_t>(
        tensors[1].load<scalar_t>() <= tensors[2].load<scalar_t>()
    );
}


template<typename scalar_t>
__device__ __forceinline__ void __eq_tensor(
        const array<tensor_param, 3> &tensors,
        const uint8_t &nothing
){
    tensors[0].store<uint8_t>(
        tensors[1].load<scalar_t>() == tensors[2].load<scalar_t>()
    );
}


CudaTensor* lt_scalar(
        CudaTensor* tensor,
        double scalar,
        CudaTensor* out,
        unsigned int blocks_per_sm,
        unsigned int num_threads
){
    out = make_result_tensor(tensor, out);
    auto tensors = make_array(out, tensor);
    AUGPY_DISPATCH(tensor->dl_tensor.dtype, "lt_scalar", ([&] {
        scalar_t casted = saturate_cast<double, scalar_t>(scalar);
        elementwise_function<2, scalar_t, __lt<scalar_t>>(
            tensors, casted, blocks_per_sm, num_threads, false
        );
    }));
    return out;
}


CudaTensor* le_scalar(
        CudaTensor* tensor,
        double scalar,
        CudaTensor* out,
        unsigned int blocks_per_sm,
        unsigned int num_threads
){
    out = make_result_tensor(tensor, out);
    auto tensors = make_array(out, tensor);
    AUGPY_DISPATCH(tensor->dl_tensor.dtype, "le_scalar", ([&] {
        scalar_t casted = saturate_cast<double, scalar_t>(scalar);
        elementwise_function<2, scalar_t, __le<scalar_t>>(
            tensors, casted, blocks_per_sm, num_threads, false
        );
    }));
    return out;
}


CudaTensor* gt_scalar(
        CudaTensor* tensor,
        double scalar,
        CudaTensor* out,
        unsigned int blocks_per_sm,
        unsigned int num_threads
){
    out = make_result_tensor(tensor, out);
    auto tensors = make_array(out, tensor);
    AUGPY_DISPATCH(tensor->dl_tensor.dtype, "gt_scalar", ([&] {
        scalar_t casted = saturate_cast<double, scalar_t>(scalar);
        elementwise_function<2, scalar_t, __gt<scalar_t>>(
            tensors, casted, blocks_per_sm, num_threads, false
        );
    }));
    return out;
}


CudaTensor* ge_scalar(
        CudaTensor* tensor,
        double scalar,
        CudaTensor* out,
        unsigned int blocks_per_sm,
        unsigned int num_threads
){
    out = make_result_tensor(tensor, out);
    auto tensors = make_array(out, tensor);
    AUGPY_DISPATCH(tensor->dl_tensor.dtype, "ge_scalar", ([&] {
        scalar_t casted = saturate_cast<double, scalar_t>(scalar);
        elementwise_function<2, scalar_t, __ge<scalar_t>>(
            tensors, casted, blocks_per_sm, num_threads, false
        );
    }));
    return out;
}


CudaTensor* eq_scalar(
        CudaTensor* tensor,
        double scalar,
        CudaTensor* out,
        unsigned int blocks_per_sm,
        unsigned int num_threads
){
    out = make_result_tensor(tensor, out);
    auto tensors = make_array(out, tensor);
    AUGPY_DISPATCH(tensor->dl_tensor.dtype, "eq_scalar", ([&] {
        scalar_t casted = saturate_cast<double, scalar_t>(scalar);
        elementwise_function<2, scalar_t, __eq<scalar_t>>(
            tensors, casted, blocks_per_sm, num_threads, false
        );
    }));
    return out;
}


CudaTensor* lt_tensor(
        CudaTensor* tensor1,
        CudaTensor* tensor2,
        CudaTensor* out,
        unsigned int blocks_per_sm,
        unsigned int num_threads
){
    out = make_result_tensor(tensor1, tensor2, out);
    auto tensors = make_array(out, tensor1, tensor2);
    AUGPY_DISPATCH(tensor1->dl_tensor.dtype, "lt_tensor", ([&] {
        elementwise_function<3, uint8_t, __lt_tensor<scalar_t>>(
            tensors, 0, blocks_per_sm, num_threads, false
        );
    }));
    return out;
}


CudaTensor* le_tensor(
        CudaTensor* tensor1,
        CudaTensor* tensor2,
        CudaTensor* out,
        unsigned int blocks_per_sm,
        unsigned int num_threads
){
    out = make_result_tensor(tensor1, tensor2, out);
    auto tensors = make_array(out, tensor1, tensor2);
    AUGPY_DISPATCH(tensor1->dl_tensor.dtype, "le_tensor", ([&] {
        elementwise_function<3, uint8_t, __le_tensor<scalar_t>>(
            tensors, 0, blocks_per_sm, num_threads, false
        );
    }));
    return out;
}


CudaTensor* eq_tensor(
        CudaTensor* tensor1,
        CudaTensor* tensor2,
        CudaTensor* out,
        unsigned int blocks_per_sm,
        unsigned int num_threads
){
    out = make_result_tensor(tensor1, tensor2, out);
    auto tensors = make_array(out, tensor1, tensor2);
    AUGPY_DISPATCH(tensor1->dl_tensor.dtype, "eq_tensor", ([&] {
        elementwise_function<3, uint8_t, __eq_tensor<scalar_t>>(
            tensors, 0, blocks_per_sm, num_threads, false
        );
    }));
    return out;
}


// namespace augpy
}
