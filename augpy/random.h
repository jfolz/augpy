#ifndef AUGPY_RANDOM_H
#define AUGPY_RANDOM_H


#include <curand.h>
#include <curand_kernel.h>
#include "tensor.h"


namespace augpy {


using rng_t = curandStateXORWOW_t;


/**
A convenient wrapper for cuRAND methods
that fill tensors with pseudo-random numbers.
*/
class RandomNumberGenerator {
public:
    /**
    Create a new RNG instance.
    ``device_id`` and ``seed`` may both be Python ``None``.
    The behavior is identical to
    \link RandomNumberGenerator(int*, unsigned long long*) \endlink
    with ``NULL`` pointers.
    */
    RandomNumberGenerator(py::object* device_id, py::object* seed);

    /**
    Create a new RNG instance.

    @param device_id GPU device ID;
                     if ``NULL``, \link current_device \endlink
                     is used
    @param seed random seed;
                if ``NULL``, read values from <a href=
                "https://en.cppreference.com/w/cpp/numeric/random/random_device"
                >std::random_device</a>
                to create a random seed.
    */
    RandomNumberGenerator(int* device_id, unsigned long long* seed);

    //~RandomNumberGenerator() noexcept(false);

    /**
    Fill ``target`` tensor with uniformly distributed numbers
    in \f$ [v_{min}, v_{max}) \f$.

    @note This is supported for integer tensors. Values are
          cast from float or double down to the integer type.
          The mean of the values is approximately
          \f$ \frac{v_{max} + v_{min}}{2} \f$.

    @warning Saturation is not used.
             \f$ v_{min} \f$ and \f$ v_{max} \f$ must be
             representable in the target tensor data type.

    @param target tensor to fill
    @param vmin minimum value; can occur
    @param vmax maximum value; does not occur
    @param blocks_per_sm number of blocks per SM
    @param num_threads number of threads per block
        @rst
        See :ref:`py/core:Blocks and threads`
        @endrst
    */
    void uniform(
        CudaTensor* target,
        double vmin,
        double vmax,
        unsigned int blocks_per_sm=BLOCKS_PER_SM,
        unsigned int num_threads=0
    );

    /**
    Fill ``target`` tensor with Gaussian distributed numbers
    with specified ``mean`` and standard deviation ``std``.

    @note This is supported for integer tensors. Values are
          drawn from the given distribution, then rounded and
          cast to the data type of the tensor with saturation.
          The values in an integer tensor are thus only
          approximately Gaussian distributed.


    @param target tensor to fill
    @param mean Gaussian mean
    @param std Gaussian standard deviation
    @param blocks_per_sm number of blocks per SM
    @param num_threads number of threads per block
        @rst
        See :ref:`py/core:Blocks and threads`
        @endrst
    */
    void gaussian(
        CudaTensor* target,
        double mean,
        double std,
        unsigned int blocks_per_sm=BLOCKS_PER_SM,
        unsigned int num_threads=0
    );

private:
    /**
    Initialize the random state of the given device.

    @param device_id GPU device ID;
                     if ``NULL``, \link current_device \endlink
                     is used
    @param seed random seed;
                if ``NULL``, read values from <a href=
                "https://en.cppreference.com/w/cpp/numeric/random/random_device"
                >std::random_device</a>
                to create a random seed.
    */
    void init_device_state(int* device_id, unsigned long long* seed);
    int device_id;
    unsigned long long seed;

    std::shared_ptr<managed_allocation> alloc;
    curandGenerator_t gen;
    rng_t* device_states;
    size_t num_states;
};


// namespace augpy
}


// AUGPY_RANDOM_H
#endif
