/**
@file elementwise.cuh

Provides convenience functions to create kernels that run
elementwise operations on tensors.
*/


#ifndef AUGPY_ELEMENTWISE_CUH
#define AUGPY_ELEMENTWISE_CUH

#include "tensor.h"
#include "saturate_cast.cuh"
#include "cub/thread/thread_load.cuh"
#include "cub/thread/thread_store.cuh"


namespace augpy {


#define AUGPY_VALUES_PER_THREAD 16


const cub::CacheLoadModifier LOAD_MOD = cub::LOAD_LDG;
const cub::CacheStoreModifier STORE_MOD = cub::STORE_DEFAULT;


/**
Combines the data pointer of a tensor with its strides in bytes.
*/
struct tensor_param {
    /**
    Data pointer. Not actually ``unsigned char``.
    */
    unsigned char* ptr;
    /**
    Strides of the tensor in bytes.
    */
    ndim_array strides;

    /**
    Interpret pointer as `scalar_t` and load value at current position.
    */
    template<typename scalar_t>
    __device__ __forceinline__ scalar_t load() const {
        return cub::ThreadLoad<LOAD_MOD>(
            reinterpret_cast<scalar_t*>(ptr)
        );
    }

    /**
    Interpret pointer as `scalar_t` and load value at current position
    plus given offset.
    */
    template<typename scalar_t>
    __device__ __forceinline__ scalar_t load(const std::ptrdiff_t offset) const {
        return cub::ThreadLoad<LOAD_MOD>(
            reinterpret_cast<scalar_t*>(ptr + offset)
        );
    }

    /**
    Interpret pointer as `scalar_t`, load value at current
    position plus given offset and cast to `temp_t`.
    */
    template<typename scalar_t, typename temp_t>
    __device__ __forceinline__ temp_t load() const {
        return saturate_cast<scalar_t, temp_t>(cub::ThreadLoad<LOAD_MOD>(
            reinterpret_cast<scalar_t*>(ptr))
        );
    }

    /**
    Interpret pointer as `scalar_t`, load value at current
    position plus given offset and cast to `temp_t`.
    */
    template<typename scalar_t, typename temp_t>
    __device__ __forceinline__ temp_t load(const std::ptrdiff_t offset) const {
        return saturate_cast<scalar_t, temp_t>(cub::ThreadLoad<LOAD_MOD>(
            reinterpret_cast<scalar_t*>(ptr + offset))
        );
    }

    /**
    Store given value `v` at current
    pointer position interpreted as `scalar_t`.
    */
    template<typename scalar_t>
    __device__ __forceinline__ void store(const scalar_t& v) const {
        cub::ThreadStore<STORE_MOD>(
            reinterpret_cast<scalar_t*>(ptr),
            v
        );
    }

    /**
    Cast given value `v` to `scalar_t` and store at current
    pointer position interpreted as `scalar_t`.
    */
    template<typename temp_t, typename scalar_t>
    __device__ __forceinline__ void store(const temp_t& v) const {
        cub::ThreadStore<STORE_MOD>(
            reinterpret_cast<scalar_t*>(ptr),
            saturate_cast<temp_t, scalar_t>(v)
        );
    }
};


// Pre-define kernel so breathe can parse it
/**
Cuda kernel for elementwise functions on arbitrary tensors.
``values_per_thread`` defines how many elements in the tensors
each thread in each block will calculate.
This is done by striding in the first dimension of the tensors.
Effectively, ``values_per_thread`` is never larger than ``shape0``.


@param tensors an array of ``n_tensors``
               \link tensor_param tensor_params \endlink;
               first tensor is output, remainder are inputs;
               strides must be given in bytes
@param constants constant value given to function
@param contiguous_strides strides in bytes a contiguous tensor
                          of the same shape would have
@param ndim number of dimensions in tensors
@param count number of values in tensors
@param values_per_thread number of values in tensor each thread
                         in each block computes
@param shape0 number of elements in first dimension of tensors
@tparam n_tensors number of tensors given to function
@tparam param_t type of constant value given to kernel function
                alongside tensors
@tparam F function of type
          ``void F(const array<tensor_param, n_tensors>&, const param_t&)``
          applied to tensors
*/
template <int n_tensors, unsigned int values_per_thread, typename param_t, typename F>
__global__ void elementwise_kernel(
        array<tensor_param, n_tensors> tensors,
        const param_t constants,
        const ndim_array contiguous_strides,
        const int ndim,
        const size_t count,
        size_t shape0
);


// The actual kernel implementation
template <int n_tensors, unsigned int values_per_thread, typename param_t,
          void (*F)(const array<tensor_param, n_tensors>&, const param_t&)>
__global__ void elementwise_kernel(
        array<tensor_param, n_tensors> tensors,
        const param_t constants,
        const ndim_array contiguous_strides,
        const int ndim,
        const size_t count,
        size_t shape0
){
    // 0D and 1D tensors don't require translation
    if (ndim <= 1) {
        size_t rem = blockDim.x * blockIdx.x * values_per_thread + threadIdx.x;
        if (rem >= count) {
            return;
        }
        shape0 = min((size_t)values_per_thread, ceil_div(shape0 - rem, blockDim.x));
        #pragma unroll
        for (int t=0; t<n_tensors; ++t) {
            tensors[t].ptr += rem * tensors[t].strides[0] / contiguous_strides[0];
        }
    }
    // with ND tensors loop over dim 0
    // dim 0 is defined by blockIdx.x * values_per_thread
    // remainder of indices is translated through strides
    else {
        size_t rem = blockDim.x * blockIdx.y + threadIdx.x;
        if (rem >= count) {
            return;
        }
        // calculate the first dimension index
        size_t x = (size_t) blockIdx.x * values_per_thread;
        // remaining values in first dimension
        shape0 = min((size_t)values_per_thread, shape0 - x);
        // move pointers in first dimension
        #pragma unroll
        for (int t=0; t<n_tensors; ++t) {
            tensors[t].ptr += tensors[t].strides[0] * x;
        }
        // ensure ndim_arrays are put into registers instead of local memory:
        //  - loop must be fixed length so it can be unrolled,
        //    thus indexing arrays with a constant value
        //  - exit the loop manually by testing against ndim
        //
        // See this section of the CUDA programming guide for details:
        // https://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html#device-memory-accesses
        #pragma unroll
        for(int dim=1; dim<DLTENSOR_MAX_NDIM; ++dim) {
            if (dim >= ndim) break;
            size_t p = rem / contiguous_strides[dim];
            #pragma unroll
            for (int t=0; t<n_tensors; ++t) {
                tensors[t].ptr += p * tensors[t].strides[dim];
            }
            rem -= p * contiguous_strides[dim];
        }
    }

    // loop shape0 in dim 0
    for(; shape0>0; --shape0) {
        F(tensors, constants);
        #pragma unroll
        for (int t=0; t<n_tensors; ++t) {
            tensors[t].ptr += tensors[t].strides[0];
        }
    }
}


// Pre-define function so breathe can parse it
/**
Apply a function elementwise to some tensors.


@param tensors an array of ``n_tensors`` tensors;
               first tensor is output, remainder are inputs
@param constant constant value given to function
@param blocks_per_sm number of blocks to create per SM on the GPU;
                     at least 1
@param num_threads number of threads in each block;
                   0 means auto-select
@param enforce_same_dtype if true, throws ``std::invalid_argument``
                          if tensors have different dtypes
@tparam n_tensors number of tensors given to function
@tparam param_t type of constant value given to kernel function
                alongside tensors
@tparam F function of type
          ``void (*F)(array<tensor_param, n_tensors>, param_t)``
          applied to tensors
*/
template <int n_tensors, typename param_t, typename F>
CudaTensor* elementwise_function(
        array<CudaTensor*, n_tensors> tensors,
        const param_t constant,
        unsigned int blocks_per_sm,
        unsigned int num_threads,
        bool enforce_same_dtype=true
);


// The actual function implementation
template <int n_tensors, typename param_t,
          void (*F)(const array<tensor_param, n_tensors>&, const param_t&)>
CudaTensor* elementwise_function(
        array<CudaTensor*, n_tensors> tensors,
        const param_t constant,
        unsigned int blocks_per_sm,
        unsigned int num_threads,
        bool enforce_same_dtype=true
){
    static_assert(n_tensors >= 0, "need at least 1 output tensor");

    CudaTensor* out = NULL;
    // create output tensor if none given
    // output shape is determined by broadcasting all input tensors
    if (!tensors[0]) {
        if (n_tensors <= 1) {
            throw std::invalid_argument("no output tensor given");
        }
        if (!tensors[1]) {
            throw std::invalid_argument("required argument 1 is None");
        }
        // make output tensor with max in every dimension of given tensors
        // requiring all tensors to not be NULL
        tensors[0] = out = create_output_tensor(&tensors[1], n_tensors-1, false);
    }
    else {
        out = tensors[0];
    }

    // fill dltensors and tensor_params with current values
    array<ndim_array, n_tensors> shapes;
    array<tensor_param, n_tensors> tensor_params;
    std::vector<DLTensor> dltensors(n_tensors);
    for (int t=0; t<n_tensors; ++t) {
        DLTensor dlt = tensors[t]->dl_tensor;
        shapes[t] = ndim_array(dlt.shape, dlt.ndim);
        tensor_params[t].strides = ndim_array(dlt.strides, dlt.ndim);
        tensor_params[t].ptr = reinterpret_cast<unsigned char*>(tensors[t]->ptr());
        dlt.shape = &shapes[t][0];
        dlt.strides = &tensor_params[t].strides[0];
        dltensors[t] = dlt;
    }

    // attempt to broadcast every tensor to output shape
    // filling the tensor_param array in the process
    DLTensor &r = dltensors[0];
    bool broadcast = false;
    for (int t=0; t<n_tensors; ++t) {
        if (enforce_same_dtype) {
            check_same_dtype_device(dltensors[t], r);
        }
        else {
            check_same_device(dltensors[t], r);
        }
        broadcast = calculate_broadcast_strides(dltensors[t], r, tensor_params[t].strides, t);
    }

    // attempt to coalesce dimensions and calculate contiguous strides
    coalesce_dimensions(dltensors);
    ndim_array contiguous_strides;
    calculate_contiguous_strides(r, contiguous_strides);

    // so far shapes of tensors in call were compatible
    // if numel(tensor)==0 for some tensor, there is nothing to do
    for (int t=0; t<n_tensors; ++t) {
        if (numel(tensors[t]->dl_tensor) == 0) {
            return out;
        }
    }

    // check if all tensors are contiguous
    bool contiguous = !broadcast;
    for (int t=0; t<n_tensors; ++t) {
        if (!contiguous) break;
        contiguous = tensors[t]->is_contiguous();
    }

    // determine the launch configuration
    calc_threads(num_threads, r.ctx.device_id);
    dim3 grid(1, 1, 1);
    size_t count;
    int64_t shape0;
    unsigned int values_per_thread = AUGPY_VALUES_PER_THREAD;
    // result is a scalar
    if (r.ndim == 0) {
        count = 1;
        shape0 = 1;
        num_threads = 1;
        values_per_thread = 1;
        contiguous_strides[0] = 1;
        contiguous = true;
    }
    // result is 1D
    else if (r.ndim == 1) {
        shape0 = r.shape[0];
        calc_blocks_values_1d(r, grid.x, count, values_per_thread, num_threads, blocks_per_sm);
        // every thread will stride by block size = num_threads
        for (int t=0; t<n_tensors; ++t) {
            tensor_params[t].strides[0] *= num_threads;
        }
        contiguous_strides[0] *= num_threads;
    }
    // result is ND
    else {
        shape0 = r.shape[0];
        calc_blocks_values_nd(r, grid, count, values_per_thread, num_threads, blocks_per_sm);
    }

    // DLTensor strides are defined as items
    // elementwise_kernel assumes all strides are given in bytes
    // multiply all strides by number of bytes in dtype
    for (int t=0; t<n_tensors; ++t) {
        int bytes = itemsize(dltensors[t].dtype);
        for (int dim=0; dim<r.ndim; ++dim) {
            tensor_params[t].strides[dim] *= bytes;
        }
    }

    // launch strided kernel
    elementwise_kernel<n_tensors, AUGPY_VALUES_PER_THREAD, param_t, F>
    <<<grid, num_threads, 0, current_stream>>>(
        tensor_params, constant, contiguous_strides,
        r.ndim, count, shape0
    );

    CUDA(cudaGetLastError());

    // record usage of involved tensors
    for (int t=0; t<n_tensors; ++t) {
        tensors[t]->record();
    }

    return out;
}


// namespace augpy
}


// AUGPY_ELEMENTWISE_CUH
#endif
