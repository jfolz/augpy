#ifndef AUGPY_FUNCTIONS_H
#define AUGPY_FUNCTIONS_H


#include <string>
#include <utility>
#include <cuda.h>
#include <pybind11/pybind11.h>
#include "nvToolsExt.h"


namespace augpy {


/**
Set the <a href=
"https://docs.nvidia.com/cuda/cuda-runtime-api/group__CUDART__DEVICE.html#group__CUDART__DEVICE_1g69e73c7dda3fc05306ae7c811a690fac"
>cudaDeviceScheduleYield</a> flag for the current_device.

\warning EXPERIMENTAL! MAY REDUCE GPU THROUGHPUT!
*/
void init();


/**
For the device defined by ``device_id``,
return the current used, free, and total memory in bytes.
*/
std::tuple<size_t, size_t, size_t> meminfo(int device_id);


/**
Enable the Cuda profiler.
*/
void enable_profiler();


/**
Disable the Cuda profiler.
*/
void disable_profiler();


/**
Tell the Nvidia profiler to start a new <a href=
"https://docs.nvidia.com/cuda/profiler-users-guide/index.html#nvtx"
>nvtx</a> range.
Can be used to place marks in profiling output.

@param msg Message attached to the range
@return range ID to be used with \link nvtx_range_end \endlink
*/
nvtxRangeId_t nvtx_range_start(std::string msg);


/**
Tell the Nvidia profiler to end the given <a href=
"https://docs.nvidia.com/cuda/profiler-users-guide/index.html#nvtx"
>nvtx</a> range.

@param end ID of the range to end
*/
void nvtx_range_end(nvtxRangeId_t end);


// namespace augpy
}


// AUGPY_FUNCTIONS_H
#endif
