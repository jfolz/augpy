// blur.h
#ifndef CUDA_BLUR_H
#define CUDA_BLUR_H


#include <vector>


namespace augpy {


/** \brief Apply box blur to a single image.

Kernel size describes both width and height in pixels
of the area in the input that is averaged for each
output pixel.
Odd values are recommended for best results.
For even values, the center of the kernel is below
and to the right of the true center.
This means the output is shifted up and left by half
a pixel.


@param input image tensor in \f$ (C,H,W) \f$ format.
@param ksize kernel size in pixels
@param out output tensor (may be `NULL`)
@return new tensor if `out` is `NULL`, else `out`
*/
CudaTensor* box_blur_single(
        CudaTensor* input,
        int ksize,
        CudaTensor* out
);


/** \brief Apply Gaussian blur to a batch of images.

Maximum kernel size can be calculated like this:
`ksize = max(3, int(max(sigmas) * 6.6 - 2.3) | 1)`

I.e., `ksize` is at least 3 and always odd.

The given kernel size defines the upper limit.
The actual kernel size is calculated with the
formula above and clipped at the given maximum.

Smaller values can be given to trade speed vs quality.
Bigger values typically do not visibly improve quality.

Odd values are strongly recommended for best results.
For even values, the center of the kernel is below
and to the right of the true center.
This means the output is shifted up and left by half
a pixel.
This can lead to inconsistencies between images
in the batch.
Images with large sigmas may be shifted, while smaller
sigmas mean no shift occurs.


@param input batch tensor in \f$ (N,C,H,W) \f$ format
@param sigmas float tensor with one sigma value per image
              in the batch
@param max_ksize maximum kernel size in pixels
@param out output tensor (may be `NULL`)
@return new tensor if `out` is `NULL`, else `out`
*/
CudaTensor* gaussian_blur(
        CudaTensor* input,
        CudaTensor* sigmas,
        int max_ksize,
        CudaTensor* out
);


/** \brief Apply Gaussian blur to a single image.

Kernel size is calculated like this:

`ksize = max(3, int(sigma * 6.6 - 2.3) | 1)`

I.e., `ksize` is at least 3 and always odd.


@param input image tensor in \f$ (N,C,H,W) \f$ format
@param sigma standard deviation of the kernel
@param out output tensor (may be `NULL`)
@return new tensor if `out` is `NULL`, else `out`
*/
CudaTensor* gaussian_blur_single(
        CudaTensor* input,
        float sigma,
        CudaTensor* out
);


// namespace augpy
}


// blur.h
#endif
