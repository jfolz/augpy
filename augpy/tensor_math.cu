#include <cuda.h>
#include <cuda_runtime.h>
#include "tensor.h"
#include "dispatch.h"
#include "saturate_cast.cuh"
#include "elementwise.cuh"
#include <tuple>


template<typename scalar_t>
__device__ __forceinline__ scalar_t generic_fma(const scalar_t x, const scalar_t y, const scalar_t z);


template<>
__device__ __forceinline__ float generic_fma(const float x, const float y, const float z){
    return fmaf(x,y,z);
}


template<>
__device__ __forceinline__ double generic_fma(const double x, const double y, const double z){
    return fma(x,y,z);
}


namespace augpy {


template <typename scalar_t, typename sscalar_t, typename temp_t>
__device__ __forceinline__ void fma_function(
        const array<tensor_param, 3> &tensors,
        const temp_t &scalar
){
    tensors[0].store<temp_t, scalar_t>(generic_fma(
        scalar,
        tensors[1].load<sscalar_t, temp_t>(),
        tensors[2].load<scalar_t, temp_t>()
    ));
}


CudaTensor* fma(
        double scalar,
        CudaTensor* tensor1,
        CudaTensor* tensor2,
        CudaTensor* out,
        unsigned int blocks_per_sm,
        unsigned int num_threads
){
    auto tensors = make_array(out, tensor1, tensor2);
    AUGPY_DISPATCH(tensor2->dl_tensor.dtype, "fma", ([&] {
        ASSERT_TRUE(dldatatype_equals(get_dldatatype<sscalar_t>(), tensor1->dl_tensor.dtype),
                    "tensor1 dtype must be signed version of tensor2 dtype");
        if (out) {
            ASSERT_TRUE(dldatatype_equals(tensor2->dl_tensor.dtype, tensor2->dl_tensor.dtype),
                        "tensor2 dtype must same as out tensor");
        }
        else {
            out = empty_like(tensor2);
        }
        out = elementwise_function<3, temp_t, fma_function<scalar_t, sscalar_t, temp_t>>(
            tensors,
            (temp_t) scalar,
            blocks_per_sm,
            num_threads,
            false
        );
    }));
    return out;
}


template <typename scalar_t, typename temp_t>
__device__ __forceinline__ void __add_scaled(
        const array<tensor_param, 2> &tensors,
        const std::tuple<temp_t, temp_t> &scalars
){
    tensors[0].store<temp_t, scalar_t>(generic_fma(
        std::get<1>(scalars),
        tensors[1].load<scalar_t, temp_t>(),
        std::get<0>(scalars)
    ));
}


CudaTensor* add_scaled(
        double alpha,
        double beta,
        CudaTensor* tensor,
        CudaTensor* out,
        unsigned int blocks_per_sm,
        unsigned int num_threads
){
    auto tensors = make_array(out, tensor);
    AUGPY_DISPATCH(tensor->dl_tensor.dtype, "add_scaled", ([&] {
        out = elementwise_function<2, std::tuple<temp_t, temp_t>, __add_scaled<scalar_t, temp_t>>(
            tensors,
            std::make_tuple((temp_t)alpha, (temp_t)beta),
            blocks_per_sm,
            num_threads
        );
    }));
    return out;
}


template<typename scalar_t, typename temp_t>
__device__ __forceinline__ void __add_scaled_tensor(
        const array<tensor_param, 3> &tensors,
        const std::tuple<temp_t> &scalars
){
    tensors[0].store<temp_t, scalar_t>(generic_fma(
        std::get<0>(scalars),
        tensors[1].load<scalar_t, temp_t>(),
        tensors[2].load<scalar_t, temp_t>()
    ));
}


CudaTensor* add_scaled_tensor(
        double alpha,
        CudaTensor* tensor1,
        CudaTensor* tensor2,
        CudaTensor* out,
        unsigned int blocks_per_sm,
        unsigned int num_threads
){
    auto tensors = make_array(out, tensor1, tensor2);
    AUGPY_DISPATCH(tensor1->dl_tensor.dtype, "add_scaled_tensor", ([&] {
        out = elementwise_function<3, std::tuple<temp_t>, __add_scaled_tensor<scalar_t, temp_t>>(
            tensors,
            std::make_tuple((temp_t)alpha),
            blocks_per_sm,
            num_threads
        );
    }));
    return out;
}


template<typename scalar_t, typename temp_t>
__device__ __forceinline__ void __mul_scaled_tensor(
        const array<tensor_param, 3> &tensors,
        const std::tuple<temp_t> &scalars
){
    tensors[0].store<temp_t, scalar_t>(generic_fma(
        tensors[1].load<scalar_t, temp_t>(),
        tensors[2].load<scalar_t, temp_t>(),
        std::get<0>(scalars)
    ));
}


CudaTensor* mul_scaled_tensor(
        double alpha,
        CudaTensor* tensor1,
        CudaTensor* tensor2,
        CudaTensor* out,
        unsigned int blocks_per_sm,
        unsigned int num_threads
){
    auto tensors = make_array(out, tensor1, tensor2);
    AUGPY_DISPATCH(tensor1->dl_tensor.dtype, "mul_scaled_tensor", ([&] {
        out = elementwise_function<3, std::tuple<temp_t>,
                                      __mul_scaled_tensor<scalar_t, temp_t>>(
            tensors,
            std::make_tuple((temp_t)alpha),
            blocks_per_sm,
            num_threads
        );
    }));
    return out;
}


template<typename scalar_t, typename temp_t>
__device__ __forceinline__ void __rdiv_scaled(
        const array<tensor_param, 2> &tensors,
        const std::tuple<temp_t, temp_t> &scalars
){
    tensors[0].store<temp_t, scalar_t>(
        std::get<0>(scalars) + std::get<1>(scalars)
        / tensors[1].load<scalar_t, temp_t>()
    );
}


CudaTensor* rdiv_scaled(
        double alpha,
        double beta,
        CudaTensor* tensor,
        CudaTensor* out,
        unsigned int blocks_per_sm,
        unsigned int num_threads
){
    auto tensors = make_array(out, tensor);
    AUGPY_DISPATCH(tensor->dl_tensor.dtype, "rdiv_scaled", ([&] {
        out = elementwise_function<2, std::tuple<temp_t, temp_t>,
                                      __rdiv_scaled<scalar_t, temp_t>>(
            tensors,
            std::make_tuple((temp_t)alpha, (temp_t)beta),
            blocks_per_sm,
            num_threads
        );
    }));
    return out;
}


template<typename scalar_t, typename temp_t>
__device__ __forceinline__ void __div_scaled_tensor(
        const array<tensor_param, 3> &tensors,
        const std::tuple<temp_t, temp_t> &scalars
){
    tensors[0].store<temp_t, scalar_t>(generic_fma(
        std::get<1>(scalars),
        tensors[1].load<scalar_t, temp_t>(),
        std::get<0>(scalars)
    ) / tensors[2].load<scalar_t, temp_t>());
}


CudaTensor* div_scaled_tensor(
        double alpha,
        double beta,
        CudaTensor* tensor1,
        CudaTensor* tensor2,
        CudaTensor* out,
        unsigned int blocks_per_sm,
        unsigned int num_threads
){
    auto tensors = make_array(out, tensor1, tensor2);
    AUGPY_DISPATCH(tensor1->dl_tensor.dtype, "rdiv_scaled", ([&] {
        out = elementwise_function<3, std::tuple<temp_t, temp_t>,
                                      __div_scaled_tensor<scalar_t, temp_t>>(
            tensors,
            std::make_tuple((temp_t)alpha, (temp_t)beta),
            blocks_per_sm,
            num_threads
        );
    }));
    return out;
}


CudaTensor* add_scalar(
        CudaTensor* tensor, double scalar, CudaTensor* out,
        unsigned int blocks_per_sm, unsigned int num_threads
){
    return add_scaled(scalar, 1.0, tensor, out,
                      blocks_per_sm, num_threads);
}


CudaTensor* sub_scalar(
        CudaTensor* tensor, double scalar, CudaTensor* out,
        unsigned int blocks_per_sm, unsigned int num_threads
){
    return add_scaled(-scalar, 1.0, tensor, out,
                      blocks_per_sm, num_threads);
}


CudaTensor* rsub_scalar(
        CudaTensor* tensor, double scalar, CudaTensor* out,
        unsigned int blocks_per_sm, unsigned int num_threads
){
    return add_scaled(scalar, -1.0, tensor, out,
                      blocks_per_sm, num_threads);
}


CudaTensor* mul_scalar(
        CudaTensor* tensor, double scalar, CudaTensor* out,
        unsigned int blocks_per_sm, unsigned int num_threads
){
    return add_scaled(0.0, scalar, tensor, out,
                      blocks_per_sm, num_threads);
}


CudaTensor* div_scalar(
        CudaTensor* tensor, double scalar, CudaTensor* out,
        unsigned int blocks_per_sm, unsigned int num_threads
){
    return add_scaled(0.0, 1.0/scalar, tensor, out,
                      blocks_per_sm, num_threads);
}


CudaTensor* rdiv_scalar(
        CudaTensor* tensor, double scalar, CudaTensor* out,
        unsigned int blocks_per_sm, unsigned int num_threads
){
    return rdiv_scaled(0.0, scalar, tensor, out,
                       blocks_per_sm, num_threads);
}


CudaTensor* add_tensor(
    CudaTensor* tensor1, CudaTensor* tensor2, CudaTensor* out,
    unsigned int blocks_per_sm, unsigned int num_threads
){
    return add_scaled_tensor(1.0, tensor1, tensor2, out,
                             blocks_per_sm, num_threads);
}


CudaTensor* sub_tensor(
    CudaTensor* tensor1, CudaTensor* tensor2, CudaTensor* out,
    unsigned int blocks_per_sm, unsigned int num_threads
){
    return add_scaled_tensor(-1.0, tensor2, tensor1, out,
                             blocks_per_sm, num_threads);
}


CudaTensor* mul_tensor(
    CudaTensor* tensor1, CudaTensor* tensor2, CudaTensor* out,
    unsigned int blocks_per_sm, unsigned int num_threads
){
    return mul_scaled_tensor(0.0, tensor1, tensor2, out,
                             blocks_per_sm, num_threads);
}


CudaTensor* div_tensor(
    CudaTensor* tensor1, CudaTensor* tensor2, CudaTensor* out,
    unsigned int blocks_per_sm, unsigned int num_threads
){
    return div_scaled_tensor(0.0, 1.0, tensor1, tensor2, out,
                             blocks_per_sm, num_threads);
}


// namespace augpy
}
