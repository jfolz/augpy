/**
@file tensor.h
Defines the CudaTensor class along with basic
copy, math, and comparison methods that operate on them.
*/


#ifndef AUGPY_TENSOR_H
#define AUGPY_TENSOR_H


#include <vector>
#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#include <cuda.h>
#include <cuda_runtime.h>
#include "dlpack.h"
#include "core.h"
#include "exception.h"


namespace py = pybind11;


namespace augpy {



#ifndef DLTENSOR_MAX_NDIM
/**
Maximum number of dimensions a
\link augpy::CudaTensor CudaTensor \endlink can have.
Currently 6.
*/
#define DLTENSOR_MAX_NDIM 6
#endif

/*
#ifndef BATCH_ARG_SIZE
/// Array number of dimensions a CudaTensor can have
/// Currently 16.
#define BATCH_ARG_SIZE 16
#endif
*/


/**
`int64` array of length \link DLTENSOR_MAX_NDIM \endlink.
Used to store shape or strides.
*/
typedef array<int64_t, DLTENSOR_MAX_NDIM> ndim_array;


/*
template<typename scalar_t>
using arg_array = array<scalar_t, BATCH_ARG_SIZE>;

typedef arg_array<float> float_array;
typedef arg_array<double> double_array;
typedef arg_array<int> int_array;
typedef arg_array<int64_t> int64_array;
*/


/**
8 bit signed integer.
*/
const DLDataType dldtype_int8{kDLInt, 8, 1};


/**
8 bit unsigned integer.
*/
const DLDataType dldtype_uint8{kDLUInt, 8, 1};


/**
16 bit signed integer.
*/
const DLDataType dldtype_int16{kDLInt, 16, 1};


/**
16 bit unsigned integer.
*/
const DLDataType dldtype_uint16{kDLUInt, 16, 1};


/**
32 bit signed integer.
*/
const DLDataType dldtype_int32{kDLInt, 32, 1};


/**
32 bit unsigned integer.
*/
const DLDataType dldtype_uint32{kDLUInt, 32, 1};


/**
64 bit signed integer.
*/
const DLDataType dldtype_int64{kDLInt, 64, 1};


/**
64 bit unsigned integer.
*/
const DLDataType dldtype_uint64{kDLUInt, 64, 1};


/**
16 bit (half precision) float.

@note not yet supported
*/
const DLDataType dldtype_float16{kDLFloat, 16, 1};


/**
32 bit (single precision) float.
*/
const DLDataType dldtype_float32{kDLFloat, 32, 1};


/**
64 bit (double precision) float.
*/
const DLDataType dldtype_float64{kDLFloat, 64, 1};


/**
Return a string representation of a \link DLDataType \endlink,
e.g., `DLDataType(kDLUInt, 32, 4)` for 32bit unsigned int with 4 lanes.
The lanes part is omitted if there is only 1 lane.
*/
std::string dldatatype_repr(DLDataType dtype);


/**
Return a concise string representation of a \link DLDataType \endlink,
e.g., `uint32x4` for 32bit unsigned int with 4 lanes.
The lanes part is omitted if there is only 1 lane.
*/
std::string dldatatype_str(DLDataType dtype);


/**
Returns the corresponding \link DLDataType \endlink
for type `scalar_t`

@tparam scalar_t input type
*/
template<typename scalar_t>
inline DLDataType get_dldatatype() {
    if (std::is_same<scalar_t, uint8_t>::value) return dldtype_uint8;
    else if (std::is_same<scalar_t, uint16_t>::value) return dldtype_uint16;
    else if (std::is_same<scalar_t, uint32_t>::value) return dldtype_uint32;
    else if (std::is_same<scalar_t, uint64_t>::value) return dldtype_uint64;
    else if (std::is_same<scalar_t, int8_t>::value) return dldtype_int8;
    else if (std::is_same<scalar_t, int16_t>::value) return dldtype_int16;
    else if (std::is_same<scalar_t, int32_t>::value) return dldtype_int32;
    else if (std::is_same<scalar_t, int64_t>::value) return dldtype_int64;
    else if (std::is_same<scalar_t, float>::value) return dldtype_float32;
    else if (std::is_same<scalar_t, double>::value) return dldtype_float64;
    else throw std::invalid_argument("dtype is not supported");
}


/**
Returns `true` if both data types are the same.
*/
bool dldatatype_equals(DLDataType t1, DLDataType t2);


/**
Augpy's tensor class.
It is a backwards compatible extension to the DLPack.
specification.

@rst
See :ref:`cpp/tensor:DLPack` for the full documentation.
@endrst

It supports all the usual operations you would expect from a
full-featured tensor class, like complex indexing and slicing.

Copy, math, and comparison operations are provided as
separate functions to call on tensors.
*/
struct CudaTensor : DLManagedTensor {
public:
    /**
    Create a new tensor with the given shape, dtype, on a specific device.

    @param shape Pointer to a shape array
    @param ndim number of dimensions, i.e., length of the `shape` array
    @param dtype data type of the new tensor
    @param device_id Cuda GPU device id where tensor memory is allocated
    */
    CudaTensor(int64_t* shape, int ndim, DLDataType dtype, int device_id);

    /**
    Alias for \link CudaTensor(int64_t*, int, DLDataType, int) \endlink
    called with `shape.data()` and `shape.size()`.
    */
    CudaTensor(std::vector<int64_t> shape, DLDataType dtype, int device_id);

    /**
    Create a new tensor that borrows memory from a parent tensor,
    but has a different shape

    @param parent Parent tensor to borrow memory from
    @param ndim number of dimensions of new tensor
    @param shape shape of new tensor, array of length `ndim`
    */
    CudaTensor(CudaTensor* parent, int ndim, int64_t* shape);

    /**
    Create a new tensor that borrows memory from a parent tensor,
    but has a different shape, may stride, and start at a different offset.

    @param parent Parent tensor to borrow memory from
    @param ndim number of dimensions of new tensor
    @param shape shape of new tensor, array of length `ndim`
    @param strides stride distances of the tensor, array of length `ndim`
    @param byte_offset start position in parent memory in bytes
    */
    CudaTensor(CudaTensor* parent, int ndim, int64_t* shape, int64_t* strides, int64_t byte_offset);

    /**
    Create an exact copy of the `parent` tensor, borrowing its memory.
    */
    CudaTensor(CudaTensor* parent);

    /**
    Wrap a \link DLManagedTensor \endlink inside a
    CudaTensor, borrowing its memory.
    */
    CudaTensor(DLManagedTensor* parent);

    /**
    Delete this CudaTensor.
    Calls the \link DLManagedTensor::deleter \endlink function if
    \link DLManagedTensor::manager_ctx \endlink is also set.

    The \link managed_allocation \endlink will be marked as
    orphaned/ready for reuse if this tensor is the last remaining
    tensor that references it.
    */
    ~CudaTensor() noexcept(false);

    /**
    Return a pointer to the first element in this tensor.
    Resolves \link DLTensor::byte_offset \endlink.
    */
    void* ptr();

    /**
    Mark this tensor to be in use by calling
    \link CudaEvent::record \endlink on its event.
    */
    void record();

    /**
    Return the Cuda event used to record
    */
    cudaEvent_t get_event();

    /**
    Returns `true` if the tensor is contiguous, i.e.,
    elements are located next to each other in memory and in
    dimensions are not reversed.
    */
    bool is_contiguous();

    /**
    Index this tensor in the first dimension at index `i`.
    Behaves like numpy indexing, i.e, index from the back if `i` is
    negative where `-1` refers to the last element.
    */
    CudaTensor* index(ssize_t i);

    /**
    Slice this tensor in the first dimension.
    Behaves like numpy slicing, i.e, start, stop, and step
    may be negative.
    */
    CudaTensor* slice_simple(py::slice slice);

    /**
    Slice this tensor in up to \link DLTensor::ndim \endlink
    dimensions.
    Behaves like numpy slicing, i.e, start, stop, and step
    may be negative.
    */
    CudaTensor* slice_complex(py::tuple slices);

    /**
    Read items from `src` and write them into the tensor
    at positions referenced by an `index`.
    */
    void setitem_index(ssize_t index, CudaTensor* src);

    /**
    Read items from `src` and write them into this tensor
    at positions referenced by a `slice`.
    */
    void setitem_simple(py::slice slice, CudaTensor* src);

    /**
    Read items from `src` and write them into this tensor
    at positions referenced by a number of `slices`.
    */
    void setitem_complex(py::tuple slices, CudaTensor* src);

    /**
    Fill this tensor with the given `scalar` value
    at positions referenced by an `index`.
    Supports broadcasting.
    */
    CudaTensor* fill_index(ssize_t index, double scalar);

    /**
    Fill this tensor with the given `scalar` value
    at positions referenced by a `slice`.
    Supports broadcasting.
    */
    CudaTensor* fill_simple(py::slice slice, double scalar);

    /**
    Fill this tensor with the given `scalar` value
    at positions referenced by a number of `slices`.
    Supports broadcasting.
    */
    CudaTensor* fill_complex(py::tuple slices, double scalar);

    /**
    Returns a new tensor with the given shape that
    borrows memory from this tensor.
    Number of elements cannot change and this tensor
    must be contiguous.
    */
    CudaTensor* reshape(std::vector<int64_t> shape);

    /**
    Returns a string representation of this tensor, e.g.,
    `<CudaTensor shape=(1, 2, 3), device=0, dtype=uint8>`.
    */
    std::string repr();

    /**
    Returns the shape of this tensor as a Python tuple.
    */
    py::tuple pyshape();

    /**
    Returns the strides of this tensor as a Python tuple.
    */
    py::tuple pystrides();

private:
    /**
    <a href=
    "https://en.cppreference.com/w/cpp/memory/shared_ptr/shared_ptr"
    >std::shared_ptr</a> that contains the
    \link managed_allocation \endlink of memory for this tensor.
    */
    std::shared_ptr<managed_allocation> alloc;

    /**
    `true` if this tensor is contiguous in memory.
    */
    bool contiguous;

    /**
    Array where the tensor's shape is stored.
    */
    ndim_array shape;

    /**
    Array where the tensor's strides are stored.
    */
    ndim_array strides;

    /**
    Cuda event used to record whether a tensor
    imported from another library is currently in use.
    */
    cudaEvent_t event;
};


/**
Throws `std::invalid_argument` if `t` is `NULL` or not contiguous.
*/
void assert_contiguous(CudaTensor* t);


/**
Compute a fused multiply-add on a scalar and two tensors, i.e.,
\f$r = s \cdot t_1 \cdot t_2\f$.

If `tensor1` has an unsigned integer data type,
then `tensor2` must have the signed version of the same type,
e.g., a `uint8` tensor must be paired with a `int8` tensor.
*/
CudaTensor* fma(
    double scalar,
    CudaTensor* tensor1,
    CudaTensor* tensor2,
    CudaTensor* out,
    unsigned int blocks_per_sm=BLOCKS_PER_SM,
    unsigned int num_threads=0
);


/**
Copy ``dst`` into ``dst``.
Supports broadcasting.

@returns ``dst``
*/
CudaTensor* copy(
        CudaTensor* src,
        CudaTensor* dst,
        unsigned int blocks_per_sm=BLOCKS_PER_SM,
        unsigned int num_threads=0
);


/**
Fill ``dst`` with the given ``scalar`` value.

@returns ``dst``
*/
CudaTensor* fill(
        double scalar,
        CudaTensor* dst,
        unsigned int blocks_per_sm=BLOCKS_PER_SM,
        unsigned int num_threads=0
);


/**
Create a new empty tensor with the same shape and dtype
on the same device.
*/
CudaTensor* empty_like(CudaTensor* tensor);


/**
Returns the number of elements in the tensor with the given shape.
*/
template <typename scalar_t>
inline size_t numel(scalar_t* shape, size_t ndim) {
    size_t n = 1;
    for (size_t i=0; i<ndim; i++) {
        n *= shape[i];
    }
    return n;
}


/**
Returns the number of elements in the tensor with the given shape.
*/
template <typename scalar_t>
inline size_t numel(std::vector<scalar_t> &shape) {
    return numel(shape.data(), shape.size());
}


/**
Returns the number of elements in the tensor.
*/
inline size_t numel(DLTensor* tensor) {
    return numel(tensor->shape, tensor->ndim);
}


/**
Returns the number of elements in the tensor.
*/
inline size_t numel(DLTensor &tensor) {
    return numel(tensor.shape, tensor.ndim);
}


/**
Returns the number of elements in the tensor.
*/
inline size_t numel(CudaTensor* tensor) {
    return numel(tensor->dl_tensor);
}


/**
Returns the number of elements in the array.
*/
inline size_t numel(py::buffer_info &array) {
    return numel(array.shape);
}


/**
Returns the number of bytes in one element of this data type.
*/
inline size_t itemsize(DLDataType &dtype) {
    return (dtype.bits * dtype.lanes + 7) / 8;
}


/**
Returns the number of bytes occupied by this tensor.
*/
inline size_t numbytes(DLTensor* tensor) {
    return numel(tensor) * itemsize(tensor->dtype);
}


/**
Returns the number of bytes occupied by this tensor.
*/
inline size_t numbytes(CudaTensor* tensor) {
    return numbytes(&tensor->dl_tensor);
}


/**
Returns the number of bytes occupied by this tensor.
*/
inline size_t numbytes(py::buffer_info &array) {
    return numel(array.shape) * array.itemsize;
}


/**
Returns `true` if the tensor is contiguous.
*/
bool check_contiguous(DLTensor* tensor);


/**
Returns `true` if the tensor is contiguous.
*/
bool check_contiguous(CudaTensor* tensor);


/**
Returns `true` if the array is contiguous.
*/
bool check_contiguous(py::buffer_info &array);


/**
Returns `true` if `array1[dim] == array2[dim]` for all dimensions
from `dim0` to `ndim-1`.
*/
bool array_equals(int dim0, int ndim, int64_t* array1, int64_t* array2);


/**
Check whether tensor is not `NULL`, has at least a minimum size in bytes,
and is contiguous.


@param tensor tensor to check
@param min_size check whether `numbytes(tensor) >= min_size`
@param contiguous if `true`, check whether tensor is contiguous
                  and return `false` if not
*/
void check_tensor(CudaTensor* tensor, size_t min_size=0, bool contiguous=true);


/**
Check whether `t1` and `t2` are located on the same GPU device.
If not, raise `std::invalid_argument`.
*/
void check_same_device(DLTensor t1, DLTensor t2);


/**
Check whether `t1` and `t2` have the same dtype
and are located on the same GPU device.
If not, raise `std::invalid_argument`.
*/
void check_same_dtype_device(DLTensor t1, DLTensor t2);


/**
Check whether `t1` and `t2` have the same dtype,
are located on the same GPU device,
and have the same shape.
If not, raise `std::invalid_argument`.
*/
void check_same_dtype_device_shape(DLTensor t1, DLTensor t2);


/**
If `threads == 0`, set `threads` to \link cores_per_sm(int) cores_per_sm(device_id)\endlink.
*/
void calc_threads(unsigned int &threads, int device_id);


/**
Use heuristics to calculate how many blocks and values per thread
to use for the given 1D tensor.

Values per thread \f$v\f$ is calculated based on the number of elements
in the tensor `t`, the number of SMs on the device \f$N_{sm}\f$,
the number of blocks per sm \f$B_{sm}\f$, and the number of threads
per block \f$N_{t}\f$:

\f[
v = \left\lceil \frac{numel(t)}{N_{sm} \cdot B_{sm} \cdot N_t} \right\rceil
\f]

The number of blocks \f$B\f$ is then calculated like this:

\f[
B = \left\lceil \frac{\lceil numel(t) / v \rceil}{N_t}\right\rceil
\f]

@param t input tensor to operate on
@param num_blocks output value, number of blocks in the grid
@param num output value, number of elements in t, i.e., `numel(t)`
@param values_per_thread input/output value, if `>0` specifies the
                         values per thread to use, otherwise will
                         hold the calculated value
@param threads number of threads in each block
@param blocks_per_sm how many blocks to generate per SM on the device;
                     defaults to \link BLOCKS_PER_SM \endlink
*/
void calc_blocks_values_1d(
        DLTensor t,
        unsigned int &num_blocks,
        size_t &num,
        unsigned int &values_per_thread,
        unsigned int threads,
        unsigned int blocks_per_sm=BLOCKS_PER_SM
);


/**
Similar to \link calc_blocks_values_1d \endlink, but for ND tensors.
Use heuristics to calculate the size of the block grid and values
per thread to use for the given ND tensor.

For ND tensors, values per thread only applies to the first dimension.
The same heuristics are used, but \f$v\f$ cannot exceed the size
of the first dimension \f$s_0\f$, so \f$v' = min(v, s_0)\f$.
`grid.x` is therefore
\f$\left\lceil \frac{s_0}{v'} \right\rceil\f$
and `grid.y` is
\f$\left\lceil \frac{numel(t)}{s_0\cdot N_t} \right\rceil\f$.

@param t input tensor to operate on
@param grid output value, the block grid used for the kernel launch;
            `grid.x` will hold the number of iterations in the first
            dimension, `grid.y` the number of blocks required for the
            remaining dimensions
@param count output value, number of elements in t starting with
             the second dimension, i.e., `numel(t) / t.shape[0]`
@param values_per_thread input/output value, if `>0` specifies the
                         values per thread to use, otherwise will
                         hold the calculated value
@param threads number of threads in each block
@param blocks_per_sm how many blocks to generate per SM on the device;
                     defaults to \link BLOCKS_PER_SM \endlink
*/
void calc_blocks_values_nd(
        DLTensor t,
        dim3 &grid,
        size_t &count,
        unsigned int &values_per_thread,
        unsigned int threads,
        unsigned int blocks_per_sm=BLOCKS_PER_SM
);


/**
Calculate the strides in number of elements the given
tensor `t` would have if it was contiguous.
*/
void calculate_contiguous_strides(DLTensor t, ndim_array &contiguous_strides);


/**
If possible, calculate the strides that are needed to
broadcast `t_src` to `t_dst`.

Broadcasting is possible if `t_src.ndim <= t_dst.ndim`
and every dimension up to `t_src.ndim` is either the same
or `t_src` shape is 1.
Stride in a broadcastable dimensions is zero.

@param t_src source tensor to broadcast
@param t_dst target tensor to broadcast to
@param src_strides output value, strides required to broadcast
@param t_src_index only used for error message formatting,
                   index of `t_src` in the function call
@returns `true` if broadcasting was used
@throws std::invalid_argument if broadcasting not possible
*/
bool calculate_broadcast_strides(
        DLTensor t_src,
        DLTensor t_dst,
        ndim_array &src_strides,
        const int t_src_index
);


/**
If possible, calculate the output shape when broadcasting
tensors `t1` and `t2` together.
Output shape will have `max(t1.ndim, t2.ndim)` dimensions.
To broadcast, both tensors must either match the size
of a dimension, or one of them must have size 1.
The other tensor then determines the size of the dimension.

@param t1 first tensor
@param t2 second tensor
@param ndim output value, number of dimensions in output
@param shape output value, shape array, must have at least
             length `max(t1.ndim, t2.ndim)`
*/
bool calculate_broadcast_output_shape(
        DLTensor t1,
        DLTensor t2,
        int &ndim,
        int64_t* shape
);


/**
Alias for
\link calculate_broadcast_output_shape(DLTensor, DLTensor, int&, int64_t*) \endlink.
*/
bool calculate_broadcast_output_shape(
        DLTensor t1,
        DLTensor t2,
        int &ndim,
        ndim_array &shape
);


/**
For all tensors in the given array,
get the maximum size in each dimension
and create a new tensor of that shape.

Does not check whether tensors are broadcastable.


@param tensors tensors that will be broadcast to output
@param n_tensors number of tensors in array
@param allow_null if `true`, allow tensors to be `NULL`,
                  otherwise throw `std::invalid_argument`
*/
CudaTensor* create_output_tensor(CudaTensor** tensors, int n_tensors, bool allow_null);


/**
Manipulate the shapes and strides of the given tensors
to coalesce (remove) unnecessary dimensions, thus
simplifying the tensors.

@warning Output is only valid if tensors have strides
         produced by \link calculate_broadcast_strides \endlink.
         Tensors must either have the same shape, or
         be broadcast and thus appear non-contiguous.

A dimension can be coalesced if all tensors are either
contiguous in that dimension or have less dimensions.
*/
void coalesce_dimensions(std::vector<DLTensor> &tensors);


/**
Copy a given tensor to a new numpy array.
This initiates an asynchronous copy from device to host memory.
*/
py::array* tensor_to_array1(CudaTensor* tensor);


/**
Copy a given tensor to a numpy array created from the given buffer `array`.
This initiates an asynchronous copy from device to host memory.
*/
py::array* tensor_to_array2(CudaTensor* tensor, py::buffer* array);


/**
Copy a Python buffer into a new tensor on the specified GPU device.
This initiates an asynchronous copy from host to device memory.
*/
CudaTensor* array_to_tensor1(py::buffer* array, int device_id=0);


/**
Copy a Python buffer to a tensor created from the given buffer `tensor`.
This initiates an asynchronous copy from host to device memory.
*/
CudaTensor* array_to_tensor2(py::buffer* array, CudaTensor* tensor);


/**
Import a GPU tensor from another library into augpy.

@note This requires explicit synchronization if augpy or the
      interfacing library is running operations on streams
      other than the \link default_stream \endlink.

@param tensor_capsule a Python capsule object that contains
                      a \link DLManagedTensor \endlink
@param name name under which the tensor is stored in the capsule,
            e.g., `"dltensor"` for Pytorch
*/
CudaTensor* import_dltensor(py::capsule* tensor_capsule, const char* name);


/**
Export a GPU tensor to be used by another library.

@note This requires explicit synchronization if augpy or the
      interfacing library is running operations on streams
      other than the \link default_stream \endlink.

@param pytensor Python-wrapped CudaTensor
@param name name under which the tensor is stored in the returned
            capsule, e.g., `"dltensor"` for Pytorch
@param destruct if `true`, add a destructor to the capsule
                which will delete the tensor when the capsule
                is deleted; only set to `false` if you know
                what you're doing
*/
py::capsule* export_dltensor(py::object* pytensor, std::string* name, bool destruct);


/**
Read values from `tensor`, cast them to the data type of
`out` and store them there.
`tensor` and `out` must have the same shape.
*/
CudaTensor* cast_tensor(
        CudaTensor* tensor,
        CudaTensor* out,
        unsigned int blocks_per_sm=BLOCKS_PER_SM,
        unsigned int num_threads=0
);


/**
Create a new tensor with values from `tensor`
cast to the given data type `dtype`.
*/
CudaTensor* cast_type(
        CudaTensor* tensor,
        DLDataType dtype,
        unsigned int blocks_per_sm=BLOCKS_PER_SM,
        unsigned int num_threads=0
);


/**
Uses CuBLAS to Calculate the matrix multiplication
of two 2D tensors. More specifically calculates
\f[
C = A \times (\alpha \cdot B) + \beta \cdot C
\f]

Only `float` and `double` data types are supported
and all tensors must have the same data type.
All tensors must be contiguous.

Returns a new tensor if `C` is `NULL`,
otherwise `C` is returned.
*/
CudaTensor* gemm(
        CudaTensor* A,
        CudaTensor* B,
        CudaTensor* C,
        double alpha=1,
        double beta=0
);


/**
Sum all elements in a tensor with saturation.

@param tensor tensor to sum, must be contiguous
@param upcast if `true`, returns temp dtype
@return sum value as scalar tensor
*/
CudaTensor* sum(
    CudaTensor* tensor,
    bool upcast
);


/**
Sum of all elements along an axis in a tensor with saturation.

@param tensor tensor to sum, may be strided
@param axis axis index to sum along
@param keepdim if `true`, keep sum axis dimension with length 1
@param upcast if `true`, returns tensor with temp dtype
@param out output tensor (may be `NULL`), must have
       correct shape
@param blocks_per_sm
@param num_threads
@return new tensor with values summed along axis if
        ``out`` is ``NULL``, else ``out``
*/
CudaTensor* sum_axis(
    CudaTensor* tensor,
    int axis,
    bool keepdim=false,
    bool upcast=false,
    CudaTensor* out=nullptr,
    unsigned int blocks_per_sm=BLOCKS_PER_SM,
    unsigned int num_threads=512
);


/**
Check whether all elements in a tensor are greater zero.

@param tensor tensor to sum, must be contiguous
@return `0` or `1` as scalar `uint8` tensor
*/
CudaTensor* all(CudaTensor* tensor);


/**
Add a `scalar` value to a `tensor`.
*/
CudaTensor* add_scalar(
    CudaTensor* tensor,
    double scalar,
    CudaTensor* out,
    unsigned int blocks_per_sm=BLOCKS_PER_SM,
    unsigned int num_threads=512
);


/**
Subtract a `scalar` value from a `tensor`.
*/
CudaTensor* sub_scalar(
    CudaTensor* tensor,
    double scalar,
    CudaTensor* out,
    unsigned int blocks_per_sm=BLOCKS_PER_SM,
    unsigned int num_threads=512
);


/**
Subtract a `tensor` from a `scalar` value.
*/
CudaTensor* rsub_scalar(
    CudaTensor* tensor,
    double scalar,
    CudaTensor* out,
    unsigned int blocks_per_sm=BLOCKS_PER_SM,
    unsigned int num_threads=512
);


/**
Multiply a `tensor` by a `scalar` value.
*/
CudaTensor* mul_scalar(
    CudaTensor* tensor,
    double scalar,
    CudaTensor* out,
    unsigned int blocks_per_sm=BLOCKS_PER_SM,
    unsigned int num_threads=512
);


/**
Divide a `tensor` by a `scalar` value.
*/
CudaTensor* div_scalar(
    CudaTensor* tensor,
    double scalar,
    CudaTensor* out,
    unsigned int blocks_per_sm=BLOCKS_PER_SM,
    unsigned int num_threads=512
);


/**
Divide a `scalar` value by a `tensor`.
*/
CudaTensor* rdiv_scalar(
    CudaTensor* tensor,
    double scalar,
    CudaTensor* out,
    unsigned int blocks_per_sm=BLOCKS_PER_SM,
    unsigned int num_threads=512
);


/**
`tensor < scalar`.
*/
CudaTensor* lt_scalar(
    CudaTensor* tensor,
    double scalar,
    CudaTensor* out,
    unsigned int blocks_per_sm=BLOCKS_PER_SM,
    unsigned int num_threads=512
);


/**
`tensor <= scalar`.
*/
CudaTensor* le_scalar(
    CudaTensor* tensor,
    double scalar,
    CudaTensor* out,
    unsigned int blocks_per_sm=BLOCKS_PER_SM,
    unsigned int num_threads=512
);


/**
`tensor > scalar`.
*/
CudaTensor* gt_scalar(
    CudaTensor* tensor,
    double scalar,
    CudaTensor* out,
    unsigned int blocks_per_sm=BLOCKS_PER_SM,
    unsigned int num_threads=512
);


/**
`tensor >= scalar`.
*/
CudaTensor* ge_scalar(
    CudaTensor* tensor,
    double scalar,
    CudaTensor* out,
    unsigned int blocks_per_sm=BLOCKS_PER_SM,
    unsigned int num_threads=512
);


/**
`tensor == scalar`.
*/
CudaTensor* eq_scalar(
    CudaTensor* tensor,
    double scalar,
    CudaTensor* out,
    unsigned int blocks_per_sm=BLOCKS_PER_SM,
    unsigned int num_threads=512
);


/**
Add `tensor2` to `tensor1`.
*/
CudaTensor* add_tensor(
    CudaTensor* tensor1,
    CudaTensor* tensor2,
    CudaTensor* out,
    unsigned int blocks_per_sm=BLOCKS_PER_SM,
    unsigned int num_threads=512
);


/**
Subtract `tensor2` from `tensor1`.
*/
CudaTensor* sub_tensor(
    CudaTensor* tensor1,
    CudaTensor* tensor2,
    CudaTensor* out,
    unsigned int blocks_per_sm=BLOCKS_PER_SM,
    unsigned int num_threads=512
);


/**
Multiply `tensor1` by `tensor2`.
*/
CudaTensor* mul_tensor(
    CudaTensor* tensor1,
    CudaTensor* tensor2,
    CudaTensor* out,
    unsigned int blocks_per_sm=BLOCKS_PER_SM,
    unsigned int num_threads=512
);


/**
Divide `tensor1` by `tensor2`.
*/
CudaTensor* div_tensor(
    CudaTensor* tensor1,
    CudaTensor* tensor2,
    CudaTensor* out,
    unsigned int blocks_per_sm=BLOCKS_PER_SM,
    unsigned int num_threads=512
);


/**
`tensor1 < tensor2`.
*/
CudaTensor* lt_tensor(
    CudaTensor* tensor1,
    CudaTensor* tensor2,
    CudaTensor* out,
    unsigned int blocks_per_sm=BLOCKS_PER_SM,
    unsigned int num_threads=512
);


/**
`tensor1 <= tensor2`.
*/
CudaTensor* le_tensor(
    CudaTensor* tensor1,
    CudaTensor* tensor2,
    CudaTensor* out,
    unsigned int blocks_per_sm=BLOCKS_PER_SM,
    unsigned int num_threads=512
);


/**
`tensor1 == tensor2`.
*/
CudaTensor* eq_tensor(
    CudaTensor* tensor1,
    CudaTensor* tensor2,
    CudaTensor* out,
    unsigned int blocks_per_sm=BLOCKS_PER_SM,
    unsigned int num_threads=512
);


// namespace augpy
}


// AUGPY_TENSOR_H
#endif
