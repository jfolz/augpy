/**
@file warp_affine.h

Functions to apply affine transformations on 2D images.
*/


#ifndef AUGPY_WARP_AFFINE_H
#define AUGPY_WARP_AFFINE_H


#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#include <pybind11/stl.h>
#include <vector>
#include "tensor.h"


namespace py = pybind11;


namespace augpy {


/**
Enum whether to scale relative to the
shortest or longest side of the image.
*/
enum WarpScaleMode {
    /**
    Scaling is relative to the shortest side of the image.
    */
    WARP_SCALE_SHORTEST,
    /**
    Scaling is relative to the longest side of the image.
    */
    WARP_SCALE_LONGEST
};


/**
Create a \f$2 \times 3\f$ matrix for a set of affine
transformations.
This matrix is compatible with the
<a href=
"https://docs.opencv.org/3.4/da/d54/group__imgproc__transform.html#ga0203d9ee5fcd28d40dbc4a1ea4451983"
>warpAffine</a> function of OpenCV with the
<a href=
"https://docs.opencv.org/3.4/da/d54/group__imgproc__transform.html#gga5bb5a1fea74ea38e1a5445ca803ff121aa48be1c433186c4eae1ea86aa0ca75ba"
>WARP_INVERSE_MAP</a>
flag set.

Transforms are applied in the following order:

  -# shear
  -# scale & aspect ratio
  -# horizontal & vertical mirror
  -# rotation
  -# horizontal & vertical shift


@param out output buffer that matrix is written to;
           must be a writeable \f$2 \times 3\f$ `float` buffer
@param source_height \f$h_s\f$ height of the image in pixels
@param source_width \f$w_s\f$ width of the image in pixels
@param target_height \f$h_t\f$ height of the output canvas in pixels
@param target_width \f$w_t\f$ width of the output canvas in pixels
@param angle clockwise angle in degrees
             with image center as rotation axis
@param scale scale factor relative to output size;
             1 means fill target height or width wise depending
             on `scale_mode` and whichever is longest/shortest;
             larger values will crop,
             smaller values leave empty space in the output canvas
@param aspect controls the aspect ratio;
              1 means same as input, values greater 1
              increase the width and reduce the height
@param shifty shift the image in y direction (vertical);
              0 centers the image on the output canvas;
              -1 means shift up as much as possible;
              1 means shfit down as much as possible;
              the maximum distance to shift is
              \f$ max(scale \cdot h_s - h_t, h_t - scale \cdot h_s) \f$
@param shiftx same as `shifty`, but in x direction (horizontal)
@param sheary controls up/down shear;
              for every pixel in the x direction move `sheary` pixels
              in y direction
@param shearx same as `sheary` but controls left/right shear
@param hmirror if `true` flip image horizontally
@param vmirror if `true` flip image vertically
@param scale_mode if \link WARP_SCALE_SHORTEST \endlink scale
                  is relative to shortest side;
                  this fills the output canvas, cropping the image
                  if necessary;
                  if \link WARP_SCALE_LONGEST \endlink scale
                  is relative to longest side;
                  this ensures the image is contained inside the
                  output canvas, but leaves empty space
@param max_supersampling upper limit for recommended supersampling
@return recommended supersampling factor for the warp
*/
int make_affine_matrix(
        py::buffer out,
        size_t source_height,
        size_t source_width,
        size_t target_height,
        size_t target_width,
        float angle,
        float scale,
        float aspect,
        float shifty,
        float shiftx,
        float sheary,
        float shearx,
        bool hmirror,
        bool vmirror,
        WarpScaleMode scale_mode,
        int max_supersampling
);


/**
Takes an image in channels-last format \f$ (H, W, C) \f$
and affine warps it into a given output tensor in
channels-first format \f$ (C, H, W) \f$.
Any blank canvas is filled with a background color.
The warp is performed with bi-linear and supersampling.


@param src image tensor
@param dst target tensor
@param matrix \f$2 \times 3\f$ `float` transformation matrix,
              see \link make_affine_matrix \endlink for details
@param background background color to fill empty canvas
@param supersampling supersampling factor, e.g., 3 means
                     9 samples are taken in a \f$3 \times 3\f$ grid
*/
void warp_affine(
        CudaTensor* src,
        CudaTensor* dst,
        py::buffer matrix,
        CudaTensor* background,
        int supersampling
);


// namespace augpy
}


// AUGPY_WARP_AFFINE_H
#endif
