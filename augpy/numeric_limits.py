import numpy as __np


from . import int8
from . import int16
from . import int32
from . import int64
from . import uint8
from . import uint16
from . import uint32
from . import uint64
from . import float32
from . import float64


CAST_LIMITS = {
    (uint8, uint8): (None, None),
    (uint8, __np.uint8): (None, None),
    (__np.uint8, uint8): (None, None),
    (__np.uint8, __np.uint8): (None, None),
    (uint8, uint16): (None, None),
    (uint8, __np.uint16): (None, None),
    (__np.uint8, uint16): (None, None),
    (__np.uint8, __np.uint16): (None, None),
    (uint16, uint8): (None, 255),
    (uint16, __np.uint8): (None, 255),
    (__np.uint16, uint8): (None, 255),
    (__np.uint16, __np.uint8): (None, 255),
    (uint8, uint32): (None, None),
    (uint8, __np.uint32): (None, None),
    (__np.uint8, uint32): (None, None),
    (__np.uint8, __np.uint32): (None, None),
    (uint32, uint8): (None, 255),
    (uint32, __np.uint8): (None, 255),
    (__np.uint32, uint8): (None, 255),
    (__np.uint32, __np.uint8): (None, 255),
    (uint8, uint64): (None, None),
    (uint8, __np.uint64): (None, None),
    (__np.uint8, uint64): (None, None),
    (__np.uint8, __np.uint64): (None, None),
    (uint64, uint8): (None, 255),
    (uint64, __np.uint8): (None, 255),
    (__np.uint64, uint8): (None, 255),
    (__np.uint64, __np.uint8): (None, 255),
    (uint8, int8): (None, 127),
    (uint8, __np.int8): (None, 127),
    (__np.uint8, int8): (None, 127),
    (__np.uint8, __np.int8): (None, 127),
    (int8, uint8): (0, None),
    (int8, __np.uint8): (0, None),
    (__np.int8, uint8): (0, None),
    (__np.int8, __np.uint8): (0, None),
    (uint8, int16): (None, None),
    (uint8, __np.int16): (None, None),
    (__np.uint8, int16): (None, None),
    (__np.uint8, __np.int16): (None, None),
    (int16, uint8): (0, 255),
    (int16, __np.uint8): (0, 255),
    (__np.int16, uint8): (0, 255),
    (__np.int16, __np.uint8): (0, 255),
    (uint8, int32): (None, None),
    (uint8, __np.int32): (None, None),
    (__np.uint8, int32): (None, None),
    (__np.uint8, __np.int32): (None, None),
    (int32, uint8): (0, 255),
    (int32, __np.uint8): (0, 255),
    (__np.int32, uint8): (0, 255),
    (__np.int32, __np.uint8): (0, 255),
    (uint8, int64): (None, None),
    (uint8, __np.int64): (None, None),
    (__np.uint8, int64): (None, None),
    (__np.uint8, __np.int64): (None, None),
    (int64, uint8): (0, 255),
    (int64, __np.uint8): (0, 255),
    (__np.int64, uint8): (0, 255),
    (__np.int64, __np.uint8): (0, 255),
    (uint8, float32): (None, None),
    (uint8, __np.float32): (None, None),
    (__np.uint8, float32): (None, None),
    (__np.uint8, __np.float32): (None, None),
    (float32, uint8): (0, 255),
    (float32, __np.uint8): (0, 255),
    (__np.float32, uint8): (0, 255),
    (__np.float32, __np.uint8): (0, 255),
    (uint8, float64): (None, None),
    (uint8, __np.float64): (None, None),
    (__np.uint8, float64): (None, None),
    (__np.uint8, __np.float64): (None, None),
    (float64, uint8): (0, 255),
    (float64, __np.uint8): (0, 255),
    (__np.float64, uint8): (0, 255),
    (__np.float64, __np.uint8): (0, 255),
    (uint16, uint16): (None, None),
    (uint16, __np.uint16): (None, None),
    (__np.uint16, uint16): (None, None),
    (__np.uint16, __np.uint16): (None, None),
    (uint16, uint32): (None, None),
    (uint16, __np.uint32): (None, None),
    (__np.uint16, uint32): (None, None),
    (__np.uint16, __np.uint32): (None, None),
    (uint32, uint16): (None, 65535),
    (uint32, __np.uint16): (None, 65535),
    (__np.uint32, uint16): (None, 65535),
    (__np.uint32, __np.uint16): (None, 65535),
    (uint16, uint64): (None, None),
    (uint16, __np.uint64): (None, None),
    (__np.uint16, uint64): (None, None),
    (__np.uint16, __np.uint64): (None, None),
    (uint64, uint16): (None, 65535),
    (uint64, __np.uint16): (None, 65535),
    (__np.uint64, uint16): (None, 65535),
    (__np.uint64, __np.uint16): (None, 65535),
    (uint16, int8): (None, 127),
    (uint16, __np.int8): (None, 127),
    (__np.uint16, int8): (None, 127),
    (__np.uint16, __np.int8): (None, 127),
    (int8, uint16): (0, None),
    (int8, __np.uint16): (0, None),
    (__np.int8, uint16): (0, None),
    (__np.int8, __np.uint16): (0, None),
    (uint16, int16): (None, 32767),
    (uint16, __np.int16): (None, 32767),
    (__np.uint16, int16): (None, 32767),
    (__np.uint16, __np.int16): (None, 32767),
    (int16, uint16): (0, None),
    (int16, __np.uint16): (0, None),
    (__np.int16, uint16): (0, None),
    (__np.int16, __np.uint16): (0, None),
    (uint16, int32): (None, None),
    (uint16, __np.int32): (None, None),
    (__np.uint16, int32): (None, None),
    (__np.uint16, __np.int32): (None, None),
    (int32, uint16): (0, 65535),
    (int32, __np.uint16): (0, 65535),
    (__np.int32, uint16): (0, 65535),
    (__np.int32, __np.uint16): (0, 65535),
    (uint16, int64): (None, None),
    (uint16, __np.int64): (None, None),
    (__np.uint16, int64): (None, None),
    (__np.uint16, __np.int64): (None, None),
    (int64, uint16): (0, 65535),
    (int64, __np.uint16): (0, 65535),
    (__np.int64, uint16): (0, 65535),
    (__np.int64, __np.uint16): (0, 65535),
    (uint16, float32): (None, None),
    (uint16, __np.float32): (None, None),
    (__np.uint16, float32): (None, None),
    (__np.uint16, __np.float32): (None, None),
    (float32, uint16): (0, 65535),
    (float32, __np.uint16): (0, 65535),
    (__np.float32, uint16): (0, 65535),
    (__np.float32, __np.uint16): (0, 65535),
    (uint16, float64): (None, None),
    (uint16, __np.float64): (None, None),
    (__np.uint16, float64): (None, None),
    (__np.uint16, __np.float64): (None, None),
    (float64, uint16): (0, 65535),
    (float64, __np.uint16): (0, 65535),
    (__np.float64, uint16): (0, 65535),
    (__np.float64, __np.uint16): (0, 65535),
    (uint32, uint32): (None, None),
    (uint32, __np.uint32): (None, None),
    (__np.uint32, uint32): (None, None),
    (__np.uint32, __np.uint32): (None, None),
    (uint32, uint64): (None, None),
    (uint32, __np.uint64): (None, None),
    (__np.uint32, uint64): (None, None),
    (__np.uint32, __np.uint64): (None, None),
    (uint64, uint32): (None, 4294967295),
    (uint64, __np.uint32): (None, 4294967295),
    (__np.uint64, uint32): (None, 4294967295),
    (__np.uint64, __np.uint32): (None, 4294967295),
    (uint32, int8): (None, 127),
    (uint32, __np.int8): (None, 127),
    (__np.uint32, int8): (None, 127),
    (__np.uint32, __np.int8): (None, 127),
    (int8, uint32): (0, None),
    (int8, __np.uint32): (0, None),
    (__np.int8, uint32): (0, None),
    (__np.int8, __np.uint32): (0, None),
    (uint32, int16): (None, 32767),
    (uint32, __np.int16): (None, 32767),
    (__np.uint32, int16): (None, 32767),
    (__np.uint32, __np.int16): (None, 32767),
    (int16, uint32): (0, None),
    (int16, __np.uint32): (0, None),
    (__np.int16, uint32): (0, None),
    (__np.int16, __np.uint32): (0, None),
    (uint32, int32): (None, 2147483647),
    (uint32, __np.int32): (None, 2147483647),
    (__np.uint32, int32): (None, 2147483647),
    (__np.uint32, __np.int32): (None, 2147483647),
    (int32, uint32): (0, None),
    (int32, __np.uint32): (0, None),
    (__np.int32, uint32): (0, None),
    (__np.int32, __np.uint32): (0, None),
    (uint32, int64): (None, None),
    (uint32, __np.int64): (None, None),
    (__np.uint32, int64): (None, None),
    (__np.uint32, __np.int64): (None, None),
    (int64, uint32): (0, 4294967295),
    (int64, __np.uint32): (0, 4294967295),
    (__np.int64, uint32): (0, 4294967295),
    (__np.int64, __np.uint32): (0, 4294967295),
    (uint32, float32): (None, None),
    (uint32, __np.float32): (None, None),
    (__np.uint32, float32): (None, None),
    (__np.uint32, __np.float32): (None, None),
    (float32, uint32): (0, 4294967295),
    (float32, __np.uint32): (0, 4294967295),
    (__np.float32, uint32): (0, 4294967295),
    (__np.float32, __np.uint32): (0, 4294967295),
    (uint32, float64): (None, None),
    (uint32, __np.float64): (None, None),
    (__np.uint32, float64): (None, None),
    (__np.uint32, __np.float64): (None, None),
    (float64, uint32): (0, 4294967295),
    (float64, __np.uint32): (0, 4294967295),
    (__np.float64, uint32): (0, 4294967295),
    (__np.float64, __np.uint32): (0, 4294967295),
    (uint64, uint64): (None, None),
    (uint64, __np.uint64): (None, None),
    (__np.uint64, uint64): (None, None),
    (__np.uint64, __np.uint64): (None, None),
    (uint64, int8): (None, 127),
    (uint64, __np.int8): (None, 127),
    (__np.uint64, int8): (None, 127),
    (__np.uint64, __np.int8): (None, 127),
    (int8, uint64): (0, None),
    (int8, __np.uint64): (0, None),
    (__np.int8, uint64): (0, None),
    (__np.int8, __np.uint64): (0, None),
    (uint64, int16): (None, 32767),
    (uint64, __np.int16): (None, 32767),
    (__np.uint64, int16): (None, 32767),
    (__np.uint64, __np.int16): (None, 32767),
    (int16, uint64): (0, None),
    (int16, __np.uint64): (0, None),
    (__np.int16, uint64): (0, None),
    (__np.int16, __np.uint64): (0, None),
    (uint64, int32): (None, 2147483647),
    (uint64, __np.int32): (None, 2147483647),
    (__np.uint64, int32): (None, 2147483647),
    (__np.uint64, __np.int32): (None, 2147483647),
    (int32, uint64): (0, None),
    (int32, __np.uint64): (0, None),
    (__np.int32, uint64): (0, None),
    (__np.int32, __np.uint64): (0, None),
    (uint64, int64): (None, 9223372036854775807),
    (uint64, __np.int64): (None, 9223372036854775807),
    (__np.uint64, int64): (None, 9223372036854775807),
    (__np.uint64, __np.int64): (None, 9223372036854775807),
    (int64, uint64): (0, None),
    (int64, __np.uint64): (0, None),
    (__np.int64, uint64): (0, None),
    (__np.int64, __np.uint64): (0, None),
    (uint64, float32): (None, None),
    (uint64, __np.float32): (None, None),
    (__np.uint64, float32): (None, None),
    (__np.uint64, __np.float32): (None, None),
    (float32, uint64): (0, 18446744073709551615),
    (float32, __np.uint64): (0, 18446744073709551615),
    (__np.float32, uint64): (0, 18446744073709551615),
    (__np.float32, __np.uint64): (0, 18446744073709551615),
    (uint64, float64): (None, None),
    (uint64, __np.float64): (None, None),
    (__np.uint64, float64): (None, None),
    (__np.uint64, __np.float64): (None, None),
    (float64, uint64): (0, 18446744073709551615),
    (float64, __np.uint64): (0, 18446744073709551615),
    (__np.float64, uint64): (0, 18446744073709551615),
    (__np.float64, __np.uint64): (0, 18446744073709551615),
    (int8, int8): (None, None),
    (int8, __np.int8): (None, None),
    (__np.int8, int8): (None, None),
    (__np.int8, __np.int8): (None, None),
    (int8, int16): (None, None),
    (int8, __np.int16): (None, None),
    (__np.int8, int16): (None, None),
    (__np.int8, __np.int16): (None, None),
    (int16, int8): (-128, 127),
    (int16, __np.int8): (-128, 127),
    (__np.int16, int8): (-128, 127),
    (__np.int16, __np.int8): (-128, 127),
    (int8, int32): (None, None),
    (int8, __np.int32): (None, None),
    (__np.int8, int32): (None, None),
    (__np.int8, __np.int32): (None, None),
    (int32, int8): (-128, 127),
    (int32, __np.int8): (-128, 127),
    (__np.int32, int8): (-128, 127),
    (__np.int32, __np.int8): (-128, 127),
    (int8, int64): (None, None),
    (int8, __np.int64): (None, None),
    (__np.int8, int64): (None, None),
    (__np.int8, __np.int64): (None, None),
    (int64, int8): (-128, 127),
    (int64, __np.int8): (-128, 127),
    (__np.int64, int8): (-128, 127),
    (__np.int64, __np.int8): (-128, 127),
    (int8, float32): (None, None),
    (int8, __np.float32): (None, None),
    (__np.int8, float32): (None, None),
    (__np.int8, __np.float32): (None, None),
    (float32, int8): (-128, 127),
    (float32, __np.int8): (-128, 127),
    (__np.float32, int8): (-128, 127),
    (__np.float32, __np.int8): (-128, 127),
    (int8, float64): (None, None),
    (int8, __np.float64): (None, None),
    (__np.int8, float64): (None, None),
    (__np.int8, __np.float64): (None, None),
    (float64, int8): (-128, 127),
    (float64, __np.int8): (-128, 127),
    (__np.float64, int8): (-128, 127),
    (__np.float64, __np.int8): (-128, 127),
    (int16, int16): (None, None),
    (int16, __np.int16): (None, None),
    (__np.int16, int16): (None, None),
    (__np.int16, __np.int16): (None, None),
    (int16, int32): (None, None),
    (int16, __np.int32): (None, None),
    (__np.int16, int32): (None, None),
    (__np.int16, __np.int32): (None, None),
    (int32, int16): (-32768, 32767),
    (int32, __np.int16): (-32768, 32767),
    (__np.int32, int16): (-32768, 32767),
    (__np.int32, __np.int16): (-32768, 32767),
    (int16, int64): (None, None),
    (int16, __np.int64): (None, None),
    (__np.int16, int64): (None, None),
    (__np.int16, __np.int64): (None, None),
    (int64, int16): (-32768, 32767),
    (int64, __np.int16): (-32768, 32767),
    (__np.int64, int16): (-32768, 32767),
    (__np.int64, __np.int16): (-32768, 32767),
    (int16, float32): (None, None),
    (int16, __np.float32): (None, None),
    (__np.int16, float32): (None, None),
    (__np.int16, __np.float32): (None, None),
    (float32, int16): (-32768, 32767),
    (float32, __np.int16): (-32768, 32767),
    (__np.float32, int16): (-32768, 32767),
    (__np.float32, __np.int16): (-32768, 32767),
    (int16, float64): (None, None),
    (int16, __np.float64): (None, None),
    (__np.int16, float64): (None, None),
    (__np.int16, __np.float64): (None, None),
    (float64, int16): (-32768, 32767),
    (float64, __np.int16): (-32768, 32767),
    (__np.float64, int16): (-32768, 32767),
    (__np.float64, __np.int16): (-32768, 32767),
    (int32, int32): (None, None),
    (int32, __np.int32): (None, None),
    (__np.int32, int32): (None, None),
    (__np.int32, __np.int32): (None, None),
    (int32, int64): (None, None),
    (int32, __np.int64): (None, None),
    (__np.int32, int64): (None, None),
    (__np.int32, __np.int64): (None, None),
    (int64, int32): (-2147483648, 2147483647),
    (int64, __np.int32): (-2147483648, 2147483647),
    (__np.int64, int32): (-2147483648, 2147483647),
    (__np.int64, __np.int32): (-2147483648, 2147483647),
    (int32, float32): (None, None),
    (int32, __np.float32): (None, None),
    (__np.int32, float32): (None, None),
    (__np.int32, __np.float32): (None, None),
    (float32, int32): (-2147483648, 2147483647),
    (float32, __np.int32): (-2147483648, 2147483647),
    (__np.float32, int32): (-2147483648, 2147483647),
    (__np.float32, __np.int32): (-2147483648, 2147483647),
    (int32, float64): (None, None),
    (int32, __np.float64): (None, None),
    (__np.int32, float64): (None, None),
    (__np.int32, __np.float64): (None, None),
    (float64, int32): (-2147483648, 2147483647),
    (float64, __np.int32): (-2147483648, 2147483647),
    (__np.float64, int32): (-2147483648, 2147483647),
    (__np.float64, __np.int32): (-2147483648, 2147483647),
    (int64, int64): (None, None),
    (int64, __np.int64): (None, None),
    (__np.int64, int64): (None, None),
    (__np.int64, __np.int64): (None, None),
    (int64, float32): (None, None),
    (int64, __np.float32): (None, None),
    (__np.int64, float32): (None, None),
    (__np.int64, __np.float32): (None, None),
    (float32, int64): (-9223372036854775808, 9223372036854775807),
    (float32, __np.int64): (-9223372036854775808, 9223372036854775807),
    (__np.float32, int64): (-9223372036854775808, 9223372036854775807),
    (__np.float32, __np.int64): (-9223372036854775808, 9223372036854775807),
    (int64, float64): (None, None),
    (int64, __np.float64): (None, None),
    (__np.int64, float64): (None, None),
    (__np.int64, __np.float64): (None, None),
    (float64, int64): (-9223372036854775808, 9223372036854775807),
    (float64, __np.int64): (-9223372036854775808, 9223372036854775807),
    (__np.float64, int64): (-9223372036854775808, 9223372036854775807),
    (__np.float64, __np.int64): (-9223372036854775808, 9223372036854775807),
    (float32, float32): (None, None),
    (float32, __np.float32): (None, None),
    (__np.float32, float32): (None, None),
    (__np.float32, __np.float32): (None, None),
    (float32, float64): (None, None),
    (float32, __np.float64): (None, None),
    (__np.float32, float64): (None, None),
    (__np.float32, __np.float64): (None, None),
    (float64, float32): (-340282346638528859811704183484516925440, 340282346638528859811704183484516925440),
    (float64, __np.float32): (-340282346638528859811704183484516925440, 340282346638528859811704183484516925440),
    (__np.float64, float32): (-340282346638528859811704183484516925440, 340282346638528859811704183484516925440),
    (__np.float64, __np.float32): (-340282346638528859811704183484516925440, 340282346638528859811704183484516925440),
    (float64, float64): (None, None),
    (float64, __np.float64): (None, None),
    (__np.float64, float64): (None, None),
    (__np.float64, __np.float64): (None, None),
}
