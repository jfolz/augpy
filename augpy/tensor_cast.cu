#include <cuda.h>
#include <cuda_runtime.h>
#include "tensor.h"
#include "dispatch.h"
#include "elementwise.cuh"


namespace augpy {


template<typename src_t>
__device__ __forceinline__ void cast_function(
        const array<tensor_param, 2> &tensors,
        const DLDataType &dst_type
){
    const src_t v = tensors[1].load<src_t>();
    __AUGPY_DISPATCH_NOEXC(dst_type, dst_t, "cast", [&] {
        tensors[0].store<src_t, dst_t>(v);
    });
}


CudaTensor* cast_tensor(
        CudaTensor* src,
        CudaTensor* dst,
        unsigned int blocks_per_sm,
        unsigned int num_threads
){
    if (!src || !dst) {
        throw std::invalid_argument("source and result tensor need to be valid tensors");
    }
    DLTensor &t_src = src->dl_tensor;
    DLTensor &t_dst = dst->dl_tensor;
    if (t_src.ndim != t_dst.ndim || !array_equals(0, t_src.ndim, t_src.shape, t_dst.shape)) {
        throw std::invalid_argument("source and result tensor must have same shape");
    }
    auto tensors = make_array(dst, src);
    __AUGPY_DISPATCH(t_src.dtype, src_t, "cast", ([&] {
        elementwise_function<2, DLDataType, cast_function<src_t>>(
            tensors, t_dst.dtype, blocks_per_sm, num_threads, false
        );
    }));
    return dst;
}


CudaTensor* cast_type(
        CudaTensor* tensor,
        DLDataType dtype,
        unsigned int blocks_per_sm,
        unsigned int num_threads
){
    DLTensor &t = tensor->dl_tensor;
    CudaTensor* out = new CudaTensor(&t.shape[0], t.ndim, dtype, t.ctx.device_id);
    cast_tensor(tensor, out, blocks_per_sm, num_threads);
    return out;
}


// namespace augpy
}
