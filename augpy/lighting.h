#ifndef AUGPY_GAMMA_H
#define AUGPY_GAMMA_H


#include "tensor.h"


namespace augpy {


/**
Apply lighting augmentation to a batch of images.
This is a four-step process:

  -# Normalize values \f$ v_{norm} = \frac{v-v_{min}}{v_{max}-v_{min}} \f$
     with \f$v_{max}\f$ the maximum lightness value
  -# Apply contrast change
  -# Apply gamma correction
  -# Denormalize values \f$ v' = v_{norm} * (v_{max}-v_{min}) + v_{min} \f$

To change contrast two reference functions are used.
With contrast \f$ \mathcal{c} \ge 0 \f$, i.e., increased contrast,
the following function is used:

\f[
    f_{pos}(v) =
    \frac{1.0037575963899724}{1 + exp(6.279 + v \cdot 12.558)} - 0.0018787981949862
\f]

With contrast \f$ \mathcal{c} < 0 \f$, i.e., decreased contrast,
the following function is used:

\f[
    f_{neg}(v) =
    0.1755606108304832 \cdot atanh(v \cdot 1.986608 - 0.993304) + 0.5
\f]

The final value is
\f$ v' = (1-\mathcal{c}) \cdot v + \mathcal{c} \cdot f(v) \f$.

Brightness and color changes are done via gamma correction.

\f[
    v' = v^{\gamma_{gray} \cdot \gamma_c}
\f]

with \f$\gamma_{gray}\f$ the gamma for overall lightness and
\f$\gamma_{c}\f$ the per-channel gamma.


@param tensor image tensor in \f$ (N,C,H,W) \f$ format
@param gammagrays tensor of \f$ N \f$ gamma gray values
@param gammacolors tensor of \f$ C\cdot N \f$ gamma values in the format
                   \f$ \gamma_{1,1}, \gamma_{1,2}, ..., \gamma_{1,C},
                   \gamma_{2,1}, \gamma_{2,2}, ... \f$
@param contrasts tensor of \f$ N \f$ contrast values in \f$ [-1, 1] \f$
@param vmin minimum lightness value in images
@param vmax maximum lightness value in images
@param out output tensor (may be `NULL`)
@return new tensor if `out` is `NULL`, else `out`
*/
CudaTensor* lighting(
    CudaTensor* tensor,
    CudaTensor* gammagrays,
    CudaTensor* gammacolors,
    CudaTensor* contrasts,
    double vmin,
    double vmax,
    CudaTensor* out
);


// namespace augpy
}


// AUGPY_GAMMA_H
#endif
