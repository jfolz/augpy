#ifndef AUGPY_EXCEPTION_H
#define AUGPY_EXCEPTION_H


#include <cstring>
#include <cuda_runtime.h>
#include <curand.h>
#include "cnmem.h"
#include <nvjpeg.h>
#include <cutlass/cutlass.h>


namespace augpy {


/**
Wrap a function that returns <a href=
"https://docs.nvidia.com/cuda/cuda-runtime-api/group__CUDART__TYPES.html#group__CUDART__TYPES_1g3f51e3575c2178246db0a94a430e0038"
>cudaError_t</a> and throw a \link augpy::cuda_error cuda_error \endlink if not ``cudaSuccess``.
*/
#ifndef CUDA
#define CUDA(a) {cudaError_t _e = a; if(_e!=cudaSuccess){throw cuda_error(_e);}}
#endif


/**
Wrap a function that returns \link cnmemStatus_t \endlink
and throw a
\link augpy::cnmem_error cnmem_error \endlink if not ``CNMEM_STATUS_SUCCESS``.
*/
#ifndef CNMEM
#define CNMEM(a) {cnmemStatus_t _e = a; if(_e!=CNMEM_STATUS_SUCCESS){throw cnmem_error(_e);}}
#endif


/**
Wrap a function that returns <a href=
"https://docs.nvidia.com/cuda/nvjpeg/nvjpeg-api-reference.html#nvjpeg-api-return-codes"
>nvjpegStatus_t</a> and throw a
\link augpy::nvjpeg_error nvjpeg_error \endlink if not ``NVJPEG_STATUS_SUCCESS``.
*/
#ifndef NVJPEG
#define NVJPEG(a) {nvjpegStatus_t _e = a; if(_e!=NVJPEG_STATUS_SUCCESS){throw nvjpeg_error(_e);}}
#endif


/**
Wrap a function that returns <a href=
"https://docs.nvidia.com/cuda/curand/group__HOST.html#group__HOST_1gb94a31d5c165858c96b6c18b70644437"
>curandStatus_t</a> and throw a
\link augpy::curand_error curand_error \endlink if not ``CURAND_STATUS_SUCCESS``.
*/
#ifndef CURAND
#define CURAND(a) {curandStatus_t _e = a; if(_e!=CURAND_STATUS_SUCCESS){throw curand_error(_e);}}
#endif


/**
Wrap a function that returns <a href=
"https://nvidia.github.io/cutlass/namespacecutlass.html#ac5a88c5840a28a9e0206b9cc7812a18d"
>cutlass::Status</a> and throw a
\link augpy::cutlass_error cutlass_error \endlink if not ``cutlass::Status::kSuccess``.
*/
#ifndef CUTLASS
#define CUTLASS(a) {cutlass::Status _e = a; if(_e!=cutlass::Status::kSuccess){throw cutlass_error(_e);}}
#endif


/**
C++ exception for Cuda errors.
*/
class cuda_error : std::exception{
public:
    /**
    Create exception from error code.
    */
    cuda_error(cudaError_t error) : code(error) {}
    /**
    Text description of error.
    */
    virtual const char* what() const noexcept{
        return cudaGetErrorString (code);
    }
private:
    cudaError_t code;
};


/**
C++ exception for cnmem errors.
*/
class cnmem_error : std::exception{
public:
    /**
    Create exception from error code.
    */
    cnmem_error(cnmemStatus_t error) : code(error) {}
    /**
    Text description of error.
    */
    virtual const char* what() const noexcept{
        return cnmemGetErrorString(code);
    }
private:
    cnmemStatus_t code;
};


/**
C++ exception for nvjpeg errors.
*/
class nvjpeg_error : std::exception{
public:
    /**
    Create exception from error code.
    */
    nvjpeg_error(nvjpegStatus_t error) : code(error) {}
    /**
    Text description of error.
    */
    virtual const char* what() const noexcept{
        switch(code){
        case NVJPEG_STATUS_NOT_INITIALIZED:
            return "nvjpeg returned \'NVJPEG_STATUS_NOT_INITIALIZED\'";
        case NVJPEG_STATUS_INVALID_PARAMETER:
            return "nvjpeg returned \'NVJPEG_STATUS_INVALID_PARAMETER\'";
        case NVJPEG_STATUS_BAD_JPEG:
            return "nvjpeg returned \'NVJPEG_STATUS_BAD_JPEG\'";
        case NVJPEG_STATUS_JPEG_NOT_SUPPORTED:
            return "nvjpeg returned \'NVJPEG_STATUS_JPEG_NOT_SUPPORTED\'";
        case NVJPEG_STATUS_ALLOCATOR_FAILURE:
            return "nvjpeg returned \'NVJPEG_STATUS_ALLOCATOR_FAILURE\'";
        case NVJPEG_STATUS_EXECUTION_FAILED:
            return "nvjpeg returned \'NVJPEG_STATUS_EXECUTION_FAILED\'";
        case NVJPEG_STATUS_ARCH_MISMATCH:
            return "nvjpeg returned \'NVJPEG_STATUS_ARCH_MISMATCH\'";
        case NVJPEG_STATUS_INTERNAL_ERROR:
            return "nvjpeg returned \'NVJPEG_STATUS_INTERNAL_ERROR\'";
        case NVJPEG_STATUS_IMPLEMENTATION_NOT_SUPPORTED:
            return "nvjpeg returned \'NVJPEG_STATUS_IMPLEMENTATION_NOT_SUPPORTED\'";
        case NVJPEG_STATUS_SUCCESS:
            return "nvjpeg returned no error \'NVJPEG_STATUS_SUCCESS\' THIS IS A BUG";
        default:
            return "UNKNOWN NVJPEG ERROR! THIS IS A BUG!";
        }
    }

private:
    nvjpegStatus_t code;
};


/**
C++ exception for CuRand errors.
*/
class curand_error : std::exception {
public:
    /**
    Create exception from error code.
    */
    curand_error(curandStatus_t error) {
        this->code = error;
    }
    /**
    Text description of error.
    */
    virtual const char* what() const noexcept{
        switch (code) {
        case CURAND_STATUS_SUCCESS:
            return "curand returned no error \'CURAND_STATUS_SUCCESS\' THIS IS A BUG";
        case CURAND_STATUS_VERSION_MISMATCH:
            return "curand returned \'CURAND_STATUS_VERSION_MISMATCH\'";
        case CURAND_STATUS_NOT_INITIALIZED:
            return "curand returned \'CURAND_STATUS_NOT_INITIALIZED\'";
        case CURAND_STATUS_ALLOCATION_FAILED:
            return "curand returned \'CURAND_STATUS_ALLOCATION_FAILED\'";
        case CURAND_STATUS_TYPE_ERROR:
            return "curand returned \'CURAND_STATUS_TYPE_ERROR\'";
        case CURAND_STATUS_OUT_OF_RANGE:
            return "curand returned \'CURAND_STATUS_OUT_OF_RANGE\'";
        case CURAND_STATUS_LENGTH_NOT_MULTIPLE:
            return "curand returned \'CURAND_STATUS_LENGTH_NOT_MULTIPLE\'";
        case CURAND_STATUS_DOUBLE_PRECISION_REQUIRED:
            return "curand returned \'CURAND_STATUS_DOUBLE_PRECISION_REQUIRED\'";
        case CURAND_STATUS_LAUNCH_FAILURE:
            return "curand returned \'CURAND_STATUS_LAUNCH_FAILURE\'";
        case CURAND_STATUS_PREEXISTING_FAILURE:
            return "curand returned \'CURAND_STATUS_PREEXISTING_FAILURE\'";
        case CURAND_STATUS_INITIALIZATION_FAILED:
            return "curand returned \'CURAND_STATUS_INITIALIZATION_FAILED\'";
        case CURAND_STATUS_ARCH_MISMATCH:
            return "curand returned \'CURAND_STATUS_ARCH_MISMATCH\'";
        case CURAND_STATUS_INTERNAL_ERROR:
            return "curand returned \'CURAND_STATUS_INTERNAL_ERROR\'";
        default:
            return "UNKNOWN CURAND ERROR! THIS IS A BUG!";
        }
    }

private:
    curandStatus_t code;
};


/**
C++ exception for CuBlas errors.
*/
class cutlass_error : std::exception{
public:
    /**
    Create exception from error code.
    */
    cutlass_error(cutlass::Status error) : code(error) {}
    /**
    Text description of error.
    */
    virtual const char* what() const noexcept{
        return cutlass::cutlassGetStatusString(code);
    }
private:
    cutlass::Status code;
};


// namespace augpy
}


// AUGPY_EXCEPTION_H
#endif
