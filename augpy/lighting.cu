#include <cuda.h>
#include <cuda_runtime.h>
#include "cub/thread/thread_load.cuh"
#include "cub/thread/thread_store.cuh"
#include "core.h"
#include "tensor.h"
#include "exception.h"
#include "dispatch.h"
#include "saturate_cast.cuh"
#include "lighting.h"


namespace augpy {


#define __MAX_LUT_SIZE 1024
#define __MAX_CHANNELS 4


const cub::CacheLoadModifier LOAD_MOD = cub::LOAD_LDG;
const cub::CacheStoreModifier STORE_MOD = cub::STORE_DEFAULT;


template<typename temp_t>
__device__ temp_t __exp(temp_t v);


template<>
__device__ float __exp(float v) {
    return expf(v);
}


template<>
__device__ double __exp(double v) {
    return exp(v);
}


template<typename temp_t>
__device__ __forceinline__ temp_t __contrast_positive(temp_t w){
    /* Original code:
    temp_t a = 6.279;
    temp_t b = -6.279;
    temp_t max = 0.9981282;
    temp_t min = 0.0018718;
    temp_t lut = 1 / (1 + __exp(a+w*(b-a)));
    return (lut-min)/(max-min);

    Optimized algebraic to:
    */
    return (temp_t)1.0037575963899724
         / (1 + __exp((temp_t)6.279 - w * 12.558))
         - (temp_t)0.0018787981949862;
}


template<typename temp_t>
__device__ __forceinline__ temp_t __contrast_negative(temp_t w){
    /* Original code:
    temp_t a = 3.348e-03;
    temp_t b = 1 - 3.348e-03;
    temp_t max = 5.69603851;
    temp_t min = -5.69603851;
    temp_t lut = 2 * atanh(2 * (a+w*(b-a)) - 1);
    return (lut-min)/(max-min);

    Optimized algebraic to:
    */
    return (temp_t)0.1755606108304832
         * atanh(w * (temp_t)1.986608 - (temp_t)0.993304)
         + (temp_t)0.5;
}


template <typename scalar_t, typename temp_t>
__global__ void lighting_kernel(
        scalar_t* src,
        scalar_t* dst,
        const float* const gamma_grays,
        const float* const gamma_colors,
        const float* const contrasts,
        const size_t C,
        const size_t count,
        const scalar_t vmin,
        const scalar_t delta,
        const unsigned int values_per_thread
){
    // contrast and gray gamma for current image
    const float contrast = contrasts[blockIdx.x];
    const float gray = gamma_grays[blockIdx.x];
    const temp_t one_by = (temp_t)1.0 / (temp_t)delta;
    // first pixel in current image for this thread
    size_t idx = blockIdx.y * blockDim.x * values_per_thread + threadIdx.x;
    size_t last_idx = min(idx + blockDim.x * values_per_thread, C*count);
    // advance src and dst to first pixel in image
    src += blockIdx.x * C * count;
    dst += blockIdx.x * C * count;
    // C is not a valid channel index,
    // so this will trigger loading of gamma_colors value
    size_t last_c = C;
    float gamma_color;
    for( ; idx<last_idx; idx+=blockDim.x){
        size_t c = idx / count;
        if (c != last_c) {
            gamma_color = gamma_colors[C*blockIdx.x+c];
        }
        last_c = c;
        scalar_t v = min(max(cub::ThreadLoad<LOAD_MOD>(src + idx), vmin) - vmin, delta);
        temp_t fraction = (temp_t)v * one_by;
        temp_t baselookup = fraction;
        temp_t contrastlookup = 0;
        temp_t base = 0;
        if(contrast < 0){
            contrastlookup = __contrast_negative(fraction);
            base = (1+contrast)*baselookup - contrast*contrastlookup;
        }
        else{
            contrastlookup = __contrast_positive(fraction);
            base = (1-contrast)*baselookup + contrast*contrastlookup;
        }
        cub::ThreadStore<STORE_MOD>(
            dst + idx,
            saturate_cast<temp_t, scalar_t>(powf(base, gray*gamma_color) * delta + vmin)
        );
    }
}


template<typename scalar_t, typename temp_t>
__global__ void lighting_kernel_lut(
        scalar_t* src,
        scalar_t* dst,
        const float* gamma_grays,
        const float* gamma_colors,
        const float* contrasts,
        const size_t C,
        const size_t count,
        const scalar_t vmin,
        const scalar_t delta,
        const unsigned int values_per_thread
){
    // contrast and gray gamma for current image
    const float gamma_gray = gamma_grays[blockIdx.x];
    const float contrast = contrasts[blockIdx.x];
    const temp_t one_by = (temp_t)1.0 / (temp_t)delta;
    // prepare lookup table in shared memory
    extern __shared__ scalar_t lut[];
    for (size_t c=0; c<C; ++c) {
        float gamma_color = gamma_colors[C * blockIdx.x + c];
        unsigned short lidx = threadIdx.x;
        for ( ; lidx<=delta; lidx+=blockDim.x) {
            temp_t fraction = (temp_t)lidx * one_by;
            if(contrast < 0){
                fraction = (1 + contrast) * fraction
                         - contrast * __contrast_negative(fraction);
            }
            else{
                fraction = (1 - contrast) * fraction
                         + contrast * __contrast_positive(fraction);
            }
            lut[(delta+1)*c+lidx] = saturate_cast<temp_t, scalar_t>(
                powf(fraction, gamma_gray * gamma_color) * delta
            ) + vmin;
        }
    }

    __syncthreads();

    // first pixel in current image for this thread
    size_t idx = blockIdx.y * blockDim.x * values_per_thread + threadIdx.x;
    size_t last_idx = min(idx + blockDim.x * values_per_thread, C*count);
    // advance src to first pixel for this thread
    src += blockIdx.x * C * count;
    dst += blockIdx.x * C * count;
    // loop until last index
    for( ; idx<last_idx; idx+=blockDim.x){
        scalar_t v = min(max(cub::ThreadLoad<LOAD_MOD>(src + idx), vmin) - vmin, delta);
        cub::ThreadStore<STORE_MOD>(dst + idx, lut[(delta+1) * (idx/count) + v]);
    }
}


CudaTensor* lighting(
    CudaTensor* tensor,
    CudaTensor* gammagrays,
    CudaTensor* gammacolors,
    CudaTensor* contrasts,
    double vmin,
    double vmax,
    CudaTensor* out
){
    // TODO check vmax > vmin
    // TODO check vmax and vmin representable in scalar_t
    assert_contiguous(tensor);
    assert_contiguous(gammagrays);
    assert_contiguous(gammacolors);
    assert_contiguous(contrasts);

    if (!out) {
        out = empty_like(tensor);
    }

	DLTensor &im_tensor = tensor->dl_tensor;
    int64_t N = im_tensor.shape[0];
    int64_t C = im_tensor.shape[1];
    int64_t H = im_tensor.shape[2];
    int64_t W = im_tensor.shape[3];

    check_same_dtype_device(im_tensor, out->dl_tensor);

    unsigned int num_threads = 0;
    calc_threads(num_threads, im_tensor.ctx.device_id);
    cudaDeviceProp props = get_device_properties(im_tensor.ctx.device_id);
    unsigned int blocks_per_sm = 8;
    unsigned int num_blocks = ceil_div(props.multiProcessorCount * blocks_per_sm, N);
    int64_t count = H*W;
    int64_t vpt = ceil_div(C*count, num_blocks * num_threads);
    unsigned int values_per_thread = min(
        (int64_t)std::numeric_limits<unsigned int>().max(),
        vpt
    );
    num_blocks = ceil_div(ceil_div(C*count, values_per_thread), num_threads);
    dim3 grid(N, num_blocks);

    if((im_tensor.dtype.code == kDLUInt || im_tensor.dtype.code == kDLInt)
       && C <= __MAX_CHANNELS && vmax - vmin < __MAX_LUT_SIZE) {
        AUGPY_DISPATCH_I(im_tensor.dtype, "lighting_kernel_lut", [&] {
            const scalar_t delta = (scalar_t)vmax - (scalar_t)vmin;
            lighting_kernel_lut<unsigned char, float>
            <<<grid, num_threads, sizeof(scalar_t)*(delta+1)*C, current_stream>>>(
                (unsigned char*)tensor->ptr(),
                (unsigned char*)out->ptr(),
                (float*)gammagrays->ptr(),
                (float*)gammacolors->ptr(),
                (float*)contrasts->ptr(),
                C, count, (scalar_t)vmin, delta, values_per_thread
            );
        });
    }
    else {
        AUGPY_DISPATCH(im_tensor.dtype, "lighting_kernel", [&] {
            const scalar_t delta = (scalar_t)vmax - (scalar_t)vmin;
            lighting_kernel<scalar_t, temp_t>
            <<<grid, num_threads, 0, current_stream>>>(
                (scalar_t*)tensor->ptr(),
                (scalar_t*)out->ptr(),
                (float*)gammagrays->ptr(),
                (float*)gammacolors->ptr(),
                (float*)contrasts->ptr(),
                C, count, (scalar_t)vmin, delta, values_per_thread
            );
        });
    }

    CUDA(cudaGetLastError());

    // mark tensors as in use
    tensor->record();
    gammagrays->record();
    gammacolors->record();
    contrasts->record();
    out->record();

    return out;
}


// namespace augpy
}
