/**
\file dispatch.h

A collection of macros to dispatch function calls on tensors
with different sets of dtpyes.
See \link DLDataType \endlink for details on dtypes.

All dtypes with 8 or 16 bits can use ``float`` as temporary
dtype ``temp_t``.
Dtypes with 32 or 64 bits must use ``double``.
*/


// dispatch.h
#ifndef DISPATCH_H
#define DISPATCH_H


#include <stdio.h>
#include "dlpack.h"


namespace augpy {


/**
Add a switch case to dispatch a dtype.

@param BITS number of bits in dtype
@param SCALAR_T_NAME name of the declared dtype, e.g., ``scalar_t``
@param SCALAR ``scalar_t`` dtype of the tensor
@param SSCALAR signed version of ``scalar_t`` dtype
@param TEMP ``temp_t`` dtype that can be used as intermediate
            value for calculations of ``scalar_t``
@param ... code to execute for this dtype
*/
#define __AUGPY_DISPATCH_CASE(BITS, SCALAR_T_NAME, SCALAR, SSCALAR, TEMP, ...) \
        case BITS: {                                                           \
            using SCALAR_T_NAME = SCALAR;                                      \
            using sscalar_t = SSCALAR;                                         \
            using temp_t = TEMP;                                               \
            __VA_ARGS__();                                                     \
            break;                                                             \
        }


/**
Dispatch the ``float`` dtype.
Declares tensor dtype under given name,
``float`` as ``sscalar_t``,
and ``float`` as ``temp_t``.

@param SCALAR_T_NAME name of the declared dtype, e.g., ``scalar_t``
@param NAME name of the dispatched function; used when exceptions are thrown
@param ... code to execute for ``float``
*/
#define __AUGPY_DISPATCH_FLOAT(SCALAR_T_NAME, NAME, ...)                            \
        __AUGPY_DISPATCH_CASE(32, SCALAR_T_NAME, float, float, float, __VA_ARGS__);


/**
Dispatch the ``double`` dtype.
Declares ``double`` under given name,
``double`` as ``sscalar_t``,
and ``double`` as ``temp_t``.

@param SCALAR_T_NAME name of the declared dtype, e.g., ``scalar_t``
@param NAME name of the dispatched function; used when exceptions are thrown
@param ... code to execute for ``float``
*/
#define __AUGPY_DISPATCH_DOUBLE(SCALAR_T_NAME, NAME, ...)                              \
        __AUGPY_DISPATCH_CASE(64, SCALAR_T_NAME, double, double, double, __VA_ARGS__);


/**
Dispatch ``unsigned int`` dtypes with 8 or 16 bits.
Declares tensor dtype under given name,
signed version of dtype as ``sscalar_t``,
and ``float`` as ``temp_t``.

@param SCALAR_T_NAME name of the declared dtype, e.g., ``scalar_t``
@param NAME name of the dispatched function; used when exceptions are thrown
@param ... code to execute for ``float``
*/
#define __AUGPY_DISPATCH_UINT_F(SCALAR_T_NAME, NAME, ...)                               \
        __AUGPY_DISPATCH_CASE(8, SCALAR_T_NAME,uint8_t, int8_t, float, __VA_ARGS__);    \
        __AUGPY_DISPATCH_CASE(16, SCALAR_T_NAME,uint16_t, int16_t, float, __VA_ARGS__);


/**
Dispatch ``unsigned int`` dtypes with 32 or 64 bits.
Declares tensor dtype under given name,
signed version of dtype as ``sscalar_t``,
and ``double`` as ``temp_t``.

@param SCALAR_T_NAME name of the declared dtype, e.g., ``scalar_t``
@param NAME name of the dispatched function; used when exceptions are thrown
@param ... code to execute for ``float``
*/
#define __AUGPY_DISPATCH_UINT_D(SCALAR_T_NAME, NAME, ...)                                 \
        __AUGPY_DISPATCH_CASE(32, SCALAR_T_NAME, uint32_t, int32_t, double, __VA_ARGS__); \
        __AUGPY_DISPATCH_CASE(64, SCALAR_T_NAME, uint64_t, int64_t, double, __VA_ARGS__);


/**
Dispatch ``int`` dtypes with 8 or 16 bits.
Declares tensor dtype under given name,
signed version of dtype as ``sscalar_t``,
and ``float`` as ``temp_t``.

@param SCALAR_T_NAME name of the declared dtype, e.g., ``scalar_t``
@param NAME name of the dispatched function; used when exceptions are thrown
@param ... code to execute for ``float``
*/
#define __AUGPY_DISPATCH_INT_F(SCALAR_T_NAME, NAME, ...)                                \
        __AUGPY_DISPATCH_CASE(8, SCALAR_T_NAME, int8_t, int8_t, float, __VA_ARGS__);    \
        __AUGPY_DISPATCH_CASE(16, SCALAR_T_NAME, int16_t, int16_t, float, __VA_ARGS__); \


/**
Dispatch ``int`` dtypes with 32 or 64 bits.
Declares tensor dtype under given name,
signed version of dtype as ``sscalar_t``,
and ``double`` as ``temp_t``.

@param SCALAR_T_NAME name of the declared dtype, e.g., ``scalar_t``
@param NAME name of the dispatched function; used when exceptions are thrown
@param ... code to execute for ``float``
*/
#define __AUGPY_DISPATCH_INT_D(SCALAR_T_NAME, NAME, ...)                                 \
        __AUGPY_DISPATCH_CASE(32, SCALAR_T_NAME, int32_t, int32_t, double, __VA_ARGS__); \
        __AUGPY_DISPATCH_CASE(64, SCALAR_T_NAME, int64_t, int32_t, double, __VA_ARGS__);


/**
Throw ``std::invalid_argument`` if tensor cannot be dispatched.

@param NAME name of the dispatched function
@param CODE dtype code, see \link DLDataTypeCode \endlink
@param BITS size of dtype in bits
*/
#define __AUGPY_THROW(NAME, CODE, BITS)                                              \
    char msg[512];                                                                   \
    switch(CODE){                                                                    \
        case kDLInt:                                                                 \
            sprintf(msg, "failed to dispatch \"%s\" for dtype int%d", NAME, BITS);   \
            break;                                                                   \
        case kDLUInt:                                                                \
            sprintf(msg, "failed to dispatch \"%s\" for dtype uint%d", NAME, BITS);  \
            break;                                                                   \
        case kDLFloat:                                                               \
            sprintf(msg, "failed to dispatch \"%s\" for dtype float%d", NAME, BITS); \
            break;                                                                   \
    }                                                                                \
    throw std::invalid_argument(msg);


/**
Dispatch all dtypes.
Declares tensor dtype under given name,
signed version of dtype as ``sscalar_t``,
and ``float`` (8, 16 bits) ``double`` (32, 64 bits) as ``temp_t``.

@param TYPE tensor dtype, see \link DLDataType \endlink
@param SCALAR_T_NAME name of the declared dtype, e.g., ``scalar_t``
@param NAME name of the dispatched function; used when exceptions are thrown
@param ... code to execute
*/
#define __AUGPY_DISPATCH(TYPE, SCALAR_T_NAME, NAME, ...){                  \
    uint8_t code = TYPE.code;                                              \
    unsigned char bits = TYPE.bits ;                                       \
    switch(code){                                                          \
        case kDLInt:                                                       \
            switch(bits){                                                  \
                __AUGPY_DISPATCH_INT_F(SCALAR_T_NAME, NAME, __VA_ARGS__);  \
                __AUGPY_DISPATCH_INT_D(SCALAR_T_NAME, NAME, __VA_ARGS__);  \
                default: __AUGPY_THROW(NAME, code, bits)                   \
            }; break;                                                      \
        case kDLUInt:                                                      \
            switch(bits){                                                  \
                __AUGPY_DISPATCH_UINT_F(SCALAR_T_NAME, NAME, __VA_ARGS__); \
                __AUGPY_DISPATCH_UINT_D(SCALAR_T_NAME, NAME, __VA_ARGS__); \
                default: __AUGPY_THROW(NAME, code, bits)                   \
            }; break;                                                      \
        case kDLFloat:                                                     \
            switch(bits){                                                  \
                __AUGPY_DISPATCH_FLOAT(SCALAR_T_NAME, NAME, __VA_ARGS__);  \
                __AUGPY_DISPATCH_DOUBLE(SCALAR_T_NAME, NAME, __VA_ARGS__); \
                default: __AUGPY_THROW(NAME, code, bits)                   \
            }; break;                                                      \
        default:                                                           \
            __AUGPY_THROW(NAME, code, bits)                                \
    }                                                                      \
}


/**
Dispatch all dtypes.
Does not throw exception when dtype cannot be dispatched.
Declares tensor dtype under given name,
signed version of dtype as ``sscalar_t``,
and ``float`` (8, 16 bits) ``double`` (32, 64 bits) as ``temp_t``.

@param TYPE tensor dtype, see \link DLDataType \endlink
@param SCALAR_T_NAME name of the declared dtype, e.g., ``scalar_t``
@param NAME name of the dispatched function; used when exceptions are thrown
@param ... code to execute
*/
#define __AUGPY_DISPATCH_NOEXC(TYPE, SCALAR_T_NAME, NAME, ...){            \
    uint8_t code = TYPE.code;                                              \
    unsigned char bits = TYPE.bits ;                                       \
    switch(code){                                                          \
        case kDLInt:                                                       \
            switch(bits){                                                  \
                __AUGPY_DISPATCH_INT_F(SCALAR_T_NAME, NAME, __VA_ARGS__);  \
                __AUGPY_DISPATCH_INT_D(SCALAR_T_NAME, NAME, __VA_ARGS__);  \
            }; break;                                                      \
        case kDLUInt:                                                      \
            switch(bits){                                                  \
                __AUGPY_DISPATCH_UINT_F(SCALAR_T_NAME, NAME, __VA_ARGS__); \
                __AUGPY_DISPATCH_UINT_D(SCALAR_T_NAME, NAME, __VA_ARGS__); \
            }; break;                                                      \
        case kDLFloat:                                                     \
            switch(bits){                                                  \
                __AUGPY_DISPATCH_FLOAT(SCALAR_T_NAME, NAME, __VA_ARGS__);  \
                __AUGPY_DISPATCH_DOUBLE(SCALAR_T_NAME, NAME, __VA_ARGS__); \
            }; break;                                                      \
    }                                                                      \
}


/**
Dispatch all integer types.
Declares tensor dtype under given name,
signed version of dtype as ``sscalar_t``,
and ``float`` (8, 16 bits) ``double`` (32, 64 bits) as ``temp_t``.

@param TYPE tensor dtype, see \link DLDataType \endlink
@param SCALAR_T_NAME name of the declared dtype, e.g., ``scalar_t``
@param NAME name of the dispatched function; used when exceptions are thrown
@param ... code to execute
*/
#define __AUGPY_DISPATCH_I(TYPE, SCALAR_T_NAME, NAME, ...){                \
    uint8_t code = TYPE.code;                                              \
    unsigned char bits = TYPE.bits ;                                       \
    switch(code){                                                          \
        case kDLInt:                                                       \
            switch(bits){                                                  \
                __AUGPY_DISPATCH_INT_F(SCALAR_T_NAME, NAME, __VA_ARGS__);  \
                __AUGPY_DISPATCH_INT_D(SCALAR_T_NAME, NAME, __VA_ARGS__);  \
                default: __AUGPY_THROW(NAME, code, bits)                   \
            }; break;                                                      \
        case kDLUInt:                                                      \
            switch(bits){                                                  \
                __AUGPY_DISPATCH_UINT_F(SCALAR_T_NAME, NAME, __VA_ARGS__); \
                __AUGPY_DISPATCH_UINT_D(SCALAR_T_NAME, NAME, __VA_ARGS__); \
                default: __AUGPY_THROW(NAME, code, bits)                   \
            }; break;                                                      \
        default:                                                           \
            __AUGPY_THROW(NAME, code, bits)                                \
    }                                                                      \
}


/**
Dispatch all dtypes that can use ``float`` as temp_t.
Declares tensor dtype under given name,
signed version of dtype as ``sscalar_t``,
and ``float`` as ``temp_t``.

@param TYPE tensor dtype, see \link DLDataType \endlink
@param SCALAR_T_NAME name of the declared dtype, e.g., ``scalar_t``
@param NAME name of the dispatched function; used when exceptions are thrown
@param ... code to execute
*/
#define __AUGPY_DISPATCH_F(TYPE, SCALAR_T_NAME, NAME, ...){                \
    uint8_t code = TYPE.code;                                              \
    unsigned char bits = TYPE.bits ;                                       \
    switch(code){                                                          \
        case kDLInt:                                                       \
            switch(bits){                                                  \
                __AUGPY_DISPATCH_INT_F(SCALAR_T_NAME, NAME, __VA_ARGS__);  \
                default: __AUGPY_THROW(NAME, code, bits)                   \
            }; break;                                                      \
        case kDLUInt:                                                      \
            switch(bits){                                                  \
                __AUGPY_DISPATCH_UINT_F(SCALAR_T_NAME, NAME, __VA_ARGS__); \
                default: __AUGPY_THROW(NAME, code, bits)                   \
            }; break;                                                      \
        case kDLFloat:                                                     \
            switch(bits){                                                  \
                __AUGPY_DISPATCH_FLOAT(SCALAR_T_NAME, NAME, __VA_ARGS__);  \
                default: __AUGPY_THROW(NAME, code, bits)                   \
            }; break;                                                      \
        default: __AUGPY_THROW(NAME, code, bits)                           \
    }                                                                      \


/**
Dispatch all dtypes that can use ``float`` as temp_t.
Does not throw exception when dtype cannot be dispatched.
Declares tensor dtype under given name,
signed version of dtype as ``sscalar_t``,
and ``float`` as ``temp_t``.

@param TYPE tensor dtype, see \link DLDataType \endlink
@param SCALAR_T_NAME name of the declared dtype, e.g., ``scalar_t``
@param NAME name of the dispatched function; used when exceptions are thrown
@param ... code to execute
*/
#define __AUGPY_DISPATCH_F_NOEXC(TYPE, SCALAR_T_NAME, NAME, ...){          \
    uint8_t code = TYPE.code;                                              \
    unsigned char bits = TYPE.bits ;                                       \
    switch(code){                                                          \
        case kDLInt:                                                       \
            switch(bits){                                                  \
                __AUGPY_DISPATCH_INT_F(SCALAR_T_NAME, NAME, __VA_ARGS__);  \
            }; break;                                                      \
        case kDLUInt:                                                      \
            switch(bits){                                                  \
                __AUGPY_DISPATCH_UINT_F(SCALAR_T_NAME, NAME, __VA_ARGS__); \
            }; break;                                                      \
        case kDLFloat:                                                     \
            switch(bits){                                                  \
                __AUGPY_DISPATCH_FLOAT(SCALAR_T_NAME, NAME, __VA_ARGS__);  \
            }; break;                                                      \
    }                                                                      \
}


/**
Dispatch all dtypes that must use ``double`` as temp_t.
Declares tensor dtype under given name,
signed version of dtype as ``sscalar_t``,
and ``double`` as ``temp_t``.

@param TYPE tensor dtype, see \link DLDataType \endlink
@param SCALAR_T_NAME name of the declared dtype, e.g., ``scalar_t``
@param NAME name of the dispatched function; used when exceptions are thrown
@param ... code to execute
*/
#define __AUGPY_DISPATCH_D(TYPE, SCALAR_T_NAME, NAME, ...){                \
    uint8_t code = TYPE.code;                                              \
    unsigned char bits = TYPE.bits ;                                       \
    switch(code){                                                          \
        case kDLInt:                                                       \
            switch(bits){                                                  \
                __AUGPY_DISPATCH_INT_D(SCALAR_T_NAME, NAME, __VA_ARGS__);  \
                default: __AUGPY_THROW(NAME, code, bits)                   \
            }; break;                                                      \
        case kDLUInt:                                                      \
            switch(bits){                                                  \
                __AUGPY_DISPATCH_UINT_D(SCALAR_T_NAME, NAME, __VA_ARGS__); \
                default: __AUGPY_THROW(NAME, code, bits)                   \
            }; break;                                                      \
        case kDLFloat:                                                     \
            switch(bits){                                                  \
                __AUGPY_DISPATCH_DOUBLE(SCALAR_T_NAME, NAME, __VA_ARGS__); \
                default: __AUGPY_THROW(NAME, code, bits)                   \
            }; break;                                                      \
        default: __AUGPY_THROW(NAME, code, bits)                           \
    }                                                                      \
}


/**
Dispatch all dtypes that must use ``double`` as temp_t.
Does not throw exception when dtype cannot be dispatched.
Declares tensor dtype under given name,
signed version of dtype as ``sscalar_t``,
and ``double`` as ``temp_t``.

@param TYPE tensor dtype, see \link DLDataType \endlink
@param SCALAR_T_NAME name of the declared dtype, e.g., ``scalar_t``
@param NAME name of the dispatched function; used when exceptions are thrown
@param ... code to execute
*/
#define __AUGPY_DISPATCH_D_NOEXC(TYPE, SCALAR_T_NAME, NAME, ...){          \
    uint8_t code = TYPE.code;                                              \
    unsigned char bits = TYPE.bits ;                                       \
    switch(code){                                                          \
        case kDLInt:                                                       \
            switch(bits){                                                  \
                __AUGPY_DISPATCH_INT_D(SCALAR_T_NAME, NAME, __VA_ARGS__);  \
            }; break;                                                      \
        case kDLUInt:                                                      \
            switch(bits){                                                  \
                __AUGPY_DISPATCH_UINT_D(SCALAR_T_NAME, NAME, __VA_ARGS__); \
            }; break;                                                      \
        case kDLFloat:                                                     \
            switch(bits){                                                  \
                __AUGPY_DISPATCH_DOUBLE(SCALAR_T_NAME, NAME, __VA_ARGS__); \
            }; break;                                                      \
    }                                                                      \
}


/**
Dispatch all dtypes.
Declares tensor dtype as ``scalar_t``,
signed version of dtype as ``sscalar_t``,
and ``float`` (8, 16 bits) ``double`` (32, 64 bits) as ``temp_t``.

@param TYPE tensor dtype, see \link DLDataType \endlink
@param NAME name of the dispatched function; used when exceptions are thrown
@param ... code to execute
*/
#define AUGPY_DISPATCH(TYPE, NAME, ...) __AUGPY_DISPATCH(TYPE, scalar_t, NAME, __VA_ARGS__)


/**
Dispatch all dtypes.
Does not throw exception when dtype cannot be dispatched.
Declares tensor dtype as ``scalar_t``,
signed version of dtype as ``sscalar_t``,
and ``float`` (8, 16 bits) ``double`` (32, 64 bits) as ``temp_t``.

@param TYPE tensor dtype, see \link DLDataType \endlink
@param NAME name of the dispatched function; used when exceptions are thrown
@param ... code to execute
*/
#define AUGPY_DISPATCH_NOEXC(TYPE, NAME, ...) __AUGPY_DISPATCH_NOEXC(TYPE, scalar_t, NAME, __VA_ARGS__)


/**
Dispatch all integer types.
Declares tensor dtype as ``scalar_t``,
signed version of dtype as ``sscalar_t``,
and ``float`` (8, 16 bits) ``double`` (32, 64 bits) as ``temp_t``.

@param TYPE tensor dtype, see \link DLDataType \endlink
@param NAME name of the dispatched function; used when exceptions are thrown
@param ... code to execute
*/
#define AUGPY_DISPATCH_I(TYPE, NAME, ...) __AUGPY_DISPATCH_I(TYPE, scalar_t, NAME, __VA_ARGS__)


/**
Dispatch all dtypes that can use ``float`` as temp_t.
Declares tensor dtype as ``scalar_t``,
signed version of dtype as ``sscalar_t``,
and ``float`` as ``temp_t``.

@param TYPE tensor dtype, see \link DLDataType \endlink
@param NAME name of the dispatched function; used when exceptions are thrown
@param ... code to execute
*/
#define AUGPY_DISPATCH_F(TYPE, NAME, ...) __AUGPY_DISPATCH_F(TYPE, scalar_t, NAME, __VA_ARGS__)


/**
Dispatch all dtypes that can use ``float`` as temp_t.
Does not throw exception when dtype cannot be dispatched.
Declares tensor dtype as ``scalar_t``,
signed version of dtype as ``sscalar_t``,
and ``float`` as ``temp_t``.

@param TYPE tensor dtype, see \link DLDataType \endlink
@param NAME name of the dispatched function; used when exceptions are thrown
@param ... code to execute
*/
#define AUGPY_DISPATCH_F_NOEXC(TYPE, NAME, ...) __AUGPY_DISPATCH_F_NOEXC(TYPE, scalar_t, NAME, __VA_ARGS__)


/**
Dispatch all dtypes that must use ``double`` as temp_t.
Declares tensor dtype as ``scalar_t``,
signed version of dtype as ``sscalar_t``,
and ``double`` as ``temp_t``.

@param TYPE tensor dtype, see \link DLDataType \endlink
@param NAME name of the dispatched function; used when exceptions are thrown
@param ... code to execute
*/
#define AUGPY_DISPATCH_D(TYPE, NAME, ...) __AUGPY_DISPATCH_D(TYPE, scalar_t, NAME, __VA_ARGS__)


/**
Dispatch all dtypes that must use ``double`` as temp_t.
Does not throw exception when dtype cannot be dispatched.
Declares tensor dtype as ``scalar_t``,
signed version of dtype as ``sscalar_t``,
and ``double`` as ``temp_t``.

@param TYPE tensor dtype, see \link DLDataType \endlink
@param NAME name of the dispatched function; used when exceptions are thrown
@param ... code to execute
*/
#define AUGPY_DISPATCH_D_NOEXC(TYPE, NAME, ...) __AUGPY_DISPATCH_D_NOEXC(TYPE, scalar_t, NAME, __VA_ARGS__)


// namespace augpy
}


// dispatch.h
#endif
