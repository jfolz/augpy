#include <stdexcept>
#include <vector>
#include <mutex>
#include <algorithm>
#include <thread>
#include <chrono>
#include <sstream>
#include "core.h"
#include "exception.h"
#include "cnmem.h"


namespace augpy {


std::mutex device_mutex;
bool device_initialized[MAX_MANAGED_DEVICES];
cudaDevicePropEx device_properties[MAX_MANAGED_DEVICES];


void init_device(int device_id) {
    CUDA(cudaSetDevice(device_id));
    if (!device_initialized[device_id]) {
        cnmemStatus_t cnmem_e = CNMEM_STATUS_SUCCESS;
        cudaError_t cuda_e = cudaSuccess;
        device_mutex.lock();
        cuda_e = cudaSetDevice(device_id);
        // another thread may have initialized
        // between checking state and acquiring lock,
        // so check again if device is not yet initialized
        if (cuda_e == cudaSuccess && !device_initialized[device_id]) {
            cnmemDevice_t device{device_id, 1024*1024, 0, NULL, NULL};
            cnmemStatus_t cnmem_e = cnmemInit(1, &device, CNMEM_FLAGS_DEFAULT);
            device_initialized[device_id] = cnmem_e==CNMEM_STATUS_SUCCESS;
            cudaDevicePropEx* props = &device_properties[device_id];
            cuda_e = cudaGetDeviceProperties((cudaDeviceProp*)props, device_id);
            // if device supports stream priorities, get the range of valid priorities
            if (cuda_e == cudaSuccess) {
                cuda_e = cudaDeviceGetStreamPriorityRange(
                    &props->leastStreamPriority,
                    &props->greatestStreamPriority
                );
            }
            props->coresPerSM = cores_per_sm(props->major, props->minor);
            props->numCudaCores = props->coresPerSM * props->multiProcessorCount;
        }
        device_mutex.unlock();
        if (cnmem_e != CNMEM_STATUS_SUCCESS){
            throw cnmem_error(cnmem_e);
        }
        if (cuda_e != cudaSuccess) {
            throw cuda_error(cuda_e);
        }
    }
}


cudaDevicePropEx get_device_properties(int device_id) {
    init_device(device_id);
    return device_properties[device_id];
}


int cores_per_sm(int major, int minor) {
    switch((major << 4) + minor) {
        case 0x30: return 192;
        case 0x32: return 192;
        case 0x35: return 192;
        case 0x37: return 192;
        case 0x50: return 128;
        case 0x52: return 128;
        case 0x53: return 128;
        case 0x60: return  64;
        case 0x61: return 128;
        case 0x62: return 128;
        case 0x70: return  64;
        case 0x72: return  64;
        case 0x75: return  64;
        case 0x80: return  64;
        case 0x86: return  128;
    }
    char msg[128];
    sprintf(msg, "unknown compute capability %d.%d", major, minor);
    throw std::invalid_argument(msg);
}


int cores_per_sm(int device_id) {
    auto props = get_device_properties(device_id);
    return props.coresPerSM;
}


int get_num_cuda_cores(int device_id) {
    auto props = get_device_properties(device_id);
    return props.numCudaCores;
}


void release() {
    device_mutex.lock();
    cnmemStatus_t e = cnmemFinalize();
    device_mutex.unlock();
    if(e!=CNMEM_STATUS_SUCCESS){
        throw cnmem_error(e);
    }
    for(int device_id=0; device_id<MAX_MANAGED_DEVICES; device_id++) {
        device_initialized[device_id] = 0;
    }
}


managed_allocation::managed_allocation(int device_id, size_t size):
        device_id(device_id), size(size) {
    ptr = NULL;
}


void managed_allocation::record() {
    CUDA(cudaEventRecord(event, current_stream));
}


// declare managed_eventalloc_nolock
cudaError_t managed_eventalloc_nolock(cudaEvent_t* event);


// declare managed_eventfree_nolock
void managed_eventfree_nolock(cudaEvent_t event);


bool check_and_remove_orphaned(managed_allocation mem) {
    cudaError_t e = cudaEventQuery(mem.event);
    if (e == cudaSuccess) {
        managed_cudafree(mem.ptr);
        managed_eventfree_nolock(mem.event);
        return true;
    }
    return false;
}


std::mutex manager_mutex;
std::vector<cudaEvent_t> event_pool[MAX_MANAGED_DEVICES];
std::vector<managed_allocation> orphaned_memory_list[MAX_MANAGED_DEVICES];


void mark_orphaned(managed_allocation* alloc) {
    manager_mutex.lock();
    orphaned_memory_list[current_device].push_back(*alloc);
    manager_mutex.unlock();
}


std::shared_ptr<managed_allocation> managed_cudamalloc(size_t size, int device_id) {
    init_device(device_id);
    managed_allocation* alloc = new managed_allocation(device_id, size);

    // 1. traverse the list of orphaned memory to
    //    free allocations that are no longer in use
    // 2. retrieve a Cuda event for the new allocation
    manager_mutex.lock();
    std::vector<managed_allocation> &orphans = orphaned_memory_list[current_device];
    orphans.erase(std::remove_if(
        orphans.begin(),
        orphans.end(),
        check_and_remove_orphaned
    ), orphans.end());
    cudaError_t e = managed_eventalloc_nolock(&alloc->event);
    manager_mutex.unlock();

    // check whether event allocation was successful
    CUDA(e);

    // allocate memory
    CNMEM(cnmemMalloc(&alloc->ptr, size, current_stream));

    // create and return new shared_ptr with mark_orphaned as deleter
    return std::shared_ptr<managed_allocation>(alloc, [](managed_allocation* alloc) {
        mark_orphaned(alloc);
    });
}


void managed_cudafree(void* ptr) {
    CNMEM(cnmemFree(ptr, current_stream));
}


cudaError_t managed_eventalloc_nolock(cudaEvent_t* event) {
    cudaError_t e = cudaSuccess;
    if (event_pool[current_device].size()) {
        *event = event_pool[current_device].back();
        event_pool[current_device].pop_back();
    }
    else {
        e = cudaEventCreateWithFlags(event, cudaEventBlockingSync | cudaEventDisableTiming);
    }
    return e;
}


void managed_eventalloc(cudaEvent_t* event) {
    cudaError_t e = cudaSuccess;
    manager_mutex.lock();
    e = managed_eventalloc_nolock(event);
    manager_mutex.unlock();
    CUDA(e);
}


void managed_eventfree_nolock(cudaEvent_t event) {
    event_pool[current_device].push_back(event);
}


void managed_eventfree(cudaEvent_t event) {
    manager_mutex.lock();
    managed_eventfree_nolock(event);
    manager_mutex.unlock();
}


CudaDevice::CudaDevice(int device_id):
    device_id(device_id), previous_device(-1) {}


int CudaDevice::get_device() {
    return device_id;
}


void CudaDevice::activate() {
    if (previous_device >= 0) {
        throw std::invalid_argument("device already active");
    }
    CUDA(cudaSetDevice(device_id));
    previous_device = current_device;
    current_device = device_id;
}


void CudaDevice::deactivate() {
    if (previous_device < 0) {
        throw std::invalid_argument("device is not active");
    }
    CUDA(cudaSetDevice(previous_device));
    current_device = previous_device;
    previous_device = -1;
}


cudaDevicePropEx CudaDevice::get_properties() {
    return get_device_properties(device_id);
}


std::string CudaDevice::repr() {
    std::stringstream s;
    s << "CudaDevice(" << device_id << ")";
    return s.str();
}


void CudaDevice::synchronize() {
    CUDA(cudaSetDevice(device_id));
    CUDA(cudaDeviceSynchronize());
}


CudaEvent::CudaEvent() {
    managed_eventalloc(&event);
}


CudaEvent::~CudaEvent() noexcept(false) {
    managed_eventfree(event);
}


cudaEvent_t CudaEvent::get_event() {
    return event;
}


void CudaEvent::record() {
    CUDA(cudaEventRecord(event, current_stream));
}


bool CudaEvent::query() {
    cudaError_t e = cudaEventQuery(event);
    switch(e) {
    case cudaSuccess: return true;
    case cudaErrorNotReady: return false;
    default: throw cudaError(e);
    }
}


void CudaEvent::synchronize(int microseconds) {
    if (microseconds > 0) {
        std::chrono::microseconds delay(microseconds);
        cudaError_t e = cudaErrorNotReady;
        while (true) {
            e = cudaEventQuery(event);
            if (e != cudaErrorNotReady) {
                break;
            }
            std::this_thread::sleep_for(delay);
        }
        if (e == cudaSuccess) {
            return;
        }
        throw cudaError(e);
    }
    else {
        CUDA(cudaEventSynchronize(event));
    }
}


CudaStream::CudaStream(int device_id, int priority):
        device_id(device_id), priority(priority), stream(0), previous_stream(0) {
    // create proxy for default stream
    if (device_id == -1 && priority == -1) {
        return;
    }
    init_device(device_id);
    CUDA(cudaStreamCreateWithPriority(&stream, cudaStreamNonBlocking, priority));
    CNMEM(cnmemRegisterStream(stream));
}


CudaStream::CudaStream(cudaStream_t stream):
        device_id(-1), priority(-1), stream(stream), previous_stream((cudaStream_t)0) {}


CudaStream::~CudaStream() noexcept(false) {
    if (device_id != -1 && priority != -1) {
        CUDA(cudaStreamDestroy(stream));
    }
}


cudaStream_t& CudaStream::get_stream() {
    return stream;
}


void CudaStream::activate() {
    if (previous_stream) {
        throw std::invalid_argument("stream already active");
    }
    previous_stream = current_stream;
    current_stream = stream;
}


void CudaStream::deactivate() {
    if (!previous_stream) {
        throw std::invalid_argument("stream is not active");
    }
    current_stream = previous_stream;
    previous_stream = 0;
}


std::string CudaStream::repr() {
    std::stringstream s;
    s << "CudaStream(device_id=" << device_id << ", priority=" << priority << ")";
    return s.str();
}


void CudaStream::synchronize(int microseconds) {
    if (microseconds > 0) {
        std::chrono::microseconds delay(microseconds);
        cudaError_t e = cudaErrorNotReady;
        while (true) {
            e = cudaStreamQuery(stream);
            if (e != cudaErrorNotReady) {
                break;
            }
            std::this_thread::sleep_for(delay);
        }
        if (e == cudaSuccess) {
            return;
        }
        throw cudaError(e);
    }
    else {
        CUDA(cudaStreamSynchronize(stream));
    }
}


thread_local cudaStream_t current_stream(0);
const CudaStream default_stream = CudaStream(-1, -1);
thread_local int current_device(0);


// namespace augpy
}
