#ifndef AUGPY_ITERATOR_CUH
#define AUGPY_ITERATOR_CUH


#include "tensor.h"


namespace augpy {


template<typename scalar_t>
class Iterator {
public:

    Iterator(
        unsigned char *const ptr,
        const int ndim,
        ndim_array *const strides,
        ndim_array *const contiguous_strides
    ) : ptr(ptr),
        ndim(ndim),
        strides(strides),
        contiguous_strides(contiguous_strides)
    {}

    __device__ __forceinline__ scalar_t& operator[](size_t index) const {
        size_t strided_index = 0;
        // ensure ndim_arrays are put into registers instead of local memory:
        //  - loop must be fixed length so it can be unrolled,
        //    thus indexing arrays with a constant value
        //  - exit the loop manually by testing against ndim
        //
        // See this section of the CUDA programming guide for details:
        // https://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html#device-memory-accesses
        #pragma unroll
        for(int dim=1; dim<DLTENSOR_MAX_NDIM; ++dim) {
            if (dim >= ndim) break;
            size_t p = index / contiguous_strides[dim];
            strided_index += p * strides[dim];
            index -= p * contiguous_strides[dim];
        }
        return *reinterpret_cast<scalar_t*>(ptr + strided_index);
    }

private:
    unsigned char *const ptr;
    const int ndim;
    const ndim_array *const strides;
    const ndim_array *const contiguous_strides;
};


// namespace augpy
}


// AUGPY_ITERATOR_CUH
#endif
