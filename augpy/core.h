/**
@file core.h
Provides core functionality for
memory, device, stream, and event management.
*/


#ifndef AUGPY_CORE_H
#define AUGPY_CORE_H

#include <memory>
#include <cuda.h>
#include <cuda_runtime.h>


namespace augpy {


/**
Maximum number of GPU devices this library can manage.
Currently 2048.
*/
#ifndef MAX_MANAGED_DEVICES
#define MAX_MANAGED_DEVICES 2048
#endif


/**
Default value for blocks to generate per SM.
Used to calculate kernel launch config.
Currently 8.
*/
#ifndef BLOCKS_PER_SM
#define BLOCKS_PER_SM 8
#endif


/**
Calculates \f$\left\lceil{a/b}\right\rceil\f$.
*/
#define ceil_div(a, b) ((a) + (b) - 1) / (b)


/**
Throw std::invalid_argument with given ``message`` if ``expr`` is not true.
*/
#define ASSERT_TRUE(expr, message) if (!(expr)) { throw std::invalid_argument(message); }


/**
A simple fixed-size array.
Importantly `sizeof(array)` gives the actual size of the
array in bytes, so it can be used as an argument in
function calls etc.

@tparam T element type
@tparam N length of array
*/
template<typename T, int N>
class array {
public:
    /**
    Array that holds the data.
    */
    T x[N];

    /**
    Create a new array, setting all bytes in `x` to zero.
    */
    array() {
        memset((T*) &x, 0, sizeof(T)*N);
    }

    /**
    Create a new array, copying `n` values from the given pointer.
    All remaining bytes in `x` are set to zero.
    */
    array(T* values, int n) {
        memcpy((T*) &x, values, sizeof(T)*n);
        if (n < N) {
            memset((T*) &x[n], 0, sizeof(T)*(N-n));
        }
    }

    /**
    Return a reference to the element at index idx.
    */
    __device__ __host__ __forceinline__
    T& operator[](size_t idx) {
        return x[idx];
    }

    /**
    Return a const reference to the element at index idx.
    */
    __device__ __host__ __forceinline__
    const T& operator[](size_t idx) const {
        return x[idx];
    }

    /**
    Return a pointer to the internal array storage `x`.
    */
    __device__ __host__ __forceinline__
    T* ptr() {
        return (T*) &x;
    }
};


/**
Similar to <a href=
"https://en.cppreference.com/w/cpp/utility/tuple/make_tuple">std::make_tuple</a>,
but makes a fixed-length \link array \endlink instead.

Example: `auto tensors = make_array(tensor1, tensor2, tensor3);`
*/
template<class T, class... Tail>
array<T, 1+sizeof...(Tail)> make_array(T head, Tail... tail) {
    constexpr int N = 1+sizeof...(Tail);
    T a[]{head, tail...};
    return array<T, N>(&a[0], N);
}


/**
Initialize the GPU with the given ``device_id``.
You can, but do not need to this manually.
It is done for you whenever you request memory or
device properties for the first.
*/
void init_device(int device_id);


/**
The
<a href="https://docs.nvidia.com/cuda/cuda-runtime-api/structcudaDeviceProp.html">cudaDeviceProp</a>
struct extended with stream priority fields.
*/
struct cudaDevicePropEx : cudaDeviceProp {
    /// Lowest priority a Cuda stream on this device can have.
    int leastStreamPriority = 0;
    /// Highest priority a Cuda stream on this device can have.
    int greatestStreamPriority = 0;
    /// Number of Cuda cores per SM.
    int coresPerSM = 0;
    /// Total number of Cuda coes.
    int numCudaCores = 0;
};


/**
Returns the device properties of the given GPU device.
*/
cudaDevicePropEx get_device_properties(int device_id);


/**
Returns the number of Cuda cores of the given GPU device.
*/
int get_num_cuda_cores(int device_id);


/**
Given the major and minor Cuda capability (e.g., 7 and 5),
returns the number of Cuda cores per SM.
*/
int cores_per_sm(int major, int minor);


/**
Given a GPU device id, returns the number of Cuda cores per SM.
*/
int cores_per_sm(int device_id);


/**
Release all allocated memory on all GPUs.
All \link augpy::CudaTensor CudaTensors \endlink become invalid immediately.
Do I have to tell you this is dangerous?
*/
void release();


/**
A chunk of managed GPU memory.
*/
struct managed_allocation {
    /**
    GPU device id
    */
    int device_id;

    /**
    Number of bytes in allocation
    */
    size_t size;

    /**
    Pointer to allocated memory
    */
    void* ptr;

    /**
    Cuda event used to track whether memory is currently in use
    */
    cudaEvent_t event;

    /**
    Make new struct filled with ``device_id`` and ``size``.
    Does not allocate memory.
    Use \link managed_cudamalloc \endlink instead.
    */
    managed_allocation(int device_id, size_t size);

    void record();
};


/**
Malloc ``size`` bytes ond GPU with given ``device id``.
Returns \link managed_allocation \endlink as <a href=
"https://en.cppreference.com/w/cpp/memory/shared_ptr/shared_ptr"
>std::shared_ptr</a>.
Throws \link cuda_error \endlink.

One all instances of shared_ptr are deleted,
the allocated memory will be marked for deletion/reuse.
*/
std::shared_ptr<managed_allocation> managed_cudamalloc(size_t size, int device_id);


/**
Frees device memory allocated by
\link managed_cudamalloc \endlink
at the given location.
Throws \link cuda_error \endlink.
*/
void managed_cudafree(void* ptr);


/**
Return a Cuda event from the event pool of the \link current_device \endlink.
Flags ``cudaEventBlockingSync`` and ``cudaEventDisableTiming`` are set.
*/
void managed_eventalloc(cudaEvent_t* event);


/**
Mark the given Cuda event as reusable.
*/
void managed_eventfree(cudaEvent_t event);


/**
Control which device is used for computation.
*/
class CudaDevice {
public:
    /**
    Create a new CudaDevice with the given Cuda device ID.
    0 is the default and typically fastest device in the system.
    */
    CudaDevice(int device_id);

    /**
    Return the device ID.
    */
    int get_device();

    /**
    Make this the \link current_device \endlink
    and remember the previous stream.
    */
    void activate();

    /**
    Make the previous device the \link current_device \endlink.
    */
    void deactivate();

    /**
    Block until all work on this device has finished.
    Cuda uses busy waiting to achieve this.
    See synchronization method of
    \link CudaStream \endlink or \link CudaEvent \endlink
    to avoid the CPU load this incurs.
    */
    void synchronize();

    /**
    Return the device properties,
    see \link get_device_properties \endlink for more detials.
    */
    cudaDevicePropEx get_properties();

    /**
    Returns a concise string representation of this device.
    */
    std::string repr();

private:
    int device_id;
    int previous_device;
};


/**
Convenience wrapper for the
<a href="https://docs.nvidia.com/cuda/cuda-runtime-api/group__CUDART__EVENT.html"> cudaEvent_t</a>
type.
*/
class CudaEvent {
public:
    /**
    Get a Cuda event from the event pool of the
    \link current_device \endlink.
    */
    CudaEvent();

    ~CudaEvent() noexcept(false);
    /**
    Return the wrapped Cuda event.
    */
    cudaEvent_t get_event();
    /**
    Record wrapped event on \link current_stream \endlink.
    */
    void record();
    /**
    Returns ``true`` if event has occurred.
    */
    bool query();
    /**
    Block until event has occurred.
    Checks in ``microseconds`` interval.
    Faster intervals make this more accurate, but increase CPU load.
    Uses standard Cuda busy-waiting method if ``microseconds <= 0``.
    */
    void synchronize(int microseconds=100);
private:
    cudaEvent_t event;
};


/**
Convenience wrapper for the
<a href="https://docs.nvidia.com/cuda/cuda-runtime-api/group__CUDART__STREAM.html">cudaStream_t</a>
type.
*/
class CudaStream {
public:
    /**
    Create a new Cuda stream on the given device.
    Lower numbers mean higher priority,
    and values are clipped to the valid range.
    Use \link get_device_properties \endlink
    to get the range of possible values for a device.
    See <a
    href="https://docs.nvidia.com/cuda/cuda-runtime-api/group__CUDART__STREAM.html#group__CUDART__STREAM_1ge2be9e9858849bf62ba4a8b66d1c3540"
    >cudaStreamCreateWithPriority</a>
    for more details.

    Use ``device_id=-1`` and ``priority=-1`` to get the
    \link default_stream \endlink.
    */
    CudaStream(int device_id=-1, int priority=-1);

    /**
    Wrap the given cudaStream_t in a CudaStream.
    */
    CudaStream(cudaStream_t stream);

    ~CudaStream() noexcept(false);

    /**
    Return the wrapped Cuda stream.
    */
    cudaStream_t& get_stream();

    /**
    Make this the \link current_stream \endlink
    and remember the previous stream.
    */
    void activate();

    /**
    Make the previous stream the \link current_stream \endlink.
    */
    void deactivate();

    /**
    Block until all work on this stream has finished.
    Checks in ``microseconds`` interval.
    Faster intervals make this more accurate, but increase CPU load.
    Uses standard Cuda busy-waiting method if ``microseconds <= 0``.
    */
    void synchronize(int microseconds=100);

    /**
    Returns a concise string representation of this stream.
    */
    std::string repr();

private:
    int device_id;
    int priority;
    cudaStream_t stream;
    cudaStream_t previous_stream;
};


/**
Controls which GPU device is used by each thread.
*/
extern thread_local int current_device;


/**
Controls which Cuda stream is used by each thread.
*/
extern thread_local cudaStream_t current_stream;


/**
The default Cuda stream as a wrapped \ref CudaStream.
*/
extern const CudaStream default_stream;


// namespace augpy
}


// AUGPY_CORE_H
#endif
