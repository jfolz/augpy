#include <cuda.h>
#include <cuda_runtime.h>
#include "tensor.h"
#include "dispatch.h"
#include "elementwise.cuh"
#include "saturate_cast.cuh"


namespace augpy {


template<typename scalar_t>
__device__ __forceinline__ void copy_function(
        const array<tensor_param, 2> &tensors,
        const unsigned char &nothing
){
    tensors[0].store<scalar_t>(tensors[1].load<scalar_t>());
}


CudaTensor* copy(
        CudaTensor* src,
        CudaTensor* dst,
        unsigned int blocks_per_sm,
        unsigned int num_threads
){
    auto tensors = make_array(dst, src);
    AUGPY_DISPATCH(src->dl_tensor.dtype, "copy", ([&] {
        dst = elementwise_function<2, unsigned char, copy_function<scalar_t>>(
            tensors, 0, blocks_per_sm, num_threads
        );
    }));
    return dst;
}


template<typename scalar_t>
__device__ __forceinline__ void fill_function(
        const array<tensor_param, 1> &tensors,
        const scalar_t &fill_value
){
    tensors[0].store<scalar_t>(fill_value);
}


CudaTensor* fill(
        double scalar,
        CudaTensor* dst,
        unsigned int blocks_per_sm,
        unsigned int num_threads
){
    auto tensors = make_array(dst);
    AUGPY_DISPATCH(dst->dl_tensor.dtype, "fill", ([&] {
        scalar_t casted_scalar = saturate_cast<double, scalar_t>(scalar);
        elementwise_function<1, scalar_t, fill_function<scalar_t>>(
            tensors, casted_scalar, blocks_per_sm, num_threads
        );
    }));
    return dst;
}


// namespace augpy
}
