#include <sstream>
#include <string.h>
#include <cutlass/gemm/device/gemm.h>
#include "tensor.h"
#include "saturate_cast.cuh"
#include "exception.h"


using namespace std;


namespace augpy {


//computes C = alpha*A*B + beta*C
CudaTensor* gemm(
        CudaTensor* A,
        CudaTensor* B,
        CudaTensor* C,
        double alpha,
        double beta
){
    assert_contiguous(A);
    assert_contiguous(B);
    DLTensor &ta = A->dl_tensor;
	DLTensor &tb = B->dl_tensor;

    int n = ta.shape[0];
    int m = tb.shape[1];
    int k = tb.shape[0];

    if (!C) {
        C = new CudaTensor({n, m}, ta.dtype, ta.ctx.device_id);
    }

    assert_contiguous(C);
	DLTensor &tc = C->dl_tensor;

    if(ta.ndim!=2 || tb.ndim != 2 || tc.ndim != 2){
        throw std::invalid_argument("tensors needs to have 2 dimensions");
    }
    if(ta.shape[1] != k) {
        throw std::invalid_argument("A.shape[1] must match B.shape[0]");
    }
    if(n != tc.shape[0] || m != tc.shape[1]) {
        throw std::invalid_argument("shape of C must match A*C");
    }

    using ColumnMajor = cutlass::layout::ColumnMajor;
    using CutlassSGemm = cutlass::gemm::device::Gemm<
        float,        // Data-type of A matrix
        ColumnMajor,  // Layout of A matrix
        float,        // Data-type of B matrix
        ColumnMajor,  // Layout of B matrix
        float,        // Data-type of C matrix
        ColumnMajor,  // Layout of C matrix
        float,
        cutlass::arch::OpClassSimt,
        cutlass::arch::Sm60
    >;
    using CutlassDGemm = cutlass::gemm::device::Gemm<
        double,        // Data-type of A matrix
        ColumnMajor,  // Layout of A matrix
        double,        // Data-type of B matrix
        ColumnMajor,  // Layout of B matrix
        double,        // Data-type of C matrix
        ColumnMajor,  // Layout of C matrix
        double,
        cutlass::arch::OpClassSimt,
        cutlass::arch::Sm60
    >;

    if (dldatatype_equals(tc.dtype, dldtype_float32)) {
        CutlassSGemm gemm_operator;
        CutlassSGemm::Arguments args(
            {m, n, k},
            {(float*) B->ptr(), m},
            {(float*) A->ptr(), k},
            {(float*) C->ptr(), m},
            {(float*) C->ptr(), m},
            {saturate_cast<double, float>(alpha), saturate_cast<double, float>(beta)}
        );
        CUTLASS(gemm_operator(args));
    }
    else if (dldatatype_equals(tc.dtype, dldtype_float64)) {
        CutlassDGemm gemm_operator;
        CutlassDGemm::Arguments args(
            {m, n, k},
            {(double*) B->ptr(), m},
            {(double*) A->ptr(), k},
            {(double*) C->ptr(), m},
            {(double*) C->ptr(), m},
            {alpha, beta}
        );
        CUTLASS(gemm_operator(args));
    }
    else {
        CUTLASS(cutlass::Status::kErrorInvalidDataType);
    }

    A->record();
    B->record();
    C->record();

    return C;
}


// namespace augpy
}
