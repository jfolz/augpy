#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include "dlpack.h"
#include "core.h"
#include "tensor.h"
#include "function.h"
#include "exception.h"
#include "random.h"
#include "nvjpegdecoder.h"
#include "warp_affine.h"
#include "lighting.h"
#include "blur.h"
#include <tuple>


namespace py = pybind11;
using namespace augpy;


PYBIND11_MODULE(_augpy, m) {
    m.doc() = "Python bindings for image processing CUDA functions";

    py::class_<cudaDevicePropEx> props(m, "CudaDeviceProp",
        R"docdelimiter(
        The `cudaDeviceProp <https://docs.nvidia.com/cuda/cuda-runtime-api/structcudaDeviceProp.html>`_
        struct extended with stream priority fields
        :py:attr:`leastStreamPriority` and :py:attr:`greatestStreamPriority`,
        :py:attr:`coresPerMultiprocessor`, and :py:attr:`maxGridSize`.
        )docdelimiter",
        py::module_local()
    );
    props
        .def_readonly("name", &cudaDevicePropEx::name,
            R"docdelimiter(
            ASCII string identifying device
            )docdelimiter"
        )
        .def_readonly("major", &cudaDevicePropEx::major,
            R"docdelimiter(
            Major compute capability
            )docdelimiter"
        )
        .def_readonly("minor", &cudaDevicePropEx::minor,
            R"docdelimiter(
            Minor compute capability
            )docdelimiter"
        )
        .def_readonly("multiProcessorCount", &cudaDevicePropEx::multiProcessorCount,
            R"docdelimiter(
            Number of multiprocessors on device
            )docdelimiter"
        )
        .def_property_readonly("coresPerMultiprocessor", [](cudaDevicePropEx props) {
                return cores_per_sm(props.major, props.minor);
            },
            R"docdelimiter(
            Number of Cuda cores per multiprocessor
            )docdelimiter"
        )
        .def_readonly("l2CacheSize", &cudaDevicePropEx::l2CacheSize,
            R"docdelimiter(
            Size of L2 cache in bytes
            )docdelimiter"
        )
        .def_property_readonly("maxGridSize", [](cudaDevicePropEx props) {
                py::tuple size = py::tuple(3);
                size[0] = props.maxGridSize[0];
                size[1] = props.maxGridSize[1];
                size[2] = props.maxGridSize[2];
                return size;
            },
            R"docdelimiter(
            Max number of blocks in each grid dimension
            )docdelimiter"
        )
        .def_readonly("maxThreadsDim", &cudaDevicePropEx::maxThreadsDim,
            R"docdelimiter(
            Maximum size of each dimension of a block
            )docdelimiter"
        )
        .def_readonly("maxThreadsPerBlock", &cudaDevicePropEx::maxThreadsPerBlock,
            R"docdelimiter(
            Maximum number of threads per block
            )docdelimiter"
        )
        .def_readonly("maxThreadsPerMultiProcessor", &cudaDevicePropEx::maxThreadsPerMultiProcessor,
            R"docdelimiter(
            Maximum resident threads per multiprocessor
            )docdelimiter"
        )
        .def_readonly("regsPerBlock", &cudaDevicePropEx::regsPerBlock,
            R"docdelimiter(
            32-bit registers available per block
            )docdelimiter"
        )
        .def_readonly("regsPerMultiprocessor", &cudaDevicePropEx::regsPerMultiprocessor,
            R"docdelimiter(
            32-bit registers available per multiprocessor
            )docdelimiter"
        )
        .def_readonly("sharedMemPerBlock", &cudaDevicePropEx::sharedMemPerBlock,
            R"docdelimiter(
            Shared memory available per block in bytes
            )docdelimiter"
        )
        .def_readonly("sharedMemPerMultiprocessor", &cudaDevicePropEx::sharedMemPerMultiprocessor,
            R"docdelimiter(
            Shared memory available per multiprocessor in bytes
            )docdelimiter"
        )
        .def_readonly("totalConstMem", &cudaDevicePropEx::totalConstMem,
            R"docdelimiter(
            Constant memory available on device in bytes
            )docdelimiter"
        )
        .def_readonly("totalGlobalMem", &cudaDevicePropEx::totalGlobalMem,
            R"docdelimiter(
            Global memory available on device in bytes
            )docdelimiter"
        )
        .def_readonly("warpSize", &cudaDevicePropEx::warpSize,
            R"docdelimiter(
            Warp size in threads
            )docdelimiter"
        )
        .def_readonly("streamPrioritiesSupported", &cudaDevicePropEx::streamPrioritiesSupported,
            R"docdelimiter(
            Device supports stream priorities
            )docdelimiter"
        )
        .def_readonly("leastStreamPriority", &cudaDevicePropEx::leastStreamPriority,
            R"docdelimiter(
            Lowest priority a Cuda stream on this device can have.
            )docdelimiter"
        )
        .def_readonly("greatestStreamPriority", &cudaDevicePropEx::greatestStreamPriority,
            R"docdelimiter(
            Highest priority a Cuda stream on this device can have.
            )docdelimiter"
        )
        .def_readonly("coresPerSM", &cudaDevicePropEx::coresPerSM,
            R"docdelimiter(
            Number of Cuda cores per SM.
            )docdelimiter"
        )
        .def_readonly("numCudaCores", &cudaDevicePropEx::numCudaCores,
            R"docdelimiter(
            Total number of Cuda coes.
            )docdelimiter"
        );

    py::class_<CudaDevice> cudadevice(m, "CudaDevice",
        R"docdelimiter(
        Create a new CudaDevice with the given Cuda device ID.
        0 is the default and typically fastest device in the system.

        Parameters:
            device_id: GPU device ID
        )docdelimiter"
    );
    cudadevice
        .def(py::init<int>(),
            py::return_value_policy::take_ownership,
            py::arg("device_id")
        )
        .def("activate", &CudaDevice::activate,
            R"docdelimiter(
            Make this the :ref:`py/core:current_stream`
            and remember the previous stream.
            )docdelimiter"
        )
        .def("deactivate", &CudaDevice::deactivate,
            R"docdelimiter(
            Make the previous stream the :ref:`py/core:current_stream`.
            )docdelimiter"
        )
        .def("synchronize", &CudaDevice::synchronize,
            R"docdelimiter(
            Block until all work on this device has finished.
            Cuda uses busy waiting to achieve this.
            See synchronization method of
            :ref:`py/core:CudaStream` or :ref:`py/core:CudaEvent`
            to avoid the CPU load this incurs.
            )docdelimiter",
            py::call_guard<py::gil_scoped_release>()
        )
        .def("get_device", &CudaDevice::get_device,
            R"docdelimiter(
            Return the device ID.
            )docdelimiter"
        )
        .def("get_properties", &CudaDevice::get_properties,
            R"docdelimiter(
            Return the device properties,
            see  :ref:`py/core:get_device_properties` for more detials.
            )docdelimiter"
        )
        .def("__repr__", &CudaDevice::repr,
            R"docdelimiter(
            String representation of this device.
            )docdelimiter"
        );

    py::class_<CudaEvent> cudaevent(m, "CudaEvent",
        R"docdelimiter(
        Convenience wrapper for the
        `cudaEvent_t <https://docs.nvidia.com/cuda/cuda-runtime-api/group__CUDART__EVENT.html>`_.

        Creating a new CudaEvent retrieves an event from the event pool of the
        :ref:`py/core:current_device`.
        )docdelimiter"
    );
    cudaevent
        .def(py::init(),
            py::return_value_policy::take_ownership
        )
        .def("record", &CudaEvent::record,
            R"docdelimiter(
            Record wrapped event on :ref:`py/core:current_stream`.
            )docdelimiter"
        )
        .def("query", &CudaEvent::query,
            R"docdelimiter(
            Returns ``True`` if event has occurred.
            )docdelimiter"
        )
        .def("synchronize", &CudaEvent::synchronize,
            R"docdelimiter(
            Block until event has occurred.
            Checks in ``microseconds`` interval.
            Faster intervals make this more accurate, but increase CPU load.
            Uses standard Cuda busy-waiting method if ``microseconds <= 0``.

            Parameters:
                microseconds: check interval
            )docdelimiter",
            py::call_guard<py::gil_scoped_release>(),
            py::arg("microseconds")=100
        );

    py::class_<CudaStream> cudastream(m, "CudaStream",
        R"docdelimiter(
        Convenience wrapper for the
        `cudaStream_t <https://docs.nvidia.com/cuda/cuda-runtime-api/group__CUDART__STREAM.html>`_
        type.

        Creates a new Cuda stream on the given device.
        Lower numbers mean higher priority,
        and values are clipped to the valid range.
        Use :py:func:`get_device_properties`
        to get the range of possible values for a device.

        See:
            `cudaStreamCreateWithPriority <https://docs.nvidia.com/cuda/cuda-runtime-api/group__CUDART__STREAM.html#group__CUDART__STREAM_1ge2be9e9858849bf62ba4a8b66d1c3540>`_

        Use ``device_id=-1`` and ``priority=-1`` to get the
        :py:attr:`default_stream`.

        Parameters:
            device_id: GPU device ID
            priority: stream priority
        )docdelimiter"
    );
    cudastream
        .def(py::init<int, int>(),
            py::return_value_policy::take_ownership,
            py::arg("device_id")=0,
            py::arg("priority")=0
        )
        .def("activate", &CudaStream::activate,
            R"docdelimiter(
            Make this the :ref:`py/core:current_stream`
            and remember the previous stream.
            )docdelimiter"
        )
        .def("deactivate", &CudaStream::deactivate,
            R"docdelimiter(
            Make the previous stream the :ref:`py/core:current_stream`.
            )docdelimiter"
        )
        .def("synchronize", &CudaStream::synchronize,
            R"docdelimiter(
            Block until all work on this stream has finished.
            Checks in ``microseconds`` interval.
            Faster intervals make this more accurate, but increase CPU load.
            Uses standard Cuda busy-waiting method if ``microseconds <= 0``.
            )docdelimiter",
            py::call_guard<py::gil_scoped_release>(),
            py::arg("microseconds")=100
        )
        .def("__repr__", &CudaStream::repr,
            R"docdelimiter(
            String representation of this stream.
            )docdelimiter"
        );

    m.def("get_current_device", [](){ return current_device; },
        R"docdelimiter(
        Returns the active device ID.

        See:
            :ref:`py/core:current_device`.
        )docdelimiter"
    );

    m.def("get_current_stream", [](){ return CudaStream(current_stream); },
        R"docdelimiter(
        Returns the active :py:class:`CudaStream`.

        See:
            :ref:`py/core:current_stream`
        )docdelimiter"
    );

    m.attr("default_stream") = default_stream;

    m.def("release", &release,
        R"docdelimiter(
        Release all allocated memory on all GPUs.
        All :py:class:`CudaTensors <CudaTensor>` become invalid immediately.
        Do I have to tell you this is dangerous?
        )docdelimiter"
    );

    m.def("get_device_properties", &get_device_properties,
        R"docdelimiter(
        Get :py:class:`CudaDeviceProp` for given device.

        Parameters:
            device_id: Cude device id

        Returns:
            CudaDeviceProp: properties of device
        )docdelimiter",
        py::arg("device_id")
    );

    py::class_<DLDataType> dtype(m, "DLDataType",
        R"docdelimiter(
        :ref:`cpp/tensor:dlpack` data type for :py:class:`CudaTensors <CudaTensor>`.

        Parameters:
            code: See :py:class:`DLDataTypeCode`
            bits: Number of bits
            lanes: Number of elements for vector types;
                must be 1 to use with :py:class:`CudaTensor`
        )docdelimiter"
    );
    dtype
        .def(
            py::init<unsigned char, unsigned char, unsigned short>(),
            py::return_value_policy::take_ownership,
            py::arg("code"),
            py::arg("bits"),
            py::arg("lanes")=1
        )
        .def_readonly("code", &DLDataType::code,
            R"docdelimiter(
            See :py:class:`DLDataTypeCode`.
            )docdelimiter"
        )
        .def_readonly("bits", &DLDataType::bits,
            R"docdelimiter(
            Number of bits.
            )docdelimiter"
        )
        .def_readonly("lanes", &DLDataType::lanes,
            R"docdelimiter(
            Mumber of elements for vector types.
            Must be 1 to use with :py:class:`CudaTensor`.
            )docdelimiter"
        )
        .def_property_readonly("itemsize", [](DLDataType self) {
                return itemsize(self);
            },
            R"docdelimiter(
            Number of bytes per element with this data type.
            )docdelimiter"
        )
        .def("__repr__", &dldatatype_repr,
            R"docdelimiter(
            String representation of data type.
            )docdelimiter"
        )
        .def("__str__", &dldatatype_str,
            R"docdelimiter(
            Concise string representation of data type.
            )docdelimiter"
        );

    py::enum_<DLDataTypeCode>(m, "DLDataTypeCode",
            R"docdelimiter(
            :ref:`cpp/tensor:dlpack` type code enum.
            )docdelimiter"
        )
        .value("kDLInt", DLDataTypeCode::kDLInt,
            R"docdelimiter(
            Signed integer.
            )docdelimiter"
        )
        .value("kDLUInt", DLDataTypeCode::kDLUInt,
            R"docdelimiter(
            Unsigned integer.
            )docdelimiter"
        )
        .value("kDLFloat", DLDataTypeCode::kDLFloat,
            R"docdelimiter(
            Floating point number.
            )docdelimiter"
        )
        .export_values();

    m.attr("int8") = dldtype_int8;
    m.attr("uint8") = dldtype_uint8;
    m.attr("int16") = dldtype_int16;
    m.attr("uint16") = dldtype_uint16;
    m.attr("int32") = dldtype_int32;
    m.attr("uint32") = dldtype_uint32;
    m.attr("int64") = dldtype_int64;
    m.attr("uint64") = dldtype_uint64;
    m.attr("float16") = dldtype_float16;
    m.attr("float32") = dldtype_float32;
    m.attr("float64") = dldtype_float64;

    py::class_<CudaTensor> cudatensor(m, "CudaTensor",
        R"docdelimiter(
        Create a new, empty tensor on a GPU device.

        Parameters:
            shape: shape of the tensor
            dtype: data type
            device_id: Cuda device id
        )docdelimiter"
    );
    cudatensor
        .def(
            py::init<std::vector<ssize_t>, DLDataType, int>(),
            py::return_value_policy::take_ownership,
            py::arg("shape"),
            py::arg("dtype")=dldtype_uint8,
            py::arg("device_id")=0
        )
        .def("__add__", &add_scalar,
            R"docdelimiter(
            )docdelimiter",
            py::call_guard<py::gil_scoped_release>(),
            py::return_value_policy::take_ownership,
            py::arg("scalar"),
            py::arg("out")=(CudaTensor*)nullptr,
            py::arg("blocks_per_sm")=BLOCKS_PER_SM,
            py::arg("threads")=512
        )
        .def("__add__", &add_tensor,
            R"docdelimiter(
            )docdelimiter",
            py::call_guard<py::gil_scoped_release>(),
            py::return_value_policy::take_ownership,
            py::arg("scalar"),
            py::arg("out")=(CudaTensor*)nullptr,
            py::arg("blocks_per_sm")=BLOCKS_PER_SM,
            py::arg("threads")=512
        )
        .def("__radd__", &add_scalar,
            R"docdelimiter(
            )docdelimiter",
            py::call_guard<py::gil_scoped_release>(),
            py::return_value_policy::take_ownership,
            py::arg("scalar"),
            py::arg("out")=(CudaTensor*)nullptr,
            py::arg("blocks_per_sm")=BLOCKS_PER_SM,
            py::arg("threads")=512
        )
        .def("__sub__", &sub_scalar,
            R"docdelimiter(
            )docdelimiter",
            py::call_guard<py::gil_scoped_release>(),
            py::return_value_policy::take_ownership,
            py::arg("scalar"),
            py::arg("out")=(CudaTensor*)nullptr,
            py::arg("blocks_per_sm")=BLOCKS_PER_SM,
            py::arg("threads")=512
        )
        .def("__sub__", &sub_tensor,
            R"docdelimiter(
            )docdelimiter",
            py::call_guard<py::gil_scoped_release>(),
            py::return_value_policy::take_ownership,
            py::arg("scalar"),
            py::arg("out")=(CudaTensor*)nullptr,
            py::arg("blocks_per_sm")=BLOCKS_PER_SM,
            py::arg("threads")=512
        )
        .def("__rsub__", &rsub_scalar,
            R"docdelimiter(
            )docdelimiter",
            py::call_guard<py::gil_scoped_release>(),
            py::return_value_policy::take_ownership,
            py::arg("scalar"),
            py::arg("out")=(CudaTensor*)nullptr,
            py::arg("blocks_per_sm")=BLOCKS_PER_SM,
            py::arg("threads")=512
        )
        .def("__mul__", &mul_scalar,
            R"docdelimiter(
            )docdelimiter",
            py::call_guard<py::gil_scoped_release>(),
            py::return_value_policy::take_ownership,
            py::arg("scalar"),
            py::arg("out")=(CudaTensor*)nullptr,
            py::arg("blocks_per_sm")=BLOCKS_PER_SM,
            py::arg("threads")=512
        )
        .def("__mul__", &mul_tensor,
            R"docdelimiter(
            )docdelimiter",
            py::call_guard<py::gil_scoped_release>(),
            py::return_value_policy::take_ownership,
            py::arg("scalar"),
            py::arg("out")=(CudaTensor*)nullptr,
            py::arg("blocks_per_sm")=BLOCKS_PER_SM,
            py::arg("threads")=512
        )
        .def("__rmul__", &mul_scalar,
            R"docdelimiter(
            )docdelimiter",
            py::call_guard<py::gil_scoped_release>(),
            py::return_value_policy::take_ownership,
            py::arg("scalar"),
            py::arg("out")=(CudaTensor*)nullptr,
            py::arg("blocks_per_sm")=BLOCKS_PER_SM,
            py::arg("threads")=512
        )
        .def("__truediv__", &div_scalar,
            R"docdelimiter(
            )docdelimiter",
            py::call_guard<py::gil_scoped_release>(),
            py::return_value_policy::take_ownership,
            py::arg("scalar"),
            py::arg("out")=(CudaTensor*)nullptr,
            py::arg("blocks_per_sm")=BLOCKS_PER_SM,
            py::arg("threads")=512
        )
        .def("__truediv__", &div_tensor,
            R"docdelimiter(
            )docdelimiter",
            py::call_guard<py::gil_scoped_release>(),
            py::return_value_policy::take_ownership,
            py::arg("scalar"),
            py::arg("out")=(CudaTensor*)nullptr,
            py::arg("blocks_per_sm")=BLOCKS_PER_SM,
            py::arg("threads")=512
        )
        .def("__rtruediv__", &rdiv_scalar,
            R"docdelimiter(
            )docdelimiter",
            py::call_guard<py::gil_scoped_release>(),
            py::return_value_policy::take_ownership,
            py::arg("scalar"),
            py::arg("out")=(CudaTensor*)nullptr,
            py::arg("blocks_per_sm")=BLOCKS_PER_SM,
            py::arg("threads")=512
        )
        .def("__lt__", &lt_scalar,
            R"docdelimiter(
            )docdelimiter",
            py::call_guard<py::gil_scoped_release>(),
            py::return_value_policy::take_ownership,
            py::arg("scalar"),
            py::arg("out")=(CudaTensor*)nullptr,
            py::arg("blocks_per_sm")=BLOCKS_PER_SM,
            py::arg("threads")=512
        )
        .def("__lt__", &lt_tensor,
            R"docdelimiter(
            )docdelimiter",
            py::call_guard<py::gil_scoped_release>(),
            py::return_value_policy::take_ownership,
            py::arg("scalar"),
            py::arg("out")=(CudaTensor*)nullptr,
            py::arg("blocks_per_sm")=BLOCKS_PER_SM,
            py::arg("threads")=512
        )
        .def("__le__", &le_tensor,
            R"docdelimiter(
            )docdelimiter",
            py::call_guard<py::gil_scoped_release>(),
            py::return_value_policy::take_ownership,
            py::arg("scalar"),
            py::arg("out")=(CudaTensor*)nullptr,
            py::arg("blocks_per_sm")=BLOCKS_PER_SM,
            py::arg("threads")=512
        )
        .def("__le__", &le_scalar,
            R"docdelimiter(
            )docdelimiter",
            py::call_guard<py::gil_scoped_release>(),
            py::return_value_policy::take_ownership,
            py::arg("scalar"),
            py::arg("out")=(CudaTensor*)nullptr,
            py::arg("blocks_per_sm")=BLOCKS_PER_SM,
            py::arg("threads")=512
        )
        .def("__gt__", &gt_scalar,
            R"docdelimiter(
            )docdelimiter",
            py::call_guard<py::gil_scoped_release>(),
            py::return_value_policy::take_ownership,
            py::arg("scalar"),
            py::arg("out")=(CudaTensor*)nullptr,
            py::arg("blocks_per_sm")=BLOCKS_PER_SM,
            py::arg("threads")=512
        )
        .def("__gt__", [](CudaTensor* self, CudaTensor* other, CudaTensor* out,
                          int blocks_per_sm, int threads) {
                return le_tensor(other, self, out, blocks_per_sm, threads);
            },
            R"docdelimiter(
            )docdelimiter",
            py::call_guard<py::gil_scoped_release>(),
            py::return_value_policy::take_ownership,
            py::arg("scalar"),
            py::arg("out")=(CudaTensor*)nullptr,
            py::arg("blocks_per_sm")=BLOCKS_PER_SM,
            py::arg("threads")=512
        )
        .def("__ge__", &ge_scalar,
            R"docdelimiter(
            )docdelimiter",
            py::call_guard<py::gil_scoped_release>(),
            py::return_value_policy::take_ownership,
            py::arg("scalar"),
            py::arg("out")=(CudaTensor*)nullptr,
            py::arg("blocks_per_sm")=BLOCKS_PER_SM,
            py::arg("threads")=512
        )
        .def("__ge__", [](CudaTensor* self, CudaTensor* other, CudaTensor* out,
                          int blocks_per_sm, int threads) {
                return lt_tensor(other, self, out, blocks_per_sm, threads);
            },
            R"docdelimiter(
            )docdelimiter",
            py::call_guard<py::gil_scoped_release>(),
            py::return_value_policy::take_ownership,
            py::arg("scalar"),
            py::arg("out")=(CudaTensor*)nullptr,
            py::arg("blocks_per_sm")=BLOCKS_PER_SM,
            py::arg("threads")=512
        )
        .def("__eq__", &eq_scalar,
            R"docdelimiter(
            )docdelimiter",
            py::call_guard<py::gil_scoped_release>(),
            py::return_value_policy::take_ownership,
            py::arg("scalar"),
            py::arg("out")=(CudaTensor*)nullptr,
            py::arg("blocks_per_sm")=BLOCKS_PER_SM,
            py::arg("threads")=512
        )
        .def("__eq__", &eq_tensor,
            R"docdelimiter(
            )docdelimiter",
            py::call_guard<py::gil_scoped_release>(),
            py::return_value_policy::take_ownership,
            py::arg("scalar"),
            py::arg("out")=(CudaTensor*)nullptr,
            py::arg("blocks_per_sm")=BLOCKS_PER_SM,
            py::arg("threads")=512
        )
        // TODO add inplace __i*__ methods
        .def("__repr__", &CudaTensor::repr,
            R"docdelimiter(
            )docdelimiter",
            py::return_value_policy::take_ownership
        )
        .def("__getitem__", &CudaTensor::index,
            R"docdelimiter(
            )docdelimiter",
            py::call_guard<py::gil_scoped_release>(),
            py::return_value_policy::take_ownership
        )
        .def("__getitem__", &CudaTensor::slice_simple,
            R"docdelimiter(
            )docdelimiter",
            py::call_guard<py::gil_scoped_release>(),
            py::return_value_policy::take_ownership
        )
        .def("__getitem__", &CudaTensor::slice_complex,
            R"docdelimiter(
            )docdelimiter",
            py::call_guard<py::gil_scoped_release>(),
            py::return_value_policy::take_ownership
        )
        .def("__setitem__", &CudaTensor::setitem_index,
            R"docdelimiter(
            )docdelimiter",
            py::call_guard<py::gil_scoped_release>()
        )
        .def("__setitem__", &CudaTensor::setitem_simple,
            R"docdelimiter(
            )docdelimiter",
            py::call_guard<py::gil_scoped_release>()
        )
        .def("__setitem__", &CudaTensor::setitem_complex,
            R"docdelimiter(
            )docdelimiter",
            py::call_guard<py::gil_scoped_release>()
        )
        .def("__setitem__", &CudaTensor::fill_index,
            R"docdelimiter(
            )docdelimiter",
            py::call_guard<py::gil_scoped_release>()
        )
        .def("__setitem__", &CudaTensor::fill_simple,
            R"docdelimiter(
            )docdelimiter",
            py::call_guard<py::gil_scoped_release>()
        )
        .def("__setitem__", &CudaTensor::fill_complex,
            R"docdelimiter(
            )docdelimiter",
            py::call_guard<py::gil_scoped_release>()
        )
        .def("fill", [](CudaTensor* t, double scalar) -> CudaTensor* {
                return fill(scalar, t);
            },
            R"docdelimiter(
            Fill the tensor with the given scalar value.

            :returns: this tensor
            )docdelimiter",
            py::call_guard<py::gil_scoped_release>(),
            py::arg("scalar")
        )
        .def("fill", [](CudaTensor* t, CudaTensor* o) -> CudaTensor* {
                return copy(o, t);
            },
            R"docdelimiter(
            Copy the given tensor into this tensor.

            :returns: this tensor
            )docdelimiter",
            py::call_guard<py::gil_scoped_release>(),
            py::arg("other")
        )
        .def("sum", &augpy::sum,
            R"docdelimiter(
            Sum all values in the tensor.

            :param upcast: if ``True``, the output scalar tensor will
                be promoted to a more expressive data type to avoid saturation

            :returns: sum as scalar tensor
            )docdelimiter",
            py::call_guard<py::gil_scoped_release>(),
            py::return_value_policy::take_ownership,
            py::arg("upcast")=false
        )
        .def("sum", &augpy::sum_axis,
            R"docdelimiter(
            Sum all values in the tensor along an axis.

            :param axis: which axis to sum along
            :param keepdim: keep the summed dimension with size 1
            :param upcast: if ``True``, the output scalar tensor will
                be promoted to a more expressive data type to avoid saturation
            :param out: use this tensor as output, must have correct
                shape, and same data type if ``upcast`` is ``False``,
                otherwise promoted type is required

            :returns: tensor summed along axis
            )docdelimiter",
            py::call_guard<py::gil_scoped_release>(),
            py::return_value_policy::take_ownership,
            py::arg("axis"),
            py::arg("keepdim")=false,
            py::arg("upcast")=false,
            py::arg("out")=(CudaTensor*)nullptr,
            py::arg("blocks_per_sm")=BLOCKS_PER_SM,
            py::arg("threads")=0
        )
        .def("reshape", &CudaTensor::reshape,
            R"docdelimiter(
            Return a new tensor that uses the same backing memory
            with a different shape. Shape must have same number
            of elements. Only contiguous tensors can be reshaped.

            Parameters:
                shape: new shape
            )docdelimiter",
            py::call_guard<py::gil_scoped_release>(),
            py::return_value_policy::take_ownership,
            py::arg("shape")
        )
        .def_property_readonly("itemsize", [](CudaTensor* t){
                return itemsize(t->dl_tensor.dtype);
            },
            R"docdelimiter(
            Size of the one element in bytes.
            )docdelimiter"
        )
        .def_property_readonly("is_contiguous", &CudaTensor::is_contiguous,
            R"docdelimiter(
            ``True`` if the tensor is contiguous, i.e.,
            elements are located next to each other in memory.
            )docdelimiter"
        )
        .def_property_readonly("dtype", [](CudaTensor* t){
                return t->dl_tensor.dtype;
            },
            R"docdelimiter(
            Tensor data type.
            )docdelimiter"
        )
        .def_property_readonly("ndim", [](CudaTensor* t){
                return t->dl_tensor.ndim;
            },
            R"docdelimiter(
            Number of dimensions.
            )docdelimiter"
        )
        .def_property_readonly("shape", &CudaTensor::pyshape,
            R"docdelimiter(
            Tensor shape.
            )docdelimiter"
        )
        .def_property_readonly("strides", &CudaTensor::pystrides,
            R"docdelimiter(
            Tensor strides, i.e., the number of elements to add
            to a flat tensor to reach the next element for each
            dimension.
            )docdelimiter"
        )
        .def_property_readonly("byte_offset", [](CudaTensor* self) {
                return self->dl_tensor.byte_offset;
            },
            R"docdelimiter(
            Starting offset in bytes for the data pointer.
            )docdelimiter"
        )
        .def_property_readonly("size", [](CudaTensor* self) {
                return numel(self);
            },
            R"docdelimiter(
            Number of elements in the tensor.
            )docdelimiter"
        )
        .def_property_readonly("ptr", [](CudaTensor* self) {
                return reinterpret_cast<size_t>(self->ptr());
            },
            R"docdelimiter(
            Data pointer.
            )docdelimiter"
        )
        .def("numpy", &tensor_to_array1,
            R"docdelimiter(
            Create a new numpy array and start copying data from
            the device to host memory.
            )docdelimiter",
            py::return_value_policy::take_ownership
        )
        .def("numpy", &tensor_to_array2,
            R"docdelimiter(
            Create a new numpy array from the given buffer and
            start copying data from the device to host memory.

            :param array: buffer to create new array from
            )docdelimiter",
            py::return_value_policy::take_ownership,
            py::arg("array")=(py::buffer*)nullptr
        );

    py::class_<RandomNumberGenerator> rng(m, "RandomNumberGenerator",
        R"docdelimiter(
        A convenient wrapper for cuRAND methods
        that fill tensors with pseudo-random numbers.

        Parameters:
            device_id: GPU device ID;
                if ``None``, :ref:`py/core:current_device` is used
            seed: random seed;
                if ``None``, read values from
                `std::random_device <https://en.cppreference.com/w/cpp/numeric/random/random_device>`_
                to create a random seed.
        )docdelimiter"
    );
    rng
        .def(py::init<py::object*, py::object*>(),
            py::return_value_policy::take_ownership,
            py::arg("device_id")=(py::object*)nullptr,
            py::arg("seed")=(py::object*)nullptr
        )
        .def("uniform", &RandomNumberGenerator::uniform,
            R"docdelimiter(
            Fill ``target`` tensor with uniformly distributed number
            in :math:`[v_{min}, v_{max})`.

            .. note::
                This is supported for integer tensors. Values are
                cast from float or double down to the integer type.
                The mean of the values is approximately
                :math:`\frac{v_{max} + v_{min}}{2}`.

            .. warning::
                Saturation is not used.
                :math:`v_{min}` and :math:`v_{max}` must be
                representable in the target tensor data type,
                else values may under or overflow.

            Parameters:
                target: tensor to fill
                vmin: minimum value; can occur
                vmax: maximum value; does not occur
            )docdelimiter",
            py::arg("target"),
            py::arg("vmin"),
            py::arg("vmax"),
            py::arg("blocks_per_sm")=BLOCKS_PER_SM,
            py::arg("threads")=0
        )
        .def("gaussian", &RandomNumberGenerator::gaussian,
            R"docdelimiter(
            Fill ``target`` tensor with Gaussian distributed numbers
            with specified ``mean`` and standard deviation ``std``.

            .. note::
                This is supported for integer tensors. Values are
                drawn from the given distribution, then rounded and
                cast to the data type of the tensor with saturation.
                The values in an integer tensor are thus only
                approximately Gaussian distributed.

            Parameters:
                target: tensor to fill
                mean: Gaussian mean
                std: Gaussian standard deviation
            )docdelimiter",
            py::arg("target"),
            py::arg("mean")=0.0,
            py::arg("std")=1.0,
            py::arg("blocks_per_sm")=BLOCKS_PER_SM,
            py::arg("threads")=0
        );

    py::class_<Decoder> decoder(m, "Decoder",
        R"docdelimiter(
        Wrapper for Nvjpeg-based JPEG decoding,
        created on the :ref:`py/core:current_device`.

        See:
            `Nvjpeg docs <https://docs.nvidia.com/cuda/nvjpeg/index.html#nvjpeg-set-device-mem-padding>`_

        Parameters:
            device_padding: memory padding on the device
            host_padding: memory padding on the host
            gpu_huffman: enable Huffman decoding on the GPU;
                not recommended unless you really need
                to offload from CPU
        )docdelimiter"
    );
    decoder
        .def(
            py::init<size_t, size_t, bool>(),
            py::return_value_policy::take_ownership,
            py::arg("device_padding")=16777216,
            py::arg("host_padding")=8388608,
            py::arg("gpu_huffman")=false
        )
        .def("decode", &Decoder::decode,
            R"docdelimiter(
            Decode a JPEG image using Nvjpeg.
            Output is in :math:`(H,W,C)` format and resides on the GPU device.

            Parameters:
                data: compressed JPEG image as a JFIF string, i.e.,
                    the full file contents
                buffer: optional buffer to use; may be ``None``;
                    if not ``None`` must be big enough to contain
                    the decoded image

            Returns:
                new tensor with decoded image on GPU in :math:`(H,W,C)` format
            )docdelimiter",
            py::return_value_policy::take_ownership,
            py::arg("data"),
            py::arg("buffer")=(CudaTensor*)nullptr
        );

    m.def("import_dltensor", &import_dltensor,
        R"docdelimiter(
        Import a GPU tensor from another library into augpy.

        Parameters:
            tensor_capsule: a Python :py:ref:`capsule <Capsules>` object that contains
                a :any:`DLManagedTensor`
            name: name under which the tensor is stored in the
                :py:ref:`capsule <Capsules>`, e.g., ``"dltensor"`` for Pytorch

        Returns:
            other tensor wrapped in a :py:class:`CudaTensor`
        )docdelimiter",
        py::call_guard<py::gil_scoped_release>(),
        py::return_value_policy::take_ownership,
        py::keep_alive<0, 1>(),
        py::arg("tensor_capsule"),
        py::arg("name")
    );

    m.def("export_dltensor", &export_dltensor,
        R"docdelimiter(
        Export a GPU tensor to be used by another library.

        Parameters:
            pytensor: Python-wrapped CudaTensor
            name: name under which the tensor is stored in the returned
                :py:ref:`capsule <Capsules>`, e.g., `"dltensor"` for Pytorch
            destruct: if ``True``, add a destructor to the
                :py:ref:`capsule <Capsules>` which will delete the tensor
                when the capsule is deleted; only set to ``False`` if you
                know what you're doing

        Returns:
            :py:ref:`capsule <Capsules>` with exported :py:class:`CudaTensor`
        )docdelimiter",
        py::call_guard<py::gil_scoped_release>(),
        py::return_value_policy::take_ownership,
        py::arg("tensor"),
        py::arg("name")="dltensor",
        py::arg("destruct")=true
    );

    m.def("tensor_to_array", &tensor_to_array1,
        R"docdelimiter(
        Copy a given tensor to a new numpy array.
        This initiates an asynchronous copy from device to host memory.
        )docdelimiter",
        py::return_value_policy::take_ownership,
        py::arg("tensor")
    );

    m.def("tensor_to_array", &tensor_to_array2,
        R"docdelimiter(
        Copy a given tensor to a numpy array created from the given buffer ``array``.
        This initiates an asynchronous copy from device to host memory.
        )docdelimiter",
        py::return_value_policy::take_ownership,
        py::arg("tensor"),
        py::arg("array")
    );

    m.def("array_to_tensor", &array_to_tensor1,
        R"docdelimiter(
        Copy a Python buffer into a new tensor on the specified GPU device.
        This initiates an asynchronous copy from host to device memory.
        )docdelimiter",
        py::return_value_policy::take_ownership,
        py::arg("array"),
        py::arg("device_id")=0
    );

    m.def("array_to_tensor", &array_to_tensor2,
        R"docdelimiter(
        Copy a Python buffer to a tensor created from the given buffer ``tensor``.
        This initiates an asynchronous copy from host to device memory.
        )docdelimiter",
        py::return_value_policy::take_ownership,
        py::arg("array"),
        py::arg("tensor")
    );

    py::enum_<WarpScaleMode>(m, "WarpScaleMode",
        R"docdelimiter(
        Enum whether to scale relative to the
        shortest or longest side of the image.
        )docdelimiter"
    )
        .value("WARP_SCALE_SHORTEST", WarpScaleMode::WARP_SCALE_SHORTEST,
            R"docdelimiter(
            Scaling is relative to the shortest side of the image.
            )docdelimiter"
        )
        .value("WARP_SCALE_LONGEST", WarpScaleMode::WARP_SCALE_LONGEST,
            R"docdelimiter(
            Scaling is relative to the longest side of the image.
            )docdelimiter"
        )
    .export_values();

    m.def("make_affine_matrix", &make_affine_matrix,
        R"docdelimiter(
        Create a :math:`2 \times 3` matrix for a set of affine
        transformations.
        This matrix is compatible with the `warpAffine
        <https://docs.opencv.org/3.4/da/d54/group__imgproc__transform.html#ga0203d9ee5fcd28d40dbc4a1ea4451983>`_
        function of OpenCV with the `WARP_INVERSE_MAP
        <https://docs.opencv.org/3.4/da/d54/group__imgproc__transform.html#gga5bb5a1fea74ea38e1a5445ca803ff121aa48be1c433186c4eae1ea86aa0ca75ba>`_
        flag set.

        Transforms are applied in the following order:

        #. shear
        #. scale & aspect ratio
        #. horizontal & vertical mirror
        #. rotation
        #. horizontal & vertical shift

        See:
            :py:func:`make_transform` for a more convenient version of this function.

        Parameters:
            out: output buffer that matrix is written to;
                must be a writeable :math:`2 \times 3` ``float`` buffer
            source_height: :math:`h_s` height of the image in pixels
            source_width: :math:`w_s` width of the image in pixels
            target_height: :math:`h_t` height of the output canvas in pixels
            target_width: :math:`w_t` width of the output canvas in pixels
            angle: clockwise angle in degrees
                with image center as rotation axis
            scale: scale factor relative to output size;
                1 means fill target height or width wise depending
                on ``scale_mode`` and whichever is longest/shortest;
                larger values will crop,
                smaller values leave empty space in the output canvas
            aspect: controls the aspect ratio;
                1 means same as input, values greater 1
                increase the width and reduce the height
            shifty: shift the image in y direction (vertical);
                0 centers the image on the output canvas;
                -1 means shift up as much as possible;
                1 means shfit down as much as possible;
                the maximum distance to shift is
                :math:`max(scale \cdot h_s - h_t, h_t - scale \cdot h_s)`
            shiftx: same as ``shifty``, but in x direction (horizontal)
            sheary: controls up/down shear;
                for every pixel in the x direction move ``sheary`` pixels
                in y direction
            shearx: same as ``sheary`` but controls left/right shear
            hmirror: if ``True`` flip image horizontally
            vmirror: if ``True`` flip image vertically
            scale_mode: if :py:attr:`WarpScaleMode.WARP_SCALE_SHORTEST` scale
                is relative to shortest side;
                this fills the output canvas, cropping the image
                if necessary;
                if :py:attr:`WarpScaleMode.WARP_SCALE_LONGEST` scale
                is relative to longest side;
                this ensures the image is contained inside the
                output canvas, but leaves empty space
            max_supersampling: upper limit for recommended supersampling

        Returns:
            recommended supersampling factor for the warp
        )docdelimiter",
        py::return_value_policy::take_ownership,
        py::arg("out"),
        py::arg("source_height"),
        py::arg("source_width"),
        py::arg("target_height"),
        py::arg("target_width"),
        py::arg("angle")=0.0,
        py::arg("scale")=1.0,
        py::arg("aspect")=1.0,
        py::arg("shifty")=0.0,
        py::arg("shiftx")=0.0,
        py::arg("sheary")=0.0,
        py::arg("shearx")=0.0,
        py::arg("hmirror")=false,
        py::arg("vmirror")=false,
        py::arg("scale_mode")=WarpScaleMode::WARP_SCALE_SHORTEST,
        py::arg("max_supersampling")=3
    );

    m.def("warp_affine", &warp_affine,
        R"docdelimiter(
        Takes an image in channels-last format :math:`(H, W, C)`
        and affine warps it into a given output tensor in
        channels-first format :math:`(C, H, W)`.
        Any blank canvas is filled with a background color.
        The warp is performed with bi-linear and supersampling.

        Parameters:
            src: image tensor
            dst: target tensor
            matrix: :math:`2 \times 3` ``float`` transformation matrix,
                see :py:func:`make_affine_matrix` for details
            background: background color to fill empty canvas
            supersampling: supersampling factor, e.g., 3 means
                9 samples are taken in a :math:`3 \times 3` grid
        )docdelimiter",
        py::return_value_policy::take_ownership,
        py::arg("src"),
        py::arg("dst"),
        py::arg("matrix"),
        py::arg("background"),
        py::arg("supersampling")
    );

    m.def("lighting", &lighting,
        R"docdelimiter(
        Apply lighting augmentation to a batch of images.
        This is a four-step process:

        #. Normalize values :math:`v_{norm} = \frac{v - v_{min}}{v_{max}-v_{min}}`
           with :math:`v_{max}` the minimum and :math:`v_{max}` the maximum
           lightness value
        #. Apply contrast change
        #. Apply gamma correction
        #. Denormalize values :math:`v' = v_{norm} * (v_{max}-v_{min}) + v_{min}`

        To change contrast two reference functions are used.
        With contrast :math:`\mathcal{c} \ge 0`, i.e., increased contrast,
        the following function is used:

        .. math::

            f_{pos}(v) =
            \frac{1.0037575963899724}{1 + exp(6.279 + v \cdot 12.558)} - 0.0018787981949862

        With contrast :math:`\mathcal{c} < 0`, i.e., decreased contrast,
        the following function is used:

        .. math::

            f_{neg}(v) =
            0.1755606108304832 \cdot atanh(v \cdot 1.986608 - 0.993304) + 0.5

        The final value is
        :math:`v' = (1-\mathcal{c}) \cdot v + \mathcal{c} \cdot f(v)`.

        Brightness and color changes are done via gamma correction.

        .. math::

            v' = v^{\gamma_{gray} \cdot \gamma_c}

        with :math:`\gamma_{gray}` the gamma for overall lightness and
        :math:`\gamma_{c}` the per-channel gamma.

        Parameters:
            tensor: image tensor in :math:`(N,C,H,W)` format
            gammagrays: tensor of :math:`N` gamma gray values
            gammacolors: tensor of :math:`C\cdot N` gamma values in the format
                :math:`\gamma_{1,1}, \gamma_{1,2}, ..., \gamma_{1,C},
                \gamma_{2,1}, \gamma_{2,2}, ... \gamma_{N,C-1}, \gamma_{N,C}`
            contrasts: tensor of :math:`N` contrast values in :math:`[-1, 1]`
            vmin: minimum lightness value in images
            vmax: maximum lightness value in images
            out: output tensor (may be ``None``)

        Returns:
            new tensor if ``out`` is ``None``, else ``out``
        )docdelimiter",
        py::call_guard<py::gil_scoped_release>(),
        py::return_value_policy::take_ownership,
        py::arg("imtensor"),
        py::arg("gammagrays"),
        py::arg("gammacolors"),
        py::arg("contrasts"),
        py::arg("vmin"),
        py::arg("vmax"),
        py::arg("out")=(CudaTensor*)nullptr
    );

    m.def("gaussian_blur", &gaussian_blur,
        R"docdelimiter(
        Apply Gaussian blur to a batch of images.

        Maximum kernel size can be calculated like this:

        ``ksize = max(3, int(max(sigmas) * 6.6 - 2.3) | 1)``

        I.e., ``ksize`` is at least 3 and always odd.

        The given kernel size defines the upper limit.
        The actual kernel size is calculated with the
        formula above and clipped at the given maximum.

        Smaller values can be given to trade speed vs quality.
        Bigger values typically do not visibly improve quality.

        Odd values are strongly recommended for best results.
        For even values, the center of the kernel is below
        and to the right of the true center.
        This means the output is shifted up and left by half
        a pixel.
        This can lead to inconsistencies between images
        in the batch.
        Images with large sigmas may be shifted, while smaller
        sigmas mean no shift occurs.

        Parameters:
            input: batch tensor with images in first dimension
            sigmas: float tensor with one sigma value per image in the batch
            max_ksize: maximum kernel size in pixels
            out: output tensor (may be ``None``)

        Returns:
            new tensor if ``out`` is ``None``, else ``out``
        )docdelimiter",
        py::call_guard<py::gil_scoped_release>(),
        py::return_value_policy::take_ownership,
        py::arg("input"),
        py::arg("sigmas"),
        py::arg("max_ksize"),
        py::arg("out")=(CudaTensor*)nullptr
    );
    m.def("gaussian_blur_single", &gaussian_blur_single,
        R"docdelimiter(
        Apply Gaussian blur to a single image.

        Kernel size is calculated like this:

        ``ksize = max(3, int(sigma * 6.6 - 2.3) | 1)``

        I.e., ``ksize`` is at least 3 and always odd.


        Parameters:
            input: image tensor in channel-first format
            sigma: standard deviation of the kernel
            out: output tensor (may be ``None``)

        Returns:
            new tensor if ``out`` is ``None``, else ``out``
        )docdelimiter",
        py::call_guard<py::gil_scoped_release>(),
        py::return_value_policy::take_ownership,
        py::arg("input"),
        py::arg("sigma"),
        py::arg("out")=(CudaTensor*)nullptr
    );
    m.def("box_blur_single", &box_blur_single,
        R"docdelimiter(
        Apply box blur to a single image.

        Kernel size describes both width and height in pixels
        of the area in the input that is averaged for each
        output pixel.
        Odd values are recommended for best results.
        For even values, the center of the kernel is below
        and to the right of the true center.
        This means the output is shifted up and left by half
        a pixel.


        Parameters:
            input: image tensor in channel-first format
            ksize: kernel size in pixels
            out: output tensor (may be ``None``)

        Returns:
            new tensor if ``out`` is ``None``, else ``out``
        )docdelimiter",
        py::call_guard<py::gil_scoped_release>(),
        py::return_value_policy::take_ownership,
        py::arg("input"),
        py::arg("ksize"),
        py::arg("out")=(CudaTensor*)nullptr
    );

    py::register_exception<cuda_error>(m, "CudaError");
    py::register_exception<cnmem_error>(m, "MemoryError");
    py::register_exception<nvjpeg_error>(m, "NvJpegError");
    py::register_exception<curand_error>(m, "CuRandError");
    py::register_exception<cutlass_error>(m, "CutlassError");

    m.def("meminfo", &meminfo,
        R"docdelimiter(
        For the device defined by ``device_id``,
        return the current used, free, and total memory in bytes.
        )docdelimiter",
        py::arg("device_id")=0
    );
    m.def("enable_profiler", &enable_profiler,
        R"docdelimiter(
        Enable the Cuda profiler.
        )docdelimiter"
    );
    m.def("disable_profiler", &disable_profiler,
        R"docdelimiter(
        Disable the Cuda profiler.
        )docdelimiter"
    );
    m.def("nvtx_range_start", &nvtx_range_start,
        R"docdelimiter(
        Tell the Nvidia profiler to start a new `nvtx
        <https://docs.nvidia.com/cuda/profiler-users-guide/index.html#nvtx>`_
        range.
        Can be used to place marks in profiling output.

        Parameters:
            msg: Message attached to the range

        Returns:
            range ID to be used with :py:func:`nvtx_range_end`
        )docdelimiter",
        py::arg("msg")
    );
    m.def("nvtx_range_end", &nvtx_range_end,
        R"docdelimiter(
        Tell the Nvidia profiler to end the given `nvtx
        <https://docs.nvidia.com/cuda/profiler-users-guide/index.html#nvtx>`_
        range.

        Parameters:
            end: ID of the range to end
        )docdelimiter",
        py::arg("end")
    );
    m.def("init", &init,
        R"docdelimiter(
        Set the `cudaDeviceScheduleYield
        <https://docs.nvidia.com/cuda/cuda-runtime-api/group__CUDART__DEVICE.html#group__CUDART__DEVICE_1g69e73c7dda3fc05306ae7c811a690fac>`_
        flag for the :ref:`py/core:current_device`.

        .. warning::

            EXPERIMENTAL! MAY REDUCE GPU THROUGHPUT AND BREAK MANY THINGS!
        )docdelimiter"
    );

    m.def("fma", &augpy::fma,
        R"docdelimiter(
        Compute a fused multiply-add on a scalar and two tensors, i.e.,

        .. math::

            r = s \cdot t_1 \cdot t_2

        If ``tensor1`` has an unsigned integer data type,
        then ``tensor2`` must have the signed version of the same type,
        e.g., a ``uint8`` tensor must be paired with a ``int8`` tensor.

        Parameters:
            scalar: scalar factor
            tensor1: tensor :math:`t_1`
            tensor2: tensor :math:`t_2`
            out: optional output tensor :math:`r`

        Returns:
            new tensor if ``out`` is ``None``, else ``out``
        )docdelimiter",
        py::call_guard<py::gil_scoped_release>(),
        py::return_value_policy::take_ownership,
        py::arg("scalar"),
        py::arg("tensor1"),
        py::arg("tensor2"),
        py::arg("out")=(CudaTensor*)nullptr,
        py::arg("blocks_per_sm")=BLOCKS_PER_SM,
        py::arg("threads")=512
    );
    m.def("add", &add_scalar,
        R"docdelimiter(
        Add a ``scalar`` value to a ``tensor``.

        :param tensor: tensor
        :param scalar: scalar value
        :param out: optional output tensor

        :returns: new tensor if ``out`` is ``None``, else ``out``
        )docdelimiter",
        py::call_guard<py::gil_scoped_release>(),
        py::return_value_policy::take_ownership,
        py::arg("tensor"),
        py::arg("scalar"),
        py::arg("out")=(CudaTensor*)nullptr,
        py::arg("blocks_per_sm")=BLOCKS_PER_SM,
        py::arg("threads")=512
    );
    m.def("add", &add_tensor,
        R"docdelimiter(
        Add ``tensor2`` to ``tensor1``.

        :param tensor1: first tensor
        :param tensor2: second tensor
        :param out: optional output tensor

        :returns: new tensor if ``out`` is ``None``, else ``out``
        )docdelimiter",
        py::call_guard<py::gil_scoped_release>(),
        py::return_value_policy::take_ownership,
        py::arg("tensor1"),
        py::arg("tensor2"),
        py::arg("out")=(CudaTensor*)nullptr,
        py::arg("blocks_per_sm")=BLOCKS_PER_SM,
        py::arg("threads")=512
    );
    m.def("sub", &sub_scalar,
        R"docdelimiter(
        Subtract a ``scalar`` value from a ``tensor``.

        :param tensor: tensor
        :param scalar: scalar value
        :param out: optional output tensor

        :returns: new tensor if ``out`` is ``None``, else ``out``
        )docdelimiter",
        py::call_guard<py::gil_scoped_release>(),
        py::return_value_policy::take_ownership,
        py::arg("tensor"),
        py::arg("scalar"),
        py::arg("out")=(CudaTensor*)nullptr,
        py::arg("blocks_per_sm")=BLOCKS_PER_SM,
        py::arg("threads")=512
    );
    m.def("sub", &sub_tensor,
        R"docdelimiter(
        Subtract ``tensor2`` from ``tensor1``.

        :param tensor1: first tensor
        :param tensor2: second tensor
        :param out: optional output tensor

        :returns: new tensor if ``out`` is ``None``, else ``out``
        )docdelimiter",
        py::call_guard<py::gil_scoped_release>(),
        py::return_value_policy::take_ownership,
        py::arg("tensor1"),
        py::arg("tensor2"),
        py::arg("out")=(CudaTensor*)nullptr,
        py::arg("blocks_per_sm")=BLOCKS_PER_SM,
        py::arg("threads")=512
    );
    m.def("rsub", &rsub_scalar,
        R"docdelimiter(
        Subtract a ``tensor`` from a ``scalar`` value.

        Parameters:
            tensor: tensor
            scalar: scalar value
            out: optional output tensor

        Returns:
            new tensor if ``out`` is ``None``, else ``out``
        )docdelimiter",
        py::call_guard<py::gil_scoped_release>(),
        py::return_value_policy::take_ownership,
        py::arg("tensor"),
        py::arg("scalar"),
        py::arg("out")=(CudaTensor*)nullptr,
        py::arg("blocks_per_sm")=BLOCKS_PER_SM,
        py::arg("threads")=512
    );
    m.def("mul", &mul_scalar,
        R"docdelimiter(
        Multiply a ``tensor`` by a ``scalar`` value.

        :param tensor: tensor
        :param scalar: scalar value
        :param out: optional output tensor

        :returns: new tensor if ``out`` is ``None``, else ``out``
        )docdelimiter",
        py::call_guard<py::gil_scoped_release>(),
        py::return_value_policy::take_ownership,
        py::arg("tensor"),
        py::arg("scalar"),
        py::arg("out")=(CudaTensor*)nullptr,
        py::arg("blocks_per_sm")=BLOCKS_PER_SM,
        py::arg("threads")=512
    );
    m.def("mul", &mul_tensor,
        R"docdelimiter(
        Multiply ``tensor1`` by ``tensor2``.

        :param tensor1: first tensor
        :param tensor2: second tensor
        :param out: optional output tensor

        :returns: new tensor if ``out`` is ``None``, else ``out``
        )docdelimiter",
        py::call_guard<py::gil_scoped_release>(),
        py::return_value_policy::take_ownership,
        py::arg("tensor1"),
        py::arg("tensor2"),
        py::arg("out")=(CudaTensor*)nullptr,
        py::arg("blocks_per_sm")=BLOCKS_PER_SM,
        py::arg("threads")=512
    );
    m.def("div", &div_scalar,
        R"docdelimiter(
        Divide a ``tensor`` by a ``scalar`` value.

        :param tensor: tensor
        :param scalar: scalar value
        :param out: optional output tensor

        :returns: new tensor if ``out`` is ``None``, else ``out``
        )docdelimiter",
        py::call_guard<py::gil_scoped_release>(),
        py::return_value_policy::take_ownership,
        py::arg("tensor"),
        py::arg("scalar"),
        py::arg("out")=(CudaTensor*)nullptr,
        py::arg("blocks_per_sm")=BLOCKS_PER_SM,
        py::arg("threads")=512
    );
    m.def("div", &div_tensor,
        R"docdelimiter(
        Divide `tensor1` by `tensor2`.

        :param tensor1: first tensor
        :param tensor2: second tensor
        :param out: optional output tensor

        :returns: new tensor if ``out`` is ``None``, else ``out``
        )docdelimiter",
        py::call_guard<py::gil_scoped_release>(),
        py::return_value_policy::take_ownership,
        py::arg("tensor1"),
        py::arg("tensor2"),
        py::arg("out")=(CudaTensor*)nullptr,
        py::arg("blocks_per_sm")=BLOCKS_PER_SM,
        py::arg("threads")=512
    );
    m.def("rdiv", &rdiv_scalar,
        R"docdelimiter(
        Divide a ``scalar`` value by a ``tensor``.

        Parameters:
            tensor: tensor
            scalar: scalar value
            out: optional output tensor

        Returns:
            new tensor if ``out`` is ``None``, else ``out``
        )docdelimiter",
        py::call_guard<py::gil_scoped_release>(),
        py::return_value_policy::take_ownership,
        py::arg("tensor"),
        py::arg("scalar"),
        py::arg("out")=(CudaTensor*)nullptr,
        py::arg("blocks_per_sm")=BLOCKS_PER_SM,
        py::arg("threads")=512
    );
    m.def("cast", &cast_tensor,
        R"docdelimiter(
        Read values from ``tensor``, cast them to the data type of
        ``out`` and store them there.
        ``tensor`` and ``out`` must have the same shape.

        :param tensor: source tensor
        :param out: output tensor
        )docdelimiter",
        py::call_guard<py::gil_scoped_release>(),
        py::return_value_policy::take_ownership,
        py::arg("tensor"),
        py::arg("out"),
        py::arg("blocks_per_sm")=BLOCKS_PER_SM,
        py::arg("threads")=0
    );
    m.def("lt", &lt_scalar,
        R"docdelimiter(
        Compute ``tensor < scalar`` as ``uint8`` tensor,
        where ``1`` means the condition is met and ``0`` otherwise.

        :param tensor: tensor
        :param scalar: scalar value
        :param out: optional output tensor

        :returns: new tensor if ``out`` is ``None``, else ``out``
        )docdelimiter",
        py::call_guard<py::gil_scoped_release>(),
        py::return_value_policy::take_ownership,
        py::arg("tensor"),
        py::arg("scalar"),
        py::arg("out")=(CudaTensor*)nullptr,
        py::arg("blocks_per_sm")=BLOCKS_PER_SM,
        py::arg("threads")=512
    );
    m.def("gt", &gt_scalar,
        R"docdelimiter(
        Compute ``tensor > scalar`` as ``uint8`` tensor,
        where ``1`` means the condition is met and ``0`` otherwise.

        :param tensor: tensor
        :param scalar: scalar value
        :param out: optional output tensor

        :returns: new tensor if ``out`` is ``None``, else ``out``
        )docdelimiter",
        py::call_guard<py::gil_scoped_release>(),
        py::return_value_policy::take_ownership,
        py::arg("tensor"),
        py::arg("scalar"),
        py::arg("out")=(CudaTensor*)nullptr,
        py::arg("blocks_per_sm")=BLOCKS_PER_SM,
        py::arg("threads")=512
    );
    m.def("le", &le_scalar,
        R"docdelimiter(
        Compute ``tensor <= scalar`` as ``uint8`` tensor,
        where ``1`` means the condition is met and ``0`` otherwise.

        :param tensor: tensor
        :param scalar: scalar value
        :param out: optional output tensor

        :returns: new tensor if ``out`` is ``None``, else ``out``
        )docdelimiter",
        py::call_guard<py::gil_scoped_release>(),
        py::return_value_policy::take_ownership,
        py::arg("tensor"),
        py::arg("scalar"),
        py::arg("out")=(CudaTensor*)nullptr,
        py::arg("blocks_per_sm")=BLOCKS_PER_SM,
        py::arg("threads")=512
    );
    m.def("ge", &ge_scalar,
        R"docdelimiter(
        Compute ``tensor >= scalar`` as ``uint8`` tensor,
        where ``1`` means the condition is met and ``0`` otherwise.

        :param tensor: tensor
        :param scalar: scalar value
        :param out: optional output tensor

        :returns: new tensor if ``out`` is ``None``, else ``out``
        )docdelimiter",
        py::call_guard<py::gil_scoped_release>(),
        py::return_value_policy::take_ownership,
        py::arg("tensor"),
        py::arg("scalar"),
        py::arg("out")=(CudaTensor*)nullptr,
        py::arg("blocks_per_sm")=BLOCKS_PER_SM,
        py::arg("threads")=512
    );
    m.def("eq", &eq_scalar,
        R"docdelimiter(
        Compute ``tensor == scalar`` as ``uint8`` tensor,
        where ``1`` means the condition is met and ``0`` otherwise.

        :param tensor: tensor
        :param scalar: scalar value
        :param out: optional output tensor

        :returns: new tensor if ``out`` is ``None``, else ``out``
        )docdelimiter",
        py::call_guard<py::gil_scoped_release>(),
        py::return_value_policy::take_ownership,
        py::arg("tensor"),
        py::arg("scalar"),
        py::arg("out")=(CudaTensor*)nullptr,
        py::arg("blocks_per_sm")=BLOCKS_PER_SM,
        py::arg("threads")=512
    );
    m.def("lt", &lt_tensor,
        R"docdelimiter(
        Compute ``tensor1 >= tensor2`` as ``uint8`` tensor,
        where ``1`` means the condition is met and ``0`` otherwise.

        :param tensor1: first tensor
        :param tensor2: second tensor
        :param out: optional output tensor

        :returns: new tensor if ``out`` is ``None``, else ``out``
        )docdelimiter",
        py::call_guard<py::gil_scoped_release>(),
        py::return_value_policy::take_ownership,
        py::arg("tensor1"),
        py::arg("tensor2"),
        py::arg("out")=(CudaTensor*)nullptr,
        py::arg("blocks_per_sm")=BLOCKS_PER_SM,
        py::arg("threads")=512
    );
    m.def("le", &le_tensor,
        R"docdelimiter(
        Compute ``tensor1 >= tensor2`` as ``uint8`` tensor,
        where ``1`` means the condition is met and ``0`` otherwise.

        :param tensor1: first tensor
        :param tensor2: second tensor
        :param out: optional output tensor

        :returns: new tensor if ``out`` is ``None``, else ``out``
        )docdelimiter",
        py::call_guard<py::gil_scoped_release>(),
        py::return_value_policy::take_ownership,
        py::arg("tensor1"),
        py::arg("tensor2"),
        py::arg("out")=(CudaTensor*)nullptr,
        py::arg("blocks_per_sm")=BLOCKS_PER_SM,
        py::arg("threads")=512
    );
    m.def("eq", &eq_tensor,
        R"docdelimiter(
        Compute ``tensor1 == tensor2`` as ``uint8`` tensor,
        where ``1`` means the condition is met and ``0`` otherwise.

        :param tensor1: first tensor
        :param tensor2: second tensor
        :param out: optional output tensor

        :returns: new tensor if ``out`` is ``None``, else ``out``
        )docdelimiter",
        py::call_guard<py::gil_scoped_release>(),
        py::return_value_policy::take_ownership,
        py::arg("tensor1"),
        py::arg("tensor2"),
        py::arg("out")=(CudaTensor*)nullptr,
        py::arg("blocks_per_sm")=BLOCKS_PER_SM,
        py::arg("threads")=512
    );

    m.def("cast", &cast_type,
        R"docdelimiter(
        Create a new tensor with values from ``tensor``
        cast to the given data type ``dtype``.

        :param tensor: source tensor
        :param dtype: target data type
        :returns: new tensor with given data type
        )docdelimiter",
        py::call_guard<py::gil_scoped_release>(),
        py::return_value_policy::take_ownership,
        py::arg("tensor"),
        py::arg("dtype"),
        py::arg("blocks_per_sm")=BLOCKS_PER_SM,
        py::arg("threads")=0
    );
    m.def("gemm", &gemm,
        R"docdelimiter(
        Calculate the matrix multiplication of two 2D tensors.
        More specifically calculates

        .. math::

            C = A \times (\alpha \cdot B) + \beta \cdot C

        Only ``float`` and ``double`` are supported.

        All tensors must have the same data type.

        All tensors must be contiguous.

        Returns:
            new output tensor if ``C`` is ``None``, otherwise ``C``
        )docdelimiter",
        py::call_guard<py::gil_scoped_release>(),
        py::arg("A"),
        py::arg("B"),
        py::arg("C")=(CudaTensor*)nullptr,
        py::arg("alpha")=1.0,
        py::arg("beta")=0.0
    );

    m.def("copy", &copy,
        R"docdelimiter(
        Copy ``src`` into ``dst``.
        Supports broadcasting.
        )docdelimiter",
        py::call_guard<py::gil_scoped_release>(),
        py::arg("src"),
        py::arg("dst"),
        py::arg("blocks_per_sm")=BLOCKS_PER_SM,
        py::arg("threads")=0
    );

    m.def("fill", &augpy::fill,
        R"docdelimiter(
        Fill `src` with the given `scalar` value.
        )docdelimiter",
        py::call_guard<py::gil_scoped_release>(),
        py::arg("scalar"),
        py::arg("dst"),
        py::arg("blocks_per_sm")=BLOCKS_PER_SM,
        py::arg("threads")=0
    );

    m.def("sum", &augpy::sum,
        R"docdelimiter(
        Sum all elements in a tensor with saturation.

        :param tensor: tensor to sum, must be contiguous
        :param upcast: if ``True``, returns tensor with
            ``float`` or ``double`` type

        :returns: sum value as scalar tensor
        )docdelimiter",
        py::call_guard<py::gil_scoped_release>(),
        py::return_value_policy::take_ownership,
        py::arg("tensor"),
        py::arg("upcast")=false
    );

    m.def("sum", &augpy::sum_axis,
        R"docdelimiter(
        Sum of all elements along an axis in a tensor with saturation.

        :param tensor: tensor to sum, may be strided
        :param axis: axis index to sum along
        :param keepdim: if ``True``, keep sum axis dimension with length 1
        :param upcast: if ``True``, returns tensor with
            ``float`` or ``double`` type
        :param out: output tensor (may be ``None``)

        :returns: tensor with values summed along axis,
            or ``None`` if ``out`` is tensor
        )docdelimiter",
        py::call_guard<py::gil_scoped_release>(),
        py::return_value_policy::take_ownership,
        py::arg("tensor"),
        py::arg("axis"),
        py::arg("keepdim")=false,
        py::arg("upcast")=false,
        py::arg("out")=(CudaTensor*)nullptr,
        py::arg("blocks_per_sm")=BLOCKS_PER_SM,
        py::arg("num_threads")=0
    );

    m.def("all", &augpy::all,
        R"docdelimiter(
        Check whether all elements in a tensor are greater zero.

        Parameters:
            tensor: tensor to sum, must be contiguous

        Returns:
            ``0`` or ``1`` as scalar ``uint8`` tensor
        )docdelimiter",
        py::call_guard<py::gil_scoped_release>(),
        py::return_value_policy::take_ownership,
        py::arg("tensor")
    );

    m.def("empty_like", &augpy::empty_like,
        R"docdelimiter(
        Create a new tensor with the same shape,
        dtype and on the same device as ``tensor``.
        )docdelimiter",
        py::call_guard<py::gil_scoped_release>(),
        py::return_value_policy::take_ownership,
        py::arg("tensor")
    );
}
