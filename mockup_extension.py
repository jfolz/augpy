import re
from pathlib import Path
from collections import defaultdict
from functools import cmp_to_key
from types import FunctionType

from sphinx.util.typing import stringify

from augpy import _augpy


HEADER = """from typing import Tuple
from typing import List


class pybind11_object:
    pass


class buffer:
    \"\"\"Any object that supports the buffer interface, like bytearray or numpy.ndarray.\"\"\"
    pass


class capsule:
    \"\"\"Python object that contains a reference to a C object.\"\"\"
    pass
"""


INCLUDE = {
    '__init__',
    '__call__',
}


def _members(obj):
    for name in dir(obj):
        if not name.startswith('__') or name in INCLUDE:
            m = getattr(obj, name)
            if not hasattr(m, '__objclass__') or m.__objclass__ is obj:
                yield name, m


def _cleanup_type_hints(
        signature,
        __self_pattern=re.compile(r'self: [^,)]+'),
        __default_pattern=re.compile(r'<[^\s]+ ([^,]+)>'),
):
    signature = stringify(signature)
    signature = signature.replace('augpy._augpy.', '').replace('augpy::', '')
    # signature = __self_pattern.sub('self', signature)
    signature = __default_pattern.sub(r'\1', signature)
    return signature


def _key(obj):
    if hasattr(obj, '__members__'):
        return 'enum'
    elif isinstance(obj, type):
        return 'class '
    elif isinstance(obj, property):
        return '@property\n    def '
    elif type(obj) == FunctionType or callable(obj):
        return 'def '
    else:
        return ''


def _signature(
        obj,
        is_method,
        __normal=re.compile(r'^[a-zA-Z_][a-zA-Z0-9_]*(\(.*\)(?:\s+->\s+.+|))'),
):
    signature = '()'
    try:
        hint, _, _ = obj.__doc__.strip('\n').partition('\n')
        m = __normal.search(hint)
        if m:
            # print(hint)
            signature = m.groups(0)[0]
    except AttributeError:
        pass
    if is_method and 'self' not in signature:
        first, sep, last = signature.partition('(')
        return f'{first}{sep}self{", " if not last.startswith(")") else ""}{last}'
    return signature


def _superclass(obj):
    try:
        return f'({obj.mro()[1].__name__})'
    except IndexError:
        return ''


def _write(name, obj, signature, indent=0, key=None):
    key = key or _key(obj)
    signature = _cleanup_type_hints(signature)
    lines = [f'{" "*indent}{key}{name}{signature}:']
    if obj.__doc__:
        lines.append(f'{" "*(indent+4)}"""{obj.__doc__}"""')
    lines.append(f'{" "*(indent+4)}pass')
    return '\n'.join(lines)


def handle_function(name, obj, indent=0, is_method=False, key=None):
    key = key or _key(obj)
    return [(name, _write(name, obj, _signature(obj, is_method), indent, key=key))]


def handle_class(name, obj, indent=0):
    lines = [_write(name, obj, _superclass(obj), indent)]
    for meth_name, meth in _members(obj):
        _, code = HANDLERS[_key(meth)](meth_name, meth, indent+4, is_method=True)[0]
        code = code.replace(name, f"'{name}'")
        lines.append(code)
    return [(name, '\n\n'.join(lines))]


def handle_attribute(name, obj, indent=0, **__):
    return [(name, f'{" "*indent}{name} = {repr(obj)}')]


def handle_enum(name, obj, indent=0, **__):
    lines = [
        _write(name, obj, '', indent, key='class '),
        handle_function(
            '__init__', obj.__init__, indent+4, is_method=True
        )[0][1].replace(name, f"'{name}'"),
    ]
    for member_name in obj.__members__:
        member_obj = getattr(obj, member_name)
        code = handle_function(
            member_name,
            member_obj,
            indent=indent + 4,
            is_method=True,
            key='@property\n    def '
        )[0][1]
        lines.append(code)
    defs = [(name, '\n'.join(lines))]
    for i, member_name in enumerate(obj.__members__):
        defs.append((member_name, f'{member_name} = {name}({i})'))
    return defs


HANDLERS = {
    'enum': handle_enum,
    'class ': handle_class,
    'def ': handle_function,
    '@property\n    def ': handle_function,
    '': handle_attribute,
}


def main():
    definitions = {}
    dependencies = defaultdict(set)

    def cmp(a, b):
        code = definitions[a]
        if code.startswith('def'):
            return 1
        if a in dependencies[b]:
            return -1
        if b in dependencies[a]:
            return 1
        return 0

    out = [f'"""{_augpy.__doc__}"""', HEADER]
    for name, obj in _members(_augpy):
        if name in definitions:
            continue
        try:
            for name, code in HANDLERS[_key(obj)](name, obj):
                definitions[name] = code
        except KeyError:
            print('ignored', name, obj)

    for name in definitions:
        for other, code in definitions.items():
            if name == other:
                continue
            if name in code:
                dependencies[other].add(name)

    order = sorted(definitions, key=cmp_to_key(cmp))
    for name in order:
        out.append(definitions[name])
    with Path('augpy', '_augpy.py').open('w') as f:
        f.write('\n\n\n'.join(out) + '\n\n\n')
        f.write('all = [\n' + (',\n'.join(map(repr, order))) + '\n]\n')


if __name__ == '__main__':
    main()
