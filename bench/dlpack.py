import time
import weakref

import matplotlib.pyplot as plt

# import tqdm
import torch
from torch.utils import dlpack

import augpy


KB = 1024
MB = KB*KB


path = 'images/potato/anotherone256.jpg'
with open(path, 'rb') as f:
    data = f.read()
stream = augpy.CudaStream()
stream.activate()
decoder = augpy.Decoder()
print(decoder)
buffer = augpy.CudaTensor((16*MB,))
h, w, c = decoder.decode(data, buffer).shape


def hello(x):
    print('hello', x)


buffer = augpy.CudaTensor((16, h, w, c))
i = -1
image = decoder.decode(data, buffer[i])
stream.synchronize()
for i in range(1000):  # tqdm.tqdm(range(10)):
    tensor = augpy.CudaTensor((1*MB,))
    print(tensor)
    capsule = augpy.export_dltensor(tensor, "dltensor")
    torch_tensor = dlpack.from_dlpack(capsule)
    print(torch_tensor)

for i in range(1000):  # tqdm.tqdm(range(10)):
    torch_tensor = torch.zeros((1, 2, 3, 4, 5))
    capsule = dlpack.to_dlpack(torch_tensor)
    augpy.import_dltensor(capsule, "dltensor")

torch_tensor = dlpack.from_dlpack(augpy.export_dltensor(image, "dltensor"))
np_image = torch_tensor.cpu().numpy()
print(buffer, buffer.shape, buffer.strides, buffer.itemsize)
plt.imshow(np_image)
plt.show()
#np_image = augpy.tensor_to_array(buffer)
#print(np_image.shape)
#plt.imshow(np_image)
#plt.show()
