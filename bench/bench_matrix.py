import random

import numpy as np

import augpy

from .benchtools import bench
from .warptools import make_transform
from .warptools import make_affine_matrix


class Test(object):
    def __init__(self, tensor=None):
        self.dltensor = tensor


def random_random(num):
    for _ in range(num):
        random.random()


def random_uniform(num):
    for _ in range(num):
        random.uniform(-45, 45)


def create_instance(num):
    thing = 'blabla'
    for _ in range(num):
        Test(thing)


def create_matrix_python_old(num):
    source_size = 500, 500
    target_size = 224, 224
    for _ in range(num):
        make_transform(
            source_size, target_size,
            random.uniform(-45, 45),
            random.uniform(0.5, 2),
            random.uniform(0.9, 1.1),
            (random.uniform(-1, 1), random.uniform(-1, 1)),
            (random.uniform(-1, 1), random.uniform(-1, 1)),
            random.random() > 0.5,
            random.random() > 0.5,
        )


def create_matrix_native(num, m=np.empty((2, 3), dtype=np.float32)):
    source_size = 500
    target_size = 224
    for _ in range(num):
        m = m.copy()
        augpy.make_affine_matrix(
            m,
            source_size, source_size,
            target_size, target_size,
            random.uniform(-45, 45),
            random.uniform(0.5, 2),
            random.uniform(0.9, 1.1),
            random.uniform(-1, 1),
            random.uniform(-1, 1),
            random.uniform(-1, 1),
            random.uniform(-1, 1),
            random.random() > 0.5,
            random.random() > 0.5,
            augpy.WARP_SCALE_SHORTEST,
            3,
        )


def create_matrix_python_new(num, m=np.empty((2, 3), dtype=np.float32)):
    source_size = 500
    target_size = 224
    for _ in range(num):
        m = m.copy()
        make_affine_matrix(
            m,
            source_size, source_size,
            target_size, target_size,
            random.uniform(-45, 45),
            random.uniform(0.5, 2),
            random.uniform(0.9, 1.1),
            random.uniform(-1, 1),
            random.uniform(-1, 1),
            random.uniform(-1, 1),
            random.uniform(-1, 1),
            random.random() > 0.5,
            random.random() > 0.5,
        )


def create_matrix_native_wrapper(num):
    source_size = 500, 500
    target_size = 224, 224
    for _ in range(num):
        augpy.make_transform(
            source_size,
            target_size,
            random.uniform(-45, 45),
            random.uniform(0.5, 2),
            random.uniform(0.9, 1.1),
            (random.uniform(-1, 1), random.uniform(-1, 1)),
            (random.uniform(-1, 1), random.uniform(-1, 1)),
            random.random() > 0.5,
            random.random() > 0.5,
        )


def main():
    if True:
        n = 1000000
        bench(create_instance, num=n, m=n)
        bench(create_instance, num=n, m=n)
    if True:
        n = 100000
        bench(random_random, num=9*n, m=n)
        bench(random_random, num=9*n, m=n)
    if True:
        n = 100000
        bench(random_uniform, num=9*n, m=n)
        bench(random_uniform, num=9*n, m=n)
    if True:
        n = 100000
        bench(create_matrix_python_old, num=n, m=n)
        bench(create_matrix_python_old, num=n, m=n)
    if True:
        n = 100000
        bench(create_matrix_python_new, num=n, m=n)
        bench(create_matrix_python_new, num=n, m=n)
    if True:
        n = 100000
        bench(create_matrix_native, num=n, m=n)
        bench(create_matrix_native, num=n, m=n)
    if True:
        n = 100000
        bench(create_matrix_native_wrapper, num=n, m=n)
        bench(create_matrix_native_wrapper, num=n, m=n)


if __name__ == '__main__':
    main()
