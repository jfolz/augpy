import random

import numpy as np
import matplotlib.pyplot as plt
from PIL import Image

import augpy

from .benchtools import bench


np.set_printoptions(suppress=True, precision=4, floatmode='fixed')


KB = 1024
MB = KB*KB


size = 224
random.seed(ord('w'))


def imread(p):
    with open(p, 'rb') as f:
        return np.array(Image.open(p))


# image = imread("images/cats/cat1.jpg")
# image = imread("images/potato/anotherone256.jpg")
image = imread("images/neithernor/muenzfernsprecherspiegelfoto.jpg")
sh, sw = image.shape[:2]
dh, dw = size, size


stream = augpy.CudaStream()
stream.activate()
image_buffer = augpy.CudaTensor(image.shape)
print(image_buffer.shape)
result_buffer = augpy.CudaTensor((3, dh, dw))
background_buffer = augpy.CudaTensor((3,))
background = np.array([255, 63, 63], dtype=np.uint8)
background_tensor = augpy.array_to_tensor(background, background_buffer)
image_tensor = augpy.array_to_tensor(image, image_buffer)


def random_warp(number, show=False):
    for _ in range(number):
        m, supersampling = augpy.make_transform(
            (sh, sw),
            (dh, dw),
            random.uniform(-45, 45),
            random.uniform(0.3, 4/3),
            random.uniform(3/4, 4/3),
            (random.uniform(-1, 1), random.uniform(-1, 1)),
            (random.uniform(-0.2, 0.2), random.uniform(-0.2, 0.2)),
            random.random() > 0.5,
            0,  # random.random() > 0.5,
            augpy.WARP_SCALE_SHORTEST,
        )
        augpy.warp_affine(
            image_tensor,
            result_buffer,
            m,
            background_tensor,
            supersampling
        )
        if show:
            result = result_buffer.numpy()
            stream.synchronize()
            result = result.transpose([1, 2, 0])
            plt.imshow(result)
            plt.show()
    stream.synchronize()


def main():
    import sys
    n = int(sys.argv[1])
    show = len(sys.argv) > 2 and eval(sys.argv[2])
    nvtx_id = augpy.nvtx_range_start('profile')
    for i in range(3):
        bench(random_warp, mul=n, number=n, show=show)
    augpy.nvtx_range_end(nvtx_id)


if __name__ == '__main__':
    main()
