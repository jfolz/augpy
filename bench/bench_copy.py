import itertools as it

import numpy as np
import torch

import augpy
from .benchtools import bench


cutype = augpy.uint8
nptype = np.uint8
n, c, h, w = 256, 3, 224, 224
stream = augpy.CudaStream()
stream.activate()
a = augpy.CudaTensor((2*n, 2*c, h, w), dtype=cutype)
ind = np.arange(n*c*h*w, dtype=nptype).reshape((n, c, h, w))\
    .repeat(2, 0).repeat(2, 1)  # .repeat(2, 2)#.repeat(2, 3)
augpy.array_to_tensor(ind, a)
a = a[::2, ::2]
scalar = augpy.CudaTensor((1, 1, 1, 1), dtype=cutype)
augpy.array_to_tensor(np.full(1, 123, dtype=nptype), scalar)
b = augpy.CudaTensor((n, c, h, w), dtype=cutype)
print('tensor a:', a, 'strides', a.strides)
print('tensor b:', b, 'strides', b.strides)
b[:] = a
arr = augpy.tensor_to_array(b)
stream.synchronize()


ind_torch = torch.from_numpy(ind).to('cuda:0')
a_torch = ind_torch[::2, ::2]
a_numpy = a_torch.contiguous().cpu().numpy()
b_torch = a_torch.contiguous()
scalar_torch = torch.from_numpy(np.ones((1, 1, 1, 1), dtype=nptype)).to('cuda:0')


assert (arr == a_numpy).all()


def test():
    a = augpy.CudaTensor((2 * n, 2 * c, 2 * h, 2 * w), dtype=cutype)
    b = augpy.CudaTensor((n, c, h, w), dtype=cutype)
    print(a[::2, ::2, ::2])
    augpy.copy(b, b)
    augpy.copy(b[32:], b[32:])
    augpy.copy(b[::2], b[::2])


def cast(number, threads=0, blocks_per_sm=8):
    i = augpy.nvtx_range_start('cast' + ('_auto' if threads == 0 else ''))
    for _ in range(number):
        # print(sharp_tensor.strides)
        augpy.cast(b, augpy.float32, blocks_per_sm=blocks_per_sm, threads=threads)
    stream.synchronize()
    augpy.nvtx_range_end(i)


def contiguous(number, threads=0, blocks_per_sm=8):
    i = augpy.nvtx_range_start('contiguous' + ('_auto' if threads == 0 else ''))
    for _ in range(number):
        # print(sharp_tensor.strides)
        augpy.copy(a, b, blocks_per_sm=blocks_per_sm, threads=threads)
    stream.synchronize()
    augpy.nvtx_range_end(i)


def broadcast(number, threads=0, blocks_per_sm=8):
    i = augpy.nvtx_range_start('broadcast' + ('_auto' if threads == 0 else ''))
    for _ in range(number):
        # print(sharp_tensor.strides)
        augpy.copy(scalar, b, blocks_per_sm=blocks_per_sm, threads=threads)
    stream.synchronize()
    augpy.nvtx_range_end(i)


def cast_torch(number):
    i = augpy.nvtx_range_start('cast_torch')
    for _ in range(number):
        # print(sharp_tensor.strides)
        c_torch = b_torch.to(torch.float32)
    torch.cuda.synchronize()
    augpy.nvtx_range_end(i)


def contiguous_torch(number):
    i = augpy.nvtx_range_start('contiguous_torch')
    for _ in range(number):
        # print(sharp_tensor.strides)
        b_torch = a_torch.contiguous()
    torch.cuda.synchronize()
    augpy.nvtx_range_end(i)


def broadcast_torch(number):
    i = augpy.nvtx_range_start('broadcast_torch')
    for _ in range(number):
        # print(sharp_tensor.strides)
        b_torch[:] = scalar_torch
    torch.cuda.synchronize()
    augpy.nvtx_range_end(i)


def base2range(*args):
    return (2**p for p in range(*args))


def main():
    import sys
    number = int(sys.argv[1])
    if len(sys.argv) > 2:
        test()
    if True:
        nvtx_id = augpy.nvtx_range_start('profile')
        funcs = [cast, contiguous, broadcast]
        funcs_torch = [cast_torch, contiguous_torch, broadcast_torch]
        threads = tuple(2 ** k for k in range(7, 11))
        blocks = (1,) + tuple(range(8, 25, 8))
        for fun, fun_torch in zip(funcs, funcs_torch):
            for t, b in it.product(threads, blocks):
                bench(fun, mul=number*n, number=number, blocks_per_sm=b, threads=t)
            bench(fun, mul=number*n, number=number)
            bench(fun_torch, mul=number*n, number=number)
        augpy.nvtx_range_end(nvtx_id)


if __name__ == '__main__':
    main()
