import torch
import augpy

from .benchtools import exp
from .benchtools import bench


N = 256


def mbused():
    return augpy.meminfo()[0] / 1024 / 1024


print('memory after import:', mbused(), 'MB')
stream = augpy.default_stream
stream.activate()
print('memory after stream:', mbused(), 'MB')
_ = augpy.CudaTensor((1,), dtype=augpy.uint8)
print('memory before init:', mbused(), 'MB')
gen = augpy.RandomNumberGenerator()
stream.synchronize()
print('memory after init:', mbused(), 'MB')
shape = N, 3, 224, 224
f = augpy.CudaTensor(shape, dtype=augpy.float32)
i = augpy.CudaTensor(shape, dtype=augpy.int8)
b = augpy.CudaTensor(shape, dtype=augpy.uint8)
tf = torch.zeros(shape, dtype=torch.float32, device='cuda:0')
ti = torch.zeros(shape, dtype=torch.int8, device='cuda:0')


def uniform_byte(batches, blocks_per_sm=8, threads=0, **__):
    for _ in range(batches):
        gen.uniform(i, -128, 128, blocks_per_sm, threads)
    stream.synchronize()


def uniform_byte_torch(batches, **__):
    size = i.shape
    d = torch.device('cuda:0')
    for _ in range(batches):
        r = torch.randint(-128, 127, size, dtype=torch.int8, device=d)
    torch.cuda.synchronize(d)


def gaussian_byte(batches, blocks_per_sm=8, threads=0, **__):
    for _ in range(batches):
        gen.gaussian(i, 10, 21, blocks_per_sm, threads)
    stream.synchronize()


def gaussian_byte_torch(batches, **__):
    for _ in range(batches):
        r = tf.normal_(10, 21).to(ti.dtype)
    torch.cuda.synchronize(ti.device)


def uniform_float(batches, blocks_per_sm=8, threads=0, **__):
    for _ in range(batches):
        gen.uniform(f, -128, 128, blocks_per_sm, threads)
    stream.synchronize()


def uniform_float_torch(batches, **__):
    size = i.shape
    d = torch.device('cuda:0')
    for _ in range(batches):
        torch.randint(-128, 127, size, dtype=torch.float32, device=d)
    torch.cuda.synchronize(d)


def gaussian_float(batches, blocks_per_sm=8, threads=0, **__):
    for _ in range(batches):
        gen.gaussian(f, 10, 21, blocks_per_sm, threads)
    stream.synchronize()


def gaussian_float_torch(batches, **__):
    for _ in range(batches):
        tf.normal_(10, 21)
    torch.cuda.synchronize(tf.device)


funcs = (gaussian_byte, uniform_byte, gaussian_float, uniform_float)
torch_funcs = (gaussian_byte_torch, uniform_byte_torch,
               gaussian_float_torch, uniform_float_torch)


@exp.automain
def main(
        _config,
        batches,
        repeat,
        ignore,
        benchmark,
        profile,
):
    mul = N * batches
    if benchmark:
        for f, tf in zip(funcs, torch_funcs):
            exp.run('benchmark_and_plot', config_updates=dict(
                path_prefix='random',
                func=f,
                torch_func=tf,
                varied_value_name='threads',
                values=tuple(2 ** k for k in range(5, 11)),
                ylabel='images/s (224x224x3)',
                kwargs=dict(mul=mul, batches=batches, repeat=repeat, ignore=ignore)
            ))
        exp.run('finalize')
    if profile:
        nvtx_id = augpy.nvtx_range_start('profile')
        for fun, fun_torch in zip(funcs, torch_funcs):
            bench(fun, repeat, mul, ignore, batches=batches)
            bench(fun_torch, repeat, mul, ignore, batches=batches)
        augpy.nvtx_range_end(nvtx_id)
    print('memory after generate:', mbused(), 'MB')
    augpy.release()
    print('memory after release:', mbused(), 'MB')
