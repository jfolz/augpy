import random
import weakref

import numpy as np
from datadings.reader import MsgpackReader
from datadings.reader import Cycler
# from simplejpeg import decode_jpeg_header
from crumpets.presets import AUGMENTATION_TRAIN
from crumpets.presets import NO_AUGMENTATION
from crumpets.torch.dataloader import TorchTurboDataLoader
from crumpets.broker import BufferWorker
import tqdm

from torch.utils import dlpack

import augpy
from augpy import export_dltensor
from augpy.image import DecodeWarp
from augpy.image import Lighting

from .benchtools import bench
from .benchtools import make_parser


N = 256
shape = 3, 224, 224

decode_warp = lambda x: x
lighting = Lighting(N, shape[0])

stream = augpy.default_stream

reader = Cycler(MsgpackReader('/netscratch/folz/ILSVRC12_422_resize/val.msgpack'))
random.seed(1234)


class RandomizeWorker(BufferWorker):
    def __init__(self, label=((1,), np.int64), image_params=None, image_rng=None):
        BufferWorker.__init__(self)
        self.add_params('image', image_params, {})
        self.add_buffer('label', label)
        self.image_rng = image_rng or NO_AUGMENTATION

    def prepare(self, sample, batch, buffers):
        buffers['label'] = sample['label']
        batch['image'].append(sample['image'])
        params = self.image_rng(None, None)
        params.update(self.params['image'])
        batch['augmentation'].append(AUGMENTATION_TRAIN(None, None))


loader = TorchTurboDataLoader(reader.rawiter(), N, RandomizeWorker(), 1,
                              shared_memory=False, gpu_augmentation=False)


def batch_decode_warp():
    for _, batch in tqdm.tqdm(loader):
        for mini_batch in batch:
            mini_batch = decode_warp(mini_batch)
            mini_batch = lighting(mini_batch)
            stream.synchronize()
            augpy_tensor = mini_batch['image']
            capsule = export_dltensor(augpy_tensor, 'dltensor')
            torch_tensor = dlpack.from_dlpack(capsule)
            mini_batch['image'] = torch_tensor
            weakref.finalize(torch_tensor, decode_warp.finalize_batch, augpy_tensor)
            del torch_tensor


def main():
    global decode_warp
    parser = make_parser()
    parser.add_argument('-t', '--threads', type=int, default=1)
    args = parser.parse_args()
    loader.epoch_iterations = args.batches
    mul = args.batches * N
    decode_warp = DecodeWarp(N, shape, (50, 100, 150), cpu_threads=args.threads)
    with loader:
        bench(batch_decode_warp, repeat=3, mul=mul)


if __name__ == '__main__':
    main()
