from math import floor
from math import sqrt
from math import pi
import random

import numpy as np
from PIL import Image
import matplotlib.pyplot as plt
import torch
import torch.utils.dlpack
import crumpets.torch.augmentation_cuda as cudaaugs

import augpy
from augpy import Decoder

from .benchtools import bench


KB = 1024
MB = KB*KB


size = 224
smin = 0
smax = 2
kmin = 1
kmax = floor(smax * 3*sqrt(2*pi)/4 + 0.5) + 1
random.seed(ord('w'))
np.random.seed(ord('w'))


path = 'images/potato/anotherone256.jpg'
with open(path, 'rb') as f:
    data = f.read()
stream = augpy.CudaStream()
stream.activate()
decoder = Decoder()
image_buffer = augpy.CudaTensor((16 * MB,))
image = decoder.decode(data, image_buffer)


background_buffer = augpy.CudaTensor((3,))
background = np.array([255, 63, 63], dtype=np.uint8)
background_tensor = augpy.array_to_tensor(background, background_buffer)

n = 256
h, w, c = size, size, 3
blurry_tensor = augpy.CudaTensor((n, c, h, w))
sharp_tensor = augpy.CudaTensor((n, c, h, w))
stream.synchronize()

sigmasbuffer = augpy.CudaTensor((n,), dtype=augpy.float32)
m = np.eye(3, dtype="float32")
m[(0, 1), 2] = 150, 50
for i in range(n):
    augpy.warp_affine(image, sharp_tensor[i], m, background_tensor, 1)

image_torch = torch.utils.dlpack.from_dlpack(augpy.export_dltensor(sharp_tensor, "dltensor"))


blurry_tensor = augpy.cast(blurry_tensor, augpy.float32)


def test():
    sigma = 1
    ksize = int(sigma * 6.6 - 2.3) | 1
    print(f'sigma={sigma}, ksize={ksize}')
    augpy.gaussian_blur_single(sharp_tensor[-1], sigma, blurry_tensor[-1])
    # augpy.box_blur_single(sharp_tensor[-1], ksize, blurry_tensor[-1])

    stream.synchronize()

    np_image = augpy.tensor_to_array(blurry_tensor)
    np_image = np_image[-1].transpose(1, 2, 0)
    plt.imshow(np_image)
    plt.show()
    pil_image = Image.fromarray(np_image, 'RGB')
    with open('blurred.png', 'wb') as f:
        pil_image.save(f)


def box_blur_single(number):
    sharp = sharp_tensor[0]
    for _ in range(number):
        ksize = random.randrange(kmin, kmax)
        blurry = augpy.box_blur_single(sharp, ksize)
    stream.synchronize()


def gaussian_blur(number):
    for _ in range(number):
        sigma = np.random.uniform(smin, smax, n).astype(np.float32)
        ksize = max(3, int(max(sigma * 6.6 - 2.3)) | 1)
        sigma = augpy.array_to_tensor(sigma)
        blurry = augpy.gaussian_blur(sharp_tensor, sigma, ksize)
    stream.synchronize()


def gaussian_blur_single(number):
    sharp = sharp_tensor[0]
    for _ in range(number):
        sigma = random.uniform(smin, smax)
        blurry = augpy.gaussian_blur_single(sharp, sigma)
    stream.synchronize()


augs = n*[{"blur": 0}]


def random_blur_torch(number):
    for _ in range(number):
        for i in range(n):
            augs[i]["blur"] = random.uniform(smin, smax)/w
        cudaaugs.add_blur(image_torch, augs)
    torch.cuda.synchronize()


def main():
    import sys
    if len(sys.argv) > 2 and eval(sys.argv[2]):
        test()
    number = int(sys.argv[1])
    batches = max(1, number // n)
    if True:
        nvtx_id = augpy.nvtx_range_start('profile')
        bench(box_blur_single, mul=number, number=number)
        bench(gaussian_blur, mul=batches*n, number=batches)
        bench(gaussian_blur_single, mul=number, number=number)
        bench(random_blur_torch, mul=batches*n, number=batches)
        augpy.nvtx_range_end(nvtx_id)


if __name__ == '__main__':
    main()
