from math import pi
from math import sin
from math import cos
from math import ceil

import numpy as np


MAX_SUPERSAMPLING = 3


def calc_scale_ratio(source_size, target_size, scale, scale_mode):
    sh, sw = source_size
    sh /= 2
    sw /= 2
    th, tw = target_size
    th /= 2
    tw /= 2
    if scale_mode == 'longest':
        r = max(sh / th, sw / tw) / scale
    elif scale_mode == 'shortest':
        r = min(sh / th, sw / tw) / scale
    else:
        raise ValueError('unknown scale mode %r' % scale_mode)
    return r, sh, sw, th, tw


def make_transform(
        source_size,
        target_size,
        angle=0,
        scale=1,
        aspect=1,
        shift=None,
        shear=None,
        hmirror=False,
        vmirror=False,
        scale_mode='shortest',
        __identity__=np.eye(3, dtype=np.float32),
        __deg2rad__=pi/180
):
    r, sh, sw, th, tw = calc_scale_ratio(source_size, target_size,
                                         scale, scale_mode)
    # first shift target to origin
    p = __identity__.copy()
    p[(0, 1), 2] = -tw, -th
    # shear
    if shear:
        ps = __identity__.copy()
        ps[(1, 0), (0, 1)] = shear
        p = np.dot(ps, p)
    # resize and mirror
    p1 = __identity__.copy()
    p1[0, 0] = r*aspect * (-1 if hmirror else 1)
    p1[1, 1] = r/aspect * (-1 if vmirror else 1)
    p = np.dot(p1, p)
    # then rotate
    if angle:
        p2 = __identity__.copy()
        rad = -angle * __deg2rad__
        cos_rad = cos(rad)
        p2[0, 0] = cos_rad
        p2[1, 1] = cos_rad
        sin_rad = sin(rad)
        p2[0, 1] = -sin_rad
        p2[1, 0] = sin_rad
        p = np.dot(p2, p)
    # finally shift to desired position in source image
    wdelta = max(sw - tw*r, tw*r - sw)
    hdelta = max(sh - th*r, th*r - sh)
    if wdelta or hdelta:
        shift = shift or (0, 0)
        p3 = __identity__.copy()
        p3[(0, 1), 2] = sw + wdelta * shift[1], sh + hdelta * shift[0]
        # chain transforms
        p = np.dot(p3, p)
    supersampling = max(1, min(3, ceil(r)))
    return p[:2].astype(np.float32), supersampling


def make_affine_matrix(
        out,
        source_width,
        source_height,
        target_width,
        target_height,
        angle,
        scale,
        aspect,
        shiftx,
        shifty,
        shearx,
        sheary,
        hmirror,
        vmirror,
        scale_mode=0,
        max_supersampling=3,
):
    if out.ndim != 2 or out.shape[0] < 2 or out.shape[1]!=3:
        raise ValueError("need 2x3 matrix")
    if out.dtype != np.float32:
        raise ValueError("matrix must be float32")

    sw = source_width / 2
    sh = source_height / 2
    tw = target_width / 2
    th = target_height / 2

    if scale_mode == 0:
        r = max(sh / th, sw / tw) / scale
    elif scale_mode == 1:
        r = min(sh / th, sw / tw) / scale
    else:
        raise ValueError("unknown scale mode")

    wdelta = max(sw - tw*r, tw*r - sw)
    hdelta = max(sh - th*r, th*r - sh)

    rad = -angle * pi / 180
    cos_rad = cos(rad)
    sin_rad = sin(rad)

    p3 = r * aspect * (-1 if hmirror else 1)
    p4 = r / aspect * (-1 if vmirror else 1)
    p7 = p3 * (-tw - th * shearx)
    p8 = p4 * (-tw * sheary - th)
    p5 = shearx * p3
    p6 = sheary * p4

    out[0, 0] = cos_rad * p3 - sin_rad * p6
    out[0, 1] = cos_rad * p5 - sin_rad * p4
    out[0, 2] = cos_rad * p7 - sin_rad * p8 \
        + sw + wdelta * shiftx

    out[1, 0] = sin_rad * p3 + cos_rad * p6
    out[1, 1] = sin_rad * p5 + cos_rad * p4
    out[1, 2] = sin_rad * p7 + cos_rad * p8 \
        + sh + hdelta * shifty

    supersampling = max(1, min(max_supersampling, ceil(r)))
    return supersampling
