import random

import numpy as np
from datadings.reader import MsgpackReader

import augpy
from augpy import make_transform
from augpy import Decoder
from augpy import array_to_tensor
from augpy import warp_affine

from .benchtools import bench


KB = 1024
MB = KB*KB


reader = MsgpackReader('/netscratch/folz/ILSVRC12_422_resize/val.msgpack')
with reader:
    data = list(reader)


def make_transform_matrix(source_size, target_size):
    return make_transform(
        source_size, target_size,
        random.uniform(-45, 45),
        random.uniform(0.5, 2),
        random.uniform(0.9, 1.1),
        (random.uniform(-1, 1), random.uniform(-1, 1)),
        (random.uniform(-1, 1), random.uniform(-1, 1)),
        random.random() > 0.5,
        random.random() > 0.5,
    )


def decode_single(host_padding=16*KB,
                  device_padding=16*KB,
                  gpu_huffman=False):
    decoder = Decoder(host_padding=int(host_padding),
                      device_padding=int(device_padding),
                      gpu_huffman=gpu_huffman)
    background_buffer = augpy.CudaTensor((3,))
    background = np.array([255, 63, 63], dtype=np.uint8)
    background_tensor = array_to_tensor(background, background_buffer)
    image_buffer = augpy.CudaTensor((1*MB,))
    target_buffer = augpy.CudaTensor((3, 224, 224))
    for d in data:
        image = decoder.decode(d['image'], image_buffer)
        m, supersampling = make_transform_matrix(image.shape[:2], target_buffer.shape[:2])
        warp_affine(
            image,
            target_buffer,
            m,
            background_tensor,
            supersampling
        )


def main():
    if True:
        print('single')
        bench(decode_single, m=len(data))
        bench(decode_single, m=len(data))


if __name__ == '__main__':
    main()
