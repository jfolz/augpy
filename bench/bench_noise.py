import random
import itertools as it
from collections import defaultdict

import numpy as np
import matplotlib.pyplot as plt
from PIL import Image

import augpy

from .benchtools import bench


KB = 1024
MB = KB*KB


size = 224
smin = 0
smax = 2
random.seed(ord('w'))
np.random.seed(ord('w'))


path = 'images/potato/anotherone256.jpg'
with open(path, 'rb') as f:
    data = f.read()
stream = augpy.CudaStream()
stream.activate()
decoder = augpy.Decoder()
image_buffer = augpy.CudaTensor((16 * MB,))
image = decoder.decode(data, image_buffer)


background_buffer = augpy.CudaTensor((3,))
background = np.array([255, 63, 63], dtype=np.uint8)
background_tensor = augpy.array_to_tensor(background, background_buffer)

n = 256
h, w, c = size, size, 3
gen = augpy.RandomNumberGenerator()
image_tensor = augpy.CudaTensor((n, c, h, w), dtype=augpy.uint8)
noisy_tensor = augpy.CudaTensor((n, c, h, w), dtype=augpy.uint8)
noise_tensor = augpy.CudaTensor((n, c, h, w), dtype=augpy.int8)
gen.uniform(noise_tensor, -127, 127)
m = np.eye(3, dtype="float32")
m[(0, 1), 2] = 150, 50
for i in range(n):
    augpy.warp_affine(image, image_tensor[i], m, background_tensor, 1)
stream.synchronize()


def mbused():
    return augpy.meminfo()[0] / 1024 / 1024


def random_noise(number):
    for _ in range(number):
        s = [random.random() for _ in range(n)]
        augpy.fma_sat_batched(s, noise_tensor, image_tensor, noisy_tensor)
    stream.synchronize()


def random_noise_single(number):
    for _ in range(number):
        for i in range(n):
            augpy.fma_sat(random.random(), noise_tensor[i], image_tensor[i], noisy_tensor[i])
    stream.synchronize()


def test():
    strength = 0.5
    augpy.fma_sat_batched([strength]*n, noise_tensor, image_tensor, noisy_tensor)
    np_image = augpy.tensor_to_array(noisy_tensor)
    np_image = np_image[n//2].transpose(1, 2, 0)
    # noinspection PyUnreachableCode
    plt.imshow(np_image)
    plt.show()
    pil_image = Image.fromarray(np_image, 'RGB')
    with open('noisy.png', 'wb') as f:
        pil_image.save(f)


def main():
    import sys
    if len(sys.argv) > 2 and eval(sys.argv[2]):
        test()
    number = int(sys.argv[1])
    if True:
        nvtx_id = augpy.nvtx_range_start('profile')
        batches = max(3, number // n)
        for i in range(3):
            bench(random_noise, m=batches*n, number=batches)
            bench(random_noise_single, m=batches*n, number=batches)
        augpy.nvtx_range_end(nvtx_id)


if __name__ == '__main__':
    main()
