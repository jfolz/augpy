import matplotlib.pyplot as plt

import augpy as ap


PATH = 'images/neithernor/muenzfernsprecherspiegelfoto.jpg'
SIZE = 224


def show(tensor):
    array = tensor.numpy()
    ap.default_stream.synchronize()
    plt.imshow(array)
    plt.show()


def show_transposed(tensor):
    array = tensor.numpy()
    ap.default_stream.synchronize()
    plt.imshow(array.transpose((1, 2, 0)))
    plt.show()


def demo():
    # get image data
    with open(PATH, 'rb') as f:
        imdata = f.read()

    # decode image
    dec = ap.Decoder()
    im_tensor = dec.decode(imdata)
    print(im_tensor)
    show(im_tensor)

    # affine warp
    im_warped = ap.CudaTensor((im_tensor.shape[2], SIZE, SIZE))
    background = ap.CudaTensor((3,))
    background.fill(128)
    m, supersampling = ap.make_transform(
        im_tensor.shape[:2],
        im_warped.shape[1:],
        angle=45
    )
    ap.warp_affine(im_tensor, im_warped, m, background, supersampling)
    show_transposed(im_warped)

    # setting values
    im_warped[:, 75:100, 150:175] = 0
    show_transposed(im_warped)

    # gaussian blur
    im_blurred = ap.gaussian_blur_single(im_warped, 2)
    show_transposed(im_blurred)

    # add noise
    gen = ap.RandomNumberGenerator()
    noise = ap.CudaTensor(im_blurred.shape, dtype=ap.int16)
    gen.uniform(noise, -128, 128)
    im_casted = ap.cast(im_blurred, ap.int16)
    # im_noisy = ap.fma(0.5, noise, im_casted)
    im_noisy = 0.5 * noise + im_casted
    im_noisy = ap.cast(im_noisy, ap.uint8)
    show_transposed(im_noisy)


if __name__ == '__main__':
    demo()
