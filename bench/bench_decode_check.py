import os.path as pt
from datadings.sets.ILSVRC2012_write import verify_image


def main():
    with open('/ds/images/imagenet/train.txt') as f, open('decode_errors.txt', 'w') as errors:
        for i, line in enumerate(f):
            path = pt.join('/ds/images/imagenet/train', line.split(' ')[0])
            print(i, path)
            try:
                with open(path, 'rb') as g:
                    verify_image(g.read(), quality=85)
            except Exception as e:
                errors.write(f'{i} {path} {str(e)}\n')
                errors.flush()


if __name__ == '__main__':
    main()
