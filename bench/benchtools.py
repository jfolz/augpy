import os.path as pt
import time
from collections import defaultdict
import json
from string import capwords

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import plottools as ptt
from sacred import Experiment
from sacred.observers import MongoObserver
from neptunecontrib.monitoring.sacred import NeptuneObserver

import augpy as ap


with open(pt.expanduser(pt.join('~', '.config', 'sacred.conf'))) as f:
    CONF = json.load(f)
MONGO_ADDR = CONF['mongo_addr']
MONGO_USER = CONF['mongo_user']
MONGO_PASS = CONF['mongo_pass']
exp = Experiment()
exp.observers.append(MongoObserver(
    url=f'mongodb://{MONGO_USER}:{MONGO_PASS}@{MONGO_ADDR}/augpy_bench',
    db_name='augpy_bench',
))
exp.observers.append(NeptuneObserver(
    project_name='jfolz/augpy-bench',
))


params = {'legend.fontsize': 10,
          'axes.labelsize': 10,
          'axes.titlesize': 10,
          'xtick.labelsize': 8,
          'ytick.labelsize': 8}
plt.rcParams.update(params)


def bench(fun, repeat=11, mul=1, ignore=5, unit='it', *args, **kwargs):
    ignore = max(0, min((repeat-1)//2, ignore))
    print(fun.__name__, kwargs)
    times = []
    for _ in range(repeat):
        a = time.time()
        fun(*args, **kwargs)
        times.append(time.time() - a)
    times.sort()
    if ignore:
        times = times[ignore:-ignore]
    its = ((repeat-2*ignore) * mul) / sum(times)
    print('%12.2f %s/s' % (its, unit))
    return its


@exp.config
def make_parser():
    batches = 1
    repeat = 11
    ignore = 5
    benchmark = True
    profile = False
    show = False


@exp.command
def finalize():
    pass


# noinspection PyPep8Naming
@exp.command
def benchmark(
        path_prefix,
        func,
        torch_func,
        kwargs,
        device=0,
        ylabel='images/s (224x224x3)',
        GPU=None,
):
    props = ap.get_device_properties(device)
    gpuname = props.name.replace(' ', '-') or GPU
    results = dict(
        path_prefix=path_prefix,
        gpu=gpuname,
        func=func.__name__,
    )
    its_auto = bench(func, **kwargs)
    results['auto'] = its_auto
    exp.log_scalar('auto', its_auto)
    if torch_func is not None:
        its_torch = bench(torch_func, **kwargs)
        exp.log_scalar('torch', its_torch)
        results['torch'] = its_torch
    with open(f'{path_prefix}_{gpuname}_{func.__name__}.json', 'w') as f:
        json.dump(results, f)


# noinspection PyPep8Naming
@exp.command
def benchmark_and_plot(
        path_prefix,
        func,
        torch_func,
        varied_value_name,
        values,
        kwargs,
        device=0,
        ylabel='images/s (224x224x3)',
        GPU=None,
):
    values = sorted(values)
    props = ap.get_device_properties(device)
    gpuname = props.name.replace(' ', '-') or GPU
    name = capwords(func.__name__.replace('_', ' '))
    results = dict(
        path_prefix=path_prefix,
        gpu=gpuname,
        func=func.__name__,
        varied_value_name=varied_value_name,
    )
    results[varied_value_name] = {}
    for v in values:
        kw = dict(kwargs)
        kw[varied_value_name] = v
        its = results[varied_value_name][v] = bench(func, **kw)
        exp.log_scalar(varied_value_name, its, v)
    its_auto = bench(func, **kwargs)
    its_torch = bench(torch_func, **kwargs)
    results['auto'] = its_auto
    results['torch'] = its_torch
    for t in values:
        exp.log_scalar('auto', its_auto, t)
        exp.log_scalar('torch', its_torch, t)
    plt.plot(values, [results[varied_value_name][v] for v in values],
             label=varied_value_name, alpha=0.5)
    plt.xticks(values)
    ptt.setup_axes(xtickcount=len(values))
    plt.hlines(its_auto, min(values), max(values), linestyles='solid', label='auto', alpha=0.5)
    plt.hlines(its_torch, min(values), max(values), linestyles=':', label='torch', alpha=0.5)
    plt.title(f'{name} ({props.name}, '
              f'CC {props.major}.{props.minor}, '
              f'{props.multiProcessorCount} SMs)')
    plt.xlabel('Blocks per SM')
    plt.ylabel(ylabel)
    plt.legend()
    plt.grid()
    plt.tight_layout()
    plt.savefig(f'{path_prefix}_{gpuname}_{func.__name__}.pdf')
    plt.close()
    with open(f'{path_prefix}_{gpuname}_{func.__name__}.json', 'w') as f:
        json.dump(results, f)
