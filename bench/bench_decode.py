import sys
import time
import itertools as it
from queue import Queue
from threading import Thread

import matplotlib.pyplot as plt
import simplejpeg
from datadings.reader import MsgpackReader

import augpy
from augpy import Decoder
from augpy import tensor_to_array

from .benchtools import bench


KB = 1024
MB = KB*KB


# paths = [
#     'images/potato/anotherone256.jpg',
#     'images/potato/potato-04-1080x675.jpg',
#     'images/potato/MOREPOTATOS.jpg',
#     'images/neithernor/muenzfernsprecherspiegelfoto.jpg',
# ]
# data = []
# for p in paths:
#     with open(p, "rb") as f:
#         data.append(f.read())
# data *= 2
path = sys.argv[1] if len(sys.argv) > 1 else '/netscratch/folz/ILSVRC12_opt/val.msgpack'
reader = MsgpackReader(path)
with reader:
    alldata = list(reader)
# data = [alldata[3]] * 1000
data = alldata


def decode_tj():
    for d in data:
        simplejpeg.decode_jpeg(d['image'], fastdct=True, fastupsample=True)


def decode_single(host_padding=16*KB,
                  device_padding=16*KB,
                  gpu_huffman=False):
    decoder = Decoder(host_padding=int(host_padding),
                      device_padding=int(device_padding),
                      gpu_huffman=gpu_huffman)
    tensor = augpy.CudaTensor((1*MB,))
    for d in data:
        image = decoder.decode(d['image'], tensor)


def show(decoder, i):
    tensor = decoder.decode_single(data[i])
    image = tensor_to_array(tensor)
    print(image.shape)
    plt.imshow(image)
    plt.show()


def grid_search(fun, runs=5, n=1, m=len(data), **kwargs):
    keys = list(kwargs.keys())
    values = list(kwargs.values())
    for combination in it.product(*values):
        new_kwargs = dict(zip(keys, combination))
        print(new_kwargs)
        for _ in range(runs):
            bench(fun, n=n, m=m, **new_kwargs)


if False:
    padding = [2**i * KB for i in range(4, 15)]
    print('grid search single')
    grid_search(decode_single,
                host_padding=padding,
                device_padding=padding)
if False:
    print('tj')
    # for i in range(len(alldata)):
    #     print(i, end=' ')
    #     data = [alldata[i]] * 500
    #     print(bench(decode_tj, m=len(data)))
    bench(decode_tj, m=len(data))
    bench(decode_tj, m=len(data))
if False:
    nthreads = 3
    print(f'single, {nthreads} threads')
    #decoder = Decoder()
    #show(decoder, 0)
    #show(decoder, 1)
    #show(decoder, 2)
    #bench(decode_single, m=len(data))
    #bench(decode_single, m=len(data))
    #bench(decode_single, m=len(data))
    # bench(decode_single, m=len(data))
    threads = [Thread(target=bench, args=(decode_single,), kwargs=dict(m=len(data)))
               for _ in range(nthreads)]
    for t in threads:
        t.start()
    for t in threads:
        t.join()
    # for i in range(len(alldata)):
    #     print(i, end=' ')
    #     data = [alldata[i]] * 500
    #     print(bench(decode_single, m=len(data)))
    # print(bench(decode_single, m=len(data)))
if True:
    print('single')
    #decoder = Decoder()
    #show(decoder, 0)
    #bench(decode_single, mul=len(data))
    bench(decode_single, mul=len(data))
