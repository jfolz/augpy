import numpy as np

import augpy


KB = 1024
MB = KB * KB


stream = augpy.CudaStream()
stream.activate()


def memused():
    return augpy.meminfo()[0] / MB


def gemm(t1, t2, result, alpha=1, beta=0):
    augpy.gemm(t1, t2, result, alpha, beta)
    result_a = result.numpy()
    stream.synchronize()
    return result_a


def test():
    v1 = np.arange(1, 3, dtype=np.float32).reshape((-1, 1))
    v2 = np.arange(1, 9, dtype=np.float32).reshape((1, -1))
    r, c = v1.shape[0], v2.shape[1]
    t1 = augpy.array_to_tensor(v1)
    t2 = augpy.array_to_tensor(v2)
    result = np.arange(r*c, dtype=np.float32).reshape((r, c))
    result = augpy.array_to_tensor(result)
    result_a = gemm(t1, t2, result)
    print(v1)
    print('*')
    print(v2)
    print('=')
    print(result_a)
    assert (result_a == np.dot(v1, v2)).all()


if __name__ == '__main__':
    print('memory: %.2fMB' % memused())
    test()
    print('memory: %.2fMB' % memused())
