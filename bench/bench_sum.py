from functools import reduce

import numpy as np
import torch

import augpy

from .benchtools import bench
from .benchtools import make_parser


KB = 1024
MB = KB * KB


stream = augpy.CudaStream()
stream.activate()


size = 256, 3, 224, 224
N = reduce(lambda a,b: a*b, size, 1)
torch_device = torch.device('cuda:0')

np_b = np.arange(N, dtype=np.uint8).reshape(size)
np_i = np.arange(N, dtype=np.int32).reshape(size)
np_f = np.arange(N, dtype=np.float32).reshape(size)
ap_b = augpy.array_to_tensor(np_b)
ap_i = augpy.array_to_tensor(np_i)
ap_f = augpy.array_to_tensor(np_f)
to_b = torch.from_numpy(np_b).to(torch_device)
to_i = torch.from_numpy(np_i).to(torch_device)
to_f = torch.from_numpy(np_f).to(torch_device)


def mul(fun, batches):
    m = N * batches / MB
    if 'short' in fun.__name__ or 'half' in fun.__name__:
        m *= 2
    elif 'int' in fun.__name__ or 'float' in fun.__name__:
        m *= 4
    elif 'long' in fun.__name__ or 'double' in fun.__name__:
        m *= 8
    return m


def memused():
    return augpy.meminfo()[0] / MB


def sum_byte(batches, **__):
    for _ in range(batches):
        ap_b.sum()
    stream.synchronize()


def sum_byte_torch(batches, **__):
    with torch.no_grad():
        for _ in range(batches):
            to_b.sum()
        torch.cuda.synchronize(torch_device)


def sum_byte_axis(batches, **__):
    for _ in range(batches):
        ap_b.sum(axis=2)
    stream.synchronize()


def sum_byte_axis_torch(batches, **__):
    with torch.no_grad():
        for _ in range(batches):
            to_b.sum(axis=2)
        torch.cuda.synchronize(torch_device)


def sum_int(batches, **__):
    for _ in range(batches):
        ap_i.sum()
    stream.synchronize()


def sum_int_axis(batches, **__):
    for _ in range(batches):
        ap_i.sum(axis=2)
    stream.synchronize()


def sum_int_torch(batches, **__):
    with torch.no_grad():
        for _ in range(batches):
            to_i.sum()
        torch.cuda.synchronize(torch_device)


def sum_int_axis_torch(batches, **__):
    with torch.no_grad():
        for _ in range(batches):
            to_i.sum(axis=2)
        torch.cuda.synchronize(torch_device)


def sum_float(batches, **__):
    for _ in range(batches):
        ap_f.sum()
    stream.synchronize()


def sum_float_axis(batches, **__):
    for _ in range(batches):
        ap_f.sum(axis=2)
    stream.synchronize()


def sum_float_torch(batches, **__):
    with torch.no_grad():
        for _ in range(batches):
            to_f.sum()
        torch.cuda.synchronize(torch_device)


def sum_float_axis_torch(batches, **__):
    with torch.no_grad():
        for _ in range(batches):
            to_f.sum(axis=2)
        torch.cuda.synchronize(torch_device)


funcs = (
    sum_byte, sum_byte_axis,
    sum_int, sum_int_axis,
    sum_float, sum_float_axis,
)
torch_funcs = (
    sum_byte_torch, sum_byte_axis_torch,
    sum_int_torch, sum_int_axis_torch,
    sum_float_torch, sum_float_axis_torch,
)


def main():
    parser = make_parser()
    args = parser.parse_args()
    print('memory after init:', memused(), 'MB')
    if True:
        nvtx_id = augpy.nvtx_range_start('profile')
        for fun, fun_torch in zip(funcs, torch_funcs):
            bench(fun, args.repeat,
                  mul(fun, args.batches), args.ignore,
                  batches=args.batches, unit='MB')
            bench(fun_torch, args.repeat,
                  mul(fun_torch, args.batches), args.ignore,
                  batches=args.batches, unit='MB')
        augpy.nvtx_range_end(nvtx_id)
    print('memory after bench:', memused(), 'MB')
    augpy.release()
    print('memory after release:', memused(), 'MB')


if __name__ == '__main__':
    main()
