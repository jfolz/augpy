import numpy as np
import matplotlib.pyplot as plt

import augpy

from .benchtools import exp


KB = 1024
MB = KB * KB
size = 224
n = 256

path = 'images/potato/anotherone256.jpg'
with open(path, 'rb') as f:
    data = f.read()
stream = augpy.CudaStream()
stream.activate()
decoder = augpy.Decoder()


background_buffer = augpy.CudaTensor((3,))
background = np.array([255, 63, 63], dtype=np.uint8)
background_tensor = augpy.array_to_tensor(background, background_buffer)

i = 0
image = decoder.decode(data)
stream.synchronize()
h, w, c = image.shape
buffer = augpy.CudaTensor((n, c, h, w))
buffer2 = augpy.CudaTensor((n, c, size, size))
buffer2_float = augpy.CudaTensor(buffer2.shape, dtype=augpy.float32)

graysbuffer = augpy.CudaTensor((n,), dtype=augpy.float32)
colorsbuffer = augpy.CudaTensor((n * 3,), dtype=augpy.float32)
contrastsbuffer = augpy.CudaTensor((n,), dtype=augpy.float32)

gammagrays = augpy.array_to_tensor(np.asarray(n * [1.0], dtype="float32"), graysbuffer)
gammacolors = augpy.array_to_tensor(np.asarray(n * [1, 1, 1], dtype="float32"), colorsbuffer)
contrasts = augpy.array_to_tensor(np.asarray(n * [-1], dtype="float32"), contrastsbuffer)

m = np.eye(3, dtype="float32")
m[(0, 1), 2] = 150, 50
augpy.warp_affine(image, buffer2[0], m, background_tensor, 1)


def test():
    augpy.lighting(buffer2, gammagrays, gammacolors, contrasts, 128, 255, out=buffer2)
    np_image = augpy.tensor_to_array(buffer2)
    print(np_image.shape)
    np_image = np_image[0].transpose(1, 2, 0)
    print(buffer, buffer.shape, buffer.strides, buffer.itemsize)
    stream.synchronize()
    plt.imshow(np_image)
    plt.show()


def lighting_byte(batches):
    for _ in range(batches):
        gammagrays = augpy.array_to_tensor(np.asarray(n * [1.0], dtype="float32"), graysbuffer)
        gammacolors = augpy.array_to_tensor(np.asarray(n * [1, 1, 1], dtype="float32"), colorsbuffer)
        contrasts = augpy.array_to_tensor(np.asarray(n * [-1], dtype="float32"), contrastsbuffer)
        result = augpy.lighting(buffer2, gammagrays, gammacolors, contrasts, 0, 255)
    stream.synchronize()


def lighting_float(batches):
    for _ in range(batches):
        gammagrays = augpy.array_to_tensor(np.asarray(n * [1.0], dtype="float32"), graysbuffer)
        gammacolors = augpy.array_to_tensor(np.asarray(n * [1, 1, 1], dtype="float32"), colorsbuffer)
        contrasts = augpy.array_to_tensor(np.asarray(n * [-1], dtype="float32"), contrastsbuffer)
        result = augpy.lighting(buffer2_float, gammagrays, gammacolors, contrasts, 0, 255)
    stream.synchronize()


funcs = (lighting_byte, lighting_float)


@exp.automain
def main(
        _config,
        batches,
        repeat,
        ignore,
        benchmark,
        profile,
        show,
):
    if show:
        test()
    if benchmark:
        mul = batches * n
        for f in funcs:
            exp.run('benchmark', config_updates=dict(
                path_prefix='lighting',
                func=f,
                torch_func=None,
                kwargs=dict(mul=mul, batches=batches, repeat=repeat, ignore=ignore),
                device=0,
                ylabel='images/s (224x224x3)',
                GPU=exp.current_run.host_info['gpus']['gpus'][0]['model'],
            ))
    if profile:
        nvtx_id = augpy.nvtx_range_start('profile')
        lighting_byte(1)
        lighting_float(1)
        augpy.nvtx_range_end(nvtx_id)
