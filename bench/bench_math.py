import itertools as it

import numpy as np
import torch

import augpy as ap

from .benchtools import exp
from .benchtools import bench


step = 1


def _i(t):
    return t[:, :, :, ::step]


shape = 256, 3, 224, 224*step
N = shape[0]
ind = np.clip(np.arange(np.prod(shape), dtype=np.int32).reshape(shape), 1, None)
stream = ap.default_stream
stream.activate()

# create augpy tensors
a_byte = _i(ap.CudaTensor(shape, dtype=ap.uint8))
a_byte.fill(0)
b_byte = _i(ap.array_to_tensor(ind.astype(np.uint8)))
r_byte = _i(ap.CudaTensor(shape, dtype=ap.uint8))
a_float = _i(ap.CudaTensor(shape, dtype=ap.float32))
a_float.fill(0)
b_float = _i(ap.array_to_tensor(ind.astype(np.float32)))
r_float = _i(ap.CudaTensor(shape, dtype=ap.float32))

# create torch tensors
a_byte_torch = _i(torch.zeros(shape, dtype=torch.uint8, device='cuda:0'))
b_byte_torch = _i(torch.from_numpy(ind).to('cuda:0', torch.uint8))
a_float_torch = _i(torch.zeros(shape, dtype=torch.float32, device='cuda:0'))
b_float_torch = _i(torch.from_numpy(ind).to('cuda:0', torch.float32))

stream.synchronize()


def _fun(name, f, batches, args, **kwargs):
    i = ap.nvtx_range_start(
        name + ('_auto' if kwargs.get('threads') == 0 else '')
    )
    for _ in range(batches):
        f(*args, **kwargs)
    stream.synchronize()
    ap.nvtx_range_end(i)


def add_torch(a, b):
    return a + b


def sub_torch(a, b):
    return a - b


def mul_torch(a, b):
    return a * b


def div_torch(a, b):
    return a / b


funcs = []
funcs_scalar = []
torch_funcs = []
torch_funcs_scalar = []
for op, dtype in it.product(['add', 'sub', 'mul', 'div'], ['byte', 'float']):
    # generate augpy functions
    exec(f"""def {op}_{dtype}(batches, **kwargs):
    _fun("{op}_{dtype}", ap.{op}, batches, (a_{dtype}, b_{dtype}, r_{dtype}), **kwargs)""")
    exec(f"""def {op}_{dtype}_scalar(batches, **kwargs):
    _fun("{op}_{dtype}_scalar", ap.{op}, batches, (a_{dtype}, 123, r_{dtype}), **kwargs)""")
    funcs.append(globals()[f'{op}_{dtype}'])
    funcs_scalar.append(globals()[f'{op}_{dtype}_scalar'])
    # generate torch functions
    exec(f"""def {op}_{dtype}_torch(batches):
    _fun("{op}_{dtype}_torch", {op}_torch, batches, (a_{dtype}_torch, b_{dtype}_torch))""")
    exec(f"""def {op}_{dtype}_torch_scalar(batches):
    _fun("{op}_{dtype}_torch_scalar", {op}_torch, batches, (a_{dtype}_torch, 123))""")
    torch_funcs.append(globals()[f'{op}_{dtype}_torch'])
    torch_funcs_scalar.append(globals()[f'{op}_{dtype}_torch_scalar'])


def base2range(*args):
    return (2**p for p in range(*args))


@exp.automain
def main(
        _config,
        batches,
        repeat,
        ignore,
        benchmark,
        profile,
):
    mul = batches * N
    if benchmark:
        for f, tf in zip(funcs, torch_funcs):
            exp.run('benchmark_and_plot', config_updates=dict(
                path_prefix='math',
                func=f,
                torch_func=tf,
                varied_value_name='threads',
                values=tuple(2 ** k for k in range(5, 11)),
                GPU=exp.current_run.host_info['gpus']['gpus'][0]['model'],
                kwargs=dict(mul=mul, batches=batches, repeat=repeat, ignore=ignore)
            ))
        exp.run('finalize')
    if profile:
        nvtx_id = ap.nvtx_range_start('profile')
        for fun, fun_torch in zip(funcs, torch_funcs):
            bench(fun, repeat, mul, ignore, batches=batches)
            bench(fun_torch, repeat, mul, ignore, batches=batches)
        ap.nvtx_range_end(nvtx_id)
