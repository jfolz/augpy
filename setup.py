import os
import os.path as pt
import sysconfig
import multiprocessing
from math import log2
import re
import sys
import warnings
from subprocess import check_output
from subprocess import CalledProcessError

from setuptools import setup
from setuptools import find_packages
from setuptools import Extension
from setuptools.command.build_ext import build_ext

from generate_saturate_cast_cuh import generate_saturate_cast_cuh


PACKAGE_DIR = pt.abspath(pt.dirname(__file__))


CUDA_PATTERN = r"release ([0-9.]+),"


ALL_CCS = 30, 32, 35, 37, 50, 52, 53, 60, 61, 62, 70, 72, 75, 80, 86


def ccs(min_cc, max_cc):
    return tuple(cc for cc in ALL_CCS if min_cc <= cc <= max_cc)


SUPPORTED_CCS = {
    (10, 0): ccs(30, 75),
    (10, 1): ccs(30, 75),
    (10, 2): ccs(30, 75),
    (11, 0): ccs(35, 86),
    (11, 1): ccs(35, 86),
}
DEFAULT_CCS = 60, 61, 70, 75, 80, 86


def find_cuda_version():
    """
    Use `nvcc --version` output to find CUDA version.
    Dots are removed from version string.

    Returns:
        CUDA version tuple, e.g. `(10, 1)` for version 10.1

    Raises:
        `RuntimeError` if nvcc is not on path
    """
    try:
        # example:
        # $ nvcc --version
        # nvcc: NVIDIA (R) Cuda compiler driver
        # Copyright (c) 2005-2018 NVIDIA Corporation
        # Built on Sat_Aug_25_21:08:01_CDT_2018
        # Cuda compilation tools, release 10.0, V10.0.130
        cmd = 'nvcc', '--version'
        nvcc_output = check_output(cmd).decode('utf-8').strip()
        match = re.search(CUDA_PATTERN, nvcc_output)
        if match:
            version = match.group(1)
            major, _, minor = version.partition('.')
            return int(major), int(minor)
    except (FileNotFoundError, CalledProcessError):
        raise RuntimeError('cannot run nvcc, CUDA not available/not on path?')


def get_default_ccs():
    cuda_ccs = DEFAULT_CCS
    cuda_version = find_cuda_version()
    try:
        supported_ccs = SUPPORTED_CCS[cuda_version]
        cuda_ccs = [str(cc) for cc in cuda_ccs if cc in supported_ccs]
    except KeyError:
        ccstr = ', '.join(map(str, DEFAULT_CCS))
        msg = f'CUDA {cuda_version} is not supported, defaulting to {ccstr}'
        warnings.warn(msg)
        cuda_ccs = DEFAULT_CCS
    return ';'.join(map(str, cuda_ccs))


class CMakeExtension(Extension):
    def __init__(self, name, cmake_root):
        # don't invoke the original build_ext for this special extension
        super().__init__(name, sources=[])
        self.cmake_root = cmake_root


def cvar(name):
    return sysconfig.get_config_var(name)


class cmake_build_ext(build_ext):
    user_options = build_ext.user_options + [
        # The format is (long option, short option, description).
        ('cuda-ccs=', None, "CUDA compute capabilities to build for "
                            "(separated by ';'), defaults to '60;61;70;75'"),
        ('jobs=', 'j', 'number of parallel build processes')
    ]

    def initialize_options(self):
        """Set default values for options."""
        # Each user option must be listed here with their default value.
        super().initialize_options()
        # noinspection PyAttributeOutsideInit
        self.cuda_ccs = get_default_ccs()
        # noinspection PyAttributeOutsideInit
        self.jobs = int(4*log2(multiprocessing.cpu_count())-2)

    def run(self):
        # build cmake extensions separately
        cmake_exts = [ext for ext in self.extensions
                      if isinstance(ext, CMakeExtension)]
        for ext in cmake_exts:
            self.extensions.remove(ext)
            # noinspection PyTypeChecker
            self.build_cmake(ext)
        # build other extensions
        super().run()
        # add cmake extensions
        self.extensions.extend(cmake_exts)

    def build_cmake(self, ext):
        import pybind11

        ext_dir = pt.abspath(pt.dirname(self.get_ext_fullpath(ext.name)))
        if not pt.exists(ext_dir):
            os.makedirs(ext_dir)
        cur_dir = pt.abspath(os.curdir)
        build_dir = pt.join(self.build_temp, ext.name)
        if not pt.exists(build_dir):
            os.makedirs(build_dir)
        os.chdir(build_dir)

        config = 'Debug' if self.debug else 'Release'
        include_dirs = [
            pybind11.get_include(True),
            pybind11.get_include(),
            cvar('INCLUDEPY'),
        ]
        link_flags = cvar('LINKFORSHARED').split(' ')
        if not self.debug:
            link_flags.append('-Wl,--strip-all,--exclude-libs,ALL')
        suffix = cvar('EXT_SUFFIX')
        generate_saturate_cast_cuh()
        self.spawn([
            'cmake',
            '-DCMAKE_BUILD_TYPE=' + config,
            '-DCMAKE_LIBRARY_OUTPUT_DIRECTORY=' + ext_dir,
            '-DPYLINKOPTIONS=' + ';'.join(link_flags),
            '-DPYINCLUDE=' + ';'.join(include_dirs),
            '-DPYSUFFIX=' + suffix,
            '-DCUDA_CCS=' + self.cuda_ccs,
            pt.join(PACKAGE_DIR, ext.cmake_root),
        ])
        if not self.dry_run:
            self.spawn([
                'cmake',
                '--build',
                '.',
                '--config', config,
                '--',
                '-j%s' % self.jobs
            ])
        os.chdir(cur_dir)


ext_modules = [
    CMakeExtension('augpy._augpy', 'augpy'),
]


packages = find_packages(
    include=['augpy', 'augpy.*'],
    exclude=[]
)


package_data = {
    package: [
        # '*.txt',
        # '*.json'
        '*.pyi',
    ]
    for package in packages
}


# add generator scripts for source distribution
if 'sdist' in sys.argv:
    data_files = [
        ('', [
            'generate_saturate_cast_cuh.py',
            'mockup_extension.py',
        ]),
    ]
else:
    data_files = []


with open(pt.join(PACKAGE_DIR, 'requirements.txt')) as f:
    dependencies = [l.strip(' \n') for l in f]


with open(pt.join(PACKAGE_DIR, 'build-requirements.txt')) as f:
    build_dependencies = [l.strip(' \n') for l in f]


with open(pt.join(PACKAGE_DIR, 'README.rst')) as f:
    description = f.read()


def read(*names):
    with open(pt.join(PACKAGE_DIR, *names), encoding='utf8') as f:
        return f.read()


# pip's single-source version method as described here:
# https://python-packaging-user-guide.readthedocs.io/single_source_version/
def find_version(*file_paths):
    version_file = read(*file_paths)
    version_match = re.search(r'^__version__ = [\'"]([^\'"]*)[\'"]',
                              version_file, re.M)
    if version_match:
        return version_match.group(1)
    raise RuntimeError('Unable to find version string.')


setup(
    name='augpy',
    version=find_version('augpy', '__init__.py'),
    author='Joachim Folz',
    author_email='joachim.folz@dfki.de',
    description='Lightweight CUDA tensor functions with a focus on image data augmentation.',
    long_description=description,
    long_description_content_type='text/x-rst; charset=UTF-8',
    keywords='CUDA tensor math data augmentation image affine warp transform gaussian blur gamma correction',
    license='MIT',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Science/Research',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'License :: OSI Approved :: MIT License',
    ],
    ext_modules=ext_modules,
    packages=packages,
    package_data=package_data,
    data_files=data_files,
    setup_requires=build_dependencies,
    install_requires=dependencies,
    cmdclass={'build_ext': cmake_build_ext},
    zip_safe=False,
    project_urls={
        'Documentation': 'https://augpy.readthedocs.io',
        'Source': 'https://gitlab.com/jfolz/augpy',
        'Tracker': 'https://gitlab.com/jfolz/augpy/issues',
    },
)
