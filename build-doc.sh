#!/bin/bash
set -e -x

python3.6 mockup_extension.py
cd doc
rm -rf out xml
python3.6 -m sphinx -b html source out
rm -rf out/.doctrees out/breathe
cd ..
